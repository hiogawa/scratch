- Scraping in a traditional way

```
$ function _tmp_wget() { wget -r -nc -e robots=off --header='User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36' http://www.feynmanlectures.caltech.edu/$1; }
$ _tmp_wget index.html
$ for c in I II III; do _tmp_wget "$c"_toc.html; done
$ for (( i = 1; i <= 52; i++)); do _tmp_wget I_$(printf "%02d" $i).html; done
$ for (( i = 89; i <= 92; i++)); do _tmp_wget I_$(printf "%02d" $i).html; done
$ for (( i = 1; i <= 42; i++)); do _tmp_wget II_$(printf "%02d" $i).html; done
$ for (( i = 1; i <= 22; i++)); do _tmp_wget III_$(printf "%02d" $i).html; done
$ python3 -m http.server # open http://localhost:8000/I_01.html
```

- Print to pdf via Chrome Debugger Protocol

```
$ ./cdp.sh
```
