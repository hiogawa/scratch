// See cdp.sh for the usage

const CDP = require('chrome-remote-interface');
const fs = require('fs');

const SCALE = 1.3;
const URL = process.argv[2]
const PDF_PATH = process.argv[3]
const INFO_PATH = process.argv[4]

// TODO: Rewrite with async/await
CDP({}, (client) => {
    const {Page, Runtime} = client;

    const browserCode0 = () => new Promise((resolve) => {
        // Remove some unnecessary contents
        Array.from(document.querySelectorAll('div.floating-menu, footer'))
        .map(node => node.remove());

        // Small hack for nicer pdf output
        Array.from(document.querySelectorAll('.chapter-title > .tag, .section-title > .tag'))
        .map(node => node.innerText = node.innerText + " ");

        // Wait until mathjax will finish rendering
        MathJax.Hub.Queue(() => resolve());
    });

    const browserCode1 = () => (
        // Extract chapter/section titles
        Array.from(document.querySelectorAll('.chapter-title, .section-title'))
        .map(node => node.innerText.replace(/\n/, ' ').trim() + "\n").join("")
    );

    // Register chromium event handler
    Page.loadEventFired(() => {
        console.log(`Page is loaded`);
        console.log(`Executing javascript ...`);
        Runtime.evaluate({
            expression: `(${browserCode0})()`,
            awaitPromise: true
        }).then(() => {
            console.log('Extract chapter/section titles ...');
            Runtime.evaluate({
                expression: `(${browserCode1})()`,
            }).then(({result}) => {
                fs.writeFileSync(INFO_PATH, result.value);
            });
            console.log('Rendering HTML to PDF ...');
            Page.printToPDF({ scale: SCALE })
            .then(({data}) => {
                console.log('Saving PDF to file ...');
                fs.writeFileSync(PDF_PATH, Buffer.from(data, 'base64'));
                console.log('Done');
                client.close();
            });
        })
    });

    // Start driving chromium
    Promise.all([
        Page.enable(),
        Runtime.enable()
    ]).then(() => {
        console.log(`=== Start navigating to ${URL} ===`);
        return Page.navigate({url: URL});
    }).catch((err) => {
        console.error(err);
        client.close();
    });

}).on('error', (err) => {
    console.error(err);
});
