#!/bin/bash

# Prerequisites
# - Chromium
# - nodejs
# - npm install -g chrome-remote-interface
# - pdftk


# Rendering html page as pdf via Chrome debugger protocol
function ScrapeAsPDF() {
    export NODE_PATH="$(npm config get prefix)/lib/node_modules"
    local N

    for (( i = 1; i <= 52; i++ )); do
        N=$(printf "%02d" $i)
        node cdp.js http://www.feynmanlectures.caltech.edu/I_${N}.html data/1_${N}.pdf data/1_${N}.titles.txt
    done

    for (( i = 1; i <= 42; i++ )); do
        N=$(printf "%02d" $i)
        node cdp.js http://www.feynmanlectures.caltech.edu/II_${N}.html data/2_${N}.pdf data/2_${N}.titles.txt
    done

    for (( i = 1; i <= 22; i++ )); do
        N=$(printf "%02d" $i)
        node cdp.js http://www.feynmanlectures.caltech.edu/III_${N}.html data/3_${N}.pdf data/3_${N}.titles.txt
    done
}


# Combine pdfs and generate bookmark using pdftk
function SubCombinePDFs() {
    local BOOK=${1}
    local NUM_CHAPTERS=${2}
    local N CURRENT_PAGE NUM_PAGES LEVEL
    local TMP0="data/tmp/pdftk_merged_${BOOK}.pdf"
    local TMP1="data/tmp/pdftk_dump_data_${BOOK}.txt"
    local TMP2="data/tmp/pdftk_dump_data_updated_${BOOK}.txt"

    mkdir -p data/tmp
    pdftk data/${BOOK}_*.pdf output ${TMP0}
    pdftk ${TMP0} dump_data output ${TMP1}
    cp ${TMP1} ${TMP2}
    CURRENT_PAGES=1
    for (( i = 1; i <= ${NUM_CHAPTERS}; i++ )); do
        N=$(printf "%02d" $i)
        NUM_PAGES=$(pdftk data/${BOOK}_${N}.pdf dump_data | grep NumberOfPages | awk '{ print $2 }')
        LEVEL=1
        while read -r TITLE; do
            cat >> ${TMP2} <<EOF
BookmarkBegin
BookmarkTitle: ${TITLE}
BookmarkLevel: ${LEVEL}
BookmarkPageNumber: ${CURRENT_PAGES}
EOF
            LEVEL=2
            # TODO: for now, section's page is not determined and it's same as chapter's page number
        done < data/${BOOK}_${N}.titles.txt
        CURRENT_PAGES=$(( ${CURRENT_PAGES} + ${NUM_PAGES} ))
    done
    pdftk ${TMP0} update_info_utf8 ${TMP2} output data/feynman_lectures_${BOOK}.pdf
}


function CombinePDFs() {
    SubCombinePDFs 1 52
    SubCombinePDFs 2 42
    SubCombinePDFs 3 22
    pdftk data/feynman_lectures_{1,2,3}.pdf output data/feynman_lectures.pdf
}


function Main() {
    local PID_CHROMIUM
    chromium --headless --remote-debugging-port=9222 >> chromium.log 2>&1 &
    PID_CHROMIUM="${!}"
    ScrapeAsPDF
    CombinePDFs
    kill "${PID_CHROMIUM}"
}

Main
