Lemma muln_eq0 (m n : nat) : (m * n = 0) <-> ((m = 0) \/ (n = 0)).
Proof.
case m.
- compute. constructor. intros. constructor 1. assumption. 

constructor; intros.
- case m.