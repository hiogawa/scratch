From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section Toy.
  Definition twice (A : Type) : (A -> A) -> A -> A := fun f x => f (f x).
  Eval compute in twice S O.  
  Eval compute in
    let n := 33 in
    twice (fun m => let m' := S m in m' + m') n.
  Locate ".+1".

  Record point : Type := Point { x : nat; y : nat; z : nat}.
  Check Point 1 2.  

  Fixpoint iota n l :=
    if l is l'.+1
    then n :: (iota n.+1 l') 
    else [ :: ].
  Notation "\sum_ ( m <= i < n ) F_i" :=
    (foldr (fun i acc => F_i + acc) O (iota m (n - m))).
  Eval compute in iota 4 8.
  Eval compute in \sum_( 3 <= x < 7 ) (x * x - 1).

End Toy.

Section Exercise1.

  (*
  Inductive triple (A B C: Type) : Type :=
    mk_triple (a : A) (b : B) (c : C).
  *)
  Inductive triple (A B C: Type) : Type :=
    mk_triple : A -> B -> C -> triple A B C.

  Fixpoint iter (A : Type) (n : nat) (op : A -> A) (a : A) : A :=
    if n is p.+1
    then op (iter p op a) (* iter p op (op a)*)
    else a.

  Definition add (n m : nat) : nat :=
    iter m S n.
  Eval compute in add 5 8.

  Definition mult (n m : nat) : nat :=
    iter m (fun x => x + n) 0.
  Eval compute in mult 5 8.
  
  Fixpoint nth (A : Type) (d : A) (ls : list A) (n : nat) : A :=
    match (ls, n) with
    | (a :: _, O) => a
    | (_ :: ls', S n') => nth d ls' n'
    | ([::], _) => d
    end.
  Eval compute in nth 99 [:: 3; 7; 11; 22] 2.
  Eval compute in nth 99 [:: 3; 7; 11; 22] 5.

  Fixpoint foldl (A B : Type) (op : A -> B -> A) (a : A) (ls : seq B) : A :=
    match ls with
    | [::] => a
    | b :: ls' => foldl op (op a b) ls' (* tail recursion *)
    end.

  Fixpoint foldr (A B : Type) (op : B -> A -> A) (a : A) (ls : seq B) : A :=
    match ls with
    | [::] => a
    | b :: ls' => op b (foldr op a ls')
    end.

  Definition rev (A : Type) (ls : list A) : list A :=
    foldl (fun ls a => a :: ls) [::] ls.
  Eval compute in rev [:: 1; 3; 5; 2; 4].

  Definition concat (A : Type) (l1 l2 : list A) : list A :=
    foldr cons l2 l1.
  Eval compute in concat [:: 1; 3; 5; 2; 4] [:: 10; 22].

  Definition flatten (A : Type) (l : seq (seq A)) : seq A :=
    foldr (concat (A:=A)) [::] l.
  Eval compute in flatten [:: (iota 2 5); (iota 4 7)].  

  Definition finite_product (A : Type) (Xs : seq (seq A)) : seq (seq A) :=
    let binary_product X YxZ := flatten (map (fun yz => map (fun x => x :: yz) X) YxZ) in
    foldr binary_product [:: [::]] Xs.
  Eval compute in finite_product [:: [:: 1; 2; 3; 4]; [:: 5; 6; 7]].
  Eval compute in finite_product [:: [:: 1; 2; 3; 4]].
  Eval compute in finite_product [:: [::]].
  Eval compute in finite_product [::].

  Definition all_words (A : Type) (n : nat) (X : seq A) : seq (seq A) :=
    finite_product (iter n (fun Xs => cons X Xs) [::]).
  Eval compute in all_words 2 [:: 1; 2; 3].

End Exercise1.


Section Chapter2.
  

End Chapter2.