Require Import Sets.Ensembles Sets.Image.

Section Toy_Set_Theory.
  Variable Xu Yu : Type.
  Variables X1 X2 : Ensemble Xu.
  Variables Y1 Y2 : Ensemble Yu.
  Variables f : Xu -> Yu.

  (* f(X1 \/ X2) = f(X1) \/ f(X2) *)
  Lemma image_union_preserved :
    Im _ _ (Union _ X1 X2) f = Union _ (Im _ _ X1 f) (Im _ _ X2 f).
  Proof.
  apply Extensionality_Ensembles.
  apply conj.
  * (* f(X1 \/ X2) < f(X1) \/ f(X2) *)
    unfold Included. intros. destruct H. destruct H.
    + apply Union_introl. apply Im_intro with x; assumption.
    + apply Union_intror. apply Im_intro with x; assumption.
  * (* f(X1 \/ X2) > f(X1) \/ f(X2) *)
    unfold Included. intros. destruct H.
    + destruct H. apply Im_intro with x. left. assumption. assumption.
    + destruct H. apply Im_intro with x. right. assumption. assumption.
  Qed.

  Notation "A -< B" := (Included _ A B) (at level 50).
  Notation "x -{ B" := (In _ B x) (at level 50).
  Notation "A \-/ B" := (Union _ A B) (at level 50).
  Notation "A /-\ B" := (Intersection _ A B) (at level 50).
  Notation "f [ A ]" := (Im _ _ A f) (at level 50).
  (* Notation "f inv_[ A ]" := (Im _ _ A f) (at level 50). *)

  Example image_intersection_not_preserved1 :
    ~ ((Im _ _ X1 f) /-\ (Im _ _ X2 f)) -< (Im _ _ (X1 /-\ X2) f).
  Admitted.

  Lemma image_intersection_not_preserved2 :
    (f [ X1 /-\ X2 ]) -< ((f [ X1 ]) /-\ (f [ X2 ])).
  Proof.
  unfold Included. intros. destruct H; constructor.
  - destruct H. rewrite -> H0. apply Im_def. assumption.
  - destruct H. rewrite -> H0. apply Im_def. assumption.
  Qed.

End Toy_Set_Theory.
