const CDP = require('chrome-remote-interface');
const fs = require('fs');

const URL = process.argv[2]
const PDF_PATH = process.argv[3]

CDP(async (client) => {
    try {
        const {Page, Runtime} = client;

        await Promise.all([Page.enable(), Runtime.enable()]);

        console.log(`Navigating to ${URL} ...`);
        await Page.navigate({url: URL});

        console.log(`Page loading ...`);
        await Page.loadEventFired();
        console.log(`Page loaded`);

        console.log(`Executing javascript ...`);
        const browserCode = () => new Promise((resolve) => {
            resolve(document.title);
        })
        const {result} = await Runtime.evaluate({
            expression: `(${browserCode})()`,
            awaitPromise: true
        });
        console.log(`title: ${result.value}`);

        console.log('Rendering to PDF ...');
        const {data} = await Page.printToPDF()

        console.log('Saving PDF to file ...');
        await fs.writeFile(PDF_PATH, Buffer.from(data, 'base64'));

        console.log('Done');
    } catch (err) { console.log(err); }
    client.close();

}).on('error', (err) => {
    console.error(err);
});
