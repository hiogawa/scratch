function Main() {
    local PID_CHROMIUM
    rm -rf chromium_data
    mkdir -p chromium_data
    chromium --user-data-dir=chromium_data --headless --remote-debugging-port=9222 --window-size=1366,768 > chromium.log 2>&1 &
    PID_CHROMIUM="${!}"
    while true; do
        if [ -f chromium_data/DevToolsActivePort ]; then break; fi
        sleep 0.01
    done
    local URL="https://translate.google.com/#de/en/Die%20Arbeitsgemeinschaft%20(AG)%20f%C3%BCr%20Frauen%20in%20Forschung%20und%20Lehre%20unterst%C3%BCtzt%20die%20Kommission%20f%C3%BCr%20Gleichbehandlung%2C%20deren%20Vorsitzende%20eine%20Frau%20aus%20der%20AG%20ist%2C%20und%20den%20Arbeitskreis%20f%C3%BCr%20Gleichbehandlungsfragen%2C%20deren%20Vorsitzende%20ebenfalls%20ein%20Mitglied%20der%20AG%20ist."
    mkdir -p data
    node translate.js "${URL}" data/test.mp3
    kill "${PID_CHROMIUM}"
}

Main "${@}"
