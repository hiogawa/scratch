const CDP = require('chrome-remote-interface');
const writeFile = require('util').promisify(require('fs').writeFile);

const URL = process.argv[2];
const DATA_PATH = process.argv[3];

const wait = (ms) => new Promise(resolve => setTimeout(resolve, ms));
// const waits = []

(async (client) => {
    try {
        var client = await CDP()
        const { Page, Runtime, Network, Input } = client;
        await Promise.all([Page, Runtime, Network].map(dom => dom.enable()));

        Network.setRequestInterception({
            patterns: [{
                urlPattern: 'https://translate.google.com/translate_tts*',
                interceptionStage: 'HeadersReceived'
            }]
        });

        Network.requestIntercepted(async (param) => {
            const { interceptionId, request } = param;
            const index = request.url.match(/idx=(\d*)/)[1];
            const { body, base64Encoded } = await Network.getResponseBodyForInterception({ interceptionId });
            const buf = base64Encoded ? Buffer.from(body, 'base64') : Buffer.from(body);
            await writeFile(`data/test_${index}.mp3`, buf);
        });

        console.log(`=== Loading ${URL} ... ===`);
        await Page.navigate({url: URL});
        await Page.loadEventFired();
        console.log(`Page loaded`);

        console.log(`Executing javascript to trigger speach ...`);
        const browserCode = () => new Promise(resolve => {
            const node = document.querySelector('div#gt-src-listen');
            node.dispatchEvent(new MouseEvent('mousedown'));
            node.dispatchEvent(new MouseEvent('mouseup'));
            resolve();
        });
        const { result } = await Runtime.evaluate({
            expression: `(${browserCode})()`,
            returnByValue: true,
            awaitPromise: true,
        });
        await wait(3000);

        console.log('Done');
    } catch (err) {
        console.log(err);
    } finally {
        if (client) {
            client.close();
        }
    }
})()
