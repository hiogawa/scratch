This uses:

- ruby, nokogiri
- node, puppeteer
- pdftk

```
# Get a list of "pdf" pages
curl -s https://www.deeplearningbook.org \
| nokogiri -e 'puts $_.css("a").map{ |a| a.attributes["href"] }' \
| grep 'contents/' > urls.txt

# Prepare environment for print.js
yarn global add puppeteer
export NODE_PATH=$(yarn global dir)/node_modules

# Print one page
node print.js https://www.deeplearningbook.org/contents/TOC.html 1.pdf

# Print all pages
BASE_URL=https://www.deeplearningbook.org
i=1
for path in $(cat urls.txt); do
	node print.js "${BASE_URL}/${path}" "${i}.pdf"
	i=$(( i + 1 ))
done

# Concatinate pdfs
ORDERED_FILES=$(find . -name '*.pdf' | ruby -e 'puts STDIN.each_line.sort_by{ |f| f.match(/\/(.*)\.pdf/)[1].to_i }')
pdftk $ORDERED_FILES cat output out.pdf
```
