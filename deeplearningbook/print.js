const puppeteer = require('puppeteer');

const	main = async (url, outPath) => {
  console.log(':: Launching browser...')
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  console.log(`:: Navigating to "${url}"...`);
  await page.goto(url, { waitUntil: 'networkidle2' });

  console.log(`:: Writing pdf to "${outPath}"...`);
  const path = url.split('/').reverse()[0];
  await page.pdf({ path: outPath });
  await browser.close();
  console.log(`:: Finished.`);
}

main(process.argv[2], process.argv[3]);
