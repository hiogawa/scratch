import os
import subprocess
import yaml

IN_FILE='sentences/data2.yaml'
OUT_DIR='sentences/tmp/'
LANG='de'
ARTIST='German Practice'
ALBUM='Practice 0'

def execute(cmd):
    return subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

def convert(filename_mp3, filename_opus, title, track_num):
    print('executing ffmpeg ...')
    cmd = 'ffmpeg -y -v quiet -i "{}" -metadata "artist={}" -metadata "album={}" -metadata "title={}" -metadata "track={}" "{}"'.format(
            filename_mp3, ARTIST, ALBUM, title, track_num, filename_opus)
    proc = execute(cmd)
    if proc.returncode != 0:
        print('-- FAILED (ffmpeg) -- {} -- {}'.format(filename))
        print(cmd.stdout)
        print(cmd.stderr)
    os.remove(filename_mp3)

def text_to_speech(filename, text):
    print('executing curl ...')
    cmd = '''
        curl -f -G -H 'User-Agent: Chrome' \
            --data-urlencode "q={}" \
            "http://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl={}" \
            > "{}"
    '''.strip().format(text, LANG, filename)
    proc = execute(cmd)
    if proc.returncode != 0:
        print('-- FAILED (curl) -- {} -- {}'.format(filename, text))
        print(cmd.stdout)
        print(cmd.stderr)

def main():
    track_num = 1
    for h in yaml.load(open(IN_FILE).read()):
        for i, text in enumerate(h['contents']):
            filename = OUT_DIR + '{} - {:02d}.mp3'.format(h['title'], i)
            filename_opus = OUT_DIR + '{} - {:02d}.opus'.format(h['title'], i)
            title = '{} - {:02d} : {}'.format(h['title'], i, text)
            print('=== ' + title + ' ===')
            text_to_speech(filename, text)
            convert(filename, filename_opus, title, track_num)
            track_num = track_num + 1

if __name__ == "__main__":
    main()
