DIR="courses.dcs.wisc.edu/wp/readinggerman/category/"

function ScrapeAsPDF() {
    mkdir -p data/
    for SUB_DIR in "${PWD}/${DIR}/"*; do
        NAME="${SUB_DIR##*/}"
        node cdp.js "file://${SUB_DIR}/index.html" "data/${NAME}.pdf" "data/${NAME}.sections.txt"
    done
}

function TOCTemplate() {
    cat <<EOF
BookmarkBegin
BookmarkTitle: ${1}
BookmarkLevel: ${2}
BookmarkPageNumber: ${3}
EOF
}

function CombinePDFs() {
    mkdir -p data/{tmp,out}
    for SUB_DIR in "${PWD}/${DIR}/"*; do
        NAME="${SUB_DIR##*/}"
        pdftk "data/${NAME}.pdf" dump_data_utf8 output "data/tmp/${NAME}.old.pdftk"
        cp "data/tmp/${NAME}.old.pdftk" "data/tmp/${NAME}.new.pdftk"
        TOCTemplate "$(head -n 1 "data/${NAME}.sections.txt")" 1 1 >> "data/tmp/${NAME}.new.pdftk"
        tail -n +2 "data/${NAME}.sections.txt" | sed -e '$a\' | while read SECTION; do
            PAGE=$(pdfgrep -n "${SECTION}" "data/${NAME}.pdf" | cut -d ':' -f 1 -)
            # TODO: for now, if SECTION is not found by pdfgrep,
            #       just set page number to be the chapter's start page
            TOCTemplate "${SECTION}" 2 "${PAGE:-1}" >> "data/tmp/${NAME}.new.pdftk"
        done
        pdftk "data/${NAME}.pdf" update_info_utf8 "data/tmp/${NAME}.new.pdftk" output "data/tmp/${NAME}.new.pdf"
    done
    pdftk data/tmp/*.new.pdf output data/out/german.pdf
}

function Main() {
    chromium --headless --remote-debugging-port=9222 >> chromium.log 2>&1 &
    PID_CHROMIUM="${!}"
    sleep 1
    ScrapeAsPDF
    CombinePDFs
    kill "${PID_CHROMIUM}"
}

Main
