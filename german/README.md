- Generate nicer pdf than theirs from "A Foundation Course in Reading German"

```
$ wget -r --include-directories=/wp/readinggerman/category https://courses.dcs.wisc.edu/wp/readinggerman/
$ bash pdf_generate.sh
```

- Generate audio from sentences

```
# Sentence list by http://frequencylists.blogspot.jp/2016/08/german-sentences-sorted-from-easiest-to.html
$ curl https://pastebin.com/raw/pqZ66NKJ | dos2unix > data/sentences/data0.txt
$ bash sentences.sh
```

c.f.

- https://courses.dcs.wisc.edu/wp/readinggerman/
- http://frequencylists.blogspot.jp/2016/08/german-sentences-sorted-from-easiest-to.html
