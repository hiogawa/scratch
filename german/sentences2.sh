G_FILE="${PWD}/data/sentences/data3.txt"
G_AUDIO_DIR="${PWD}/data/sentences/tmp"

function AddMetadata() {
    local TITLE="${1}"
    local FILE="${2}"
    local TRACK="${3}"
    local ARTIST="German Sentences"
    local TMP_OUT=$(mktemp --suffix .mp3)
    ffmpeg -y -v quiet -i "${FILE}" -metadata "track=${TRACK}" -metadata "title=${TITLE}" -metadata "artist=${ARTIST}" "${TMP_OUT}" < /dev/null
    mv -f "${TMP_OUT}" "${FILE}"
}

function Combine() {
    local FILE="${1}"
    shift
    local PARTS=("${@}")
    local TMP=$(mktemp --suffix .txt)
    for P in "${PARTS[@]}"; do echo "file ${P}" >> "${TMP}"; done
    cat "${TMP}"
    ffmpeg -y -v quiet -f concat -safe 0 -i "${TMP}" -c copy "${FILE}" < /dev/null
    rm "${TMP}"
}

function Split() {
    python3 - "${1}" <<EOF
import sys, re
MAX_LEN = 160
s = sys.argv[1]
buf = ''
for t in s.split(' '):
    buf = buf + ' ' + t
    if len(buf) >= MAX_LEN:
        print(buf.strip())
        buf = ''
print(buf.strip())
EOF
}

function TextToSpeach() {
    local FILE="${1}"
    local TEXT="${2}"
    local LANG="de"

    local PART_FILES=()
    while read PART; do
        local N="${#PART_FILES[@]}"
        local PART_FILE="${FILE}.part.${N}.mp3"
        PART_FILES["${N}"]="${PART_FILE}"
        curl -f -G -H 'User-Agent: Chrome' \
            --data-urlencode "q=${PART}" \
            "http://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=${LANG}" \
            > "${PART_FILE}"
        if [ "${?}" != "0" ]; then
            echo "FAILED: ${PART_FILE} - ${PART}"
            return 1
        fi
    done <<< $(Split "${TEXT}")
    Combine "${FILE}" "${PART_FILES[@]}"
    rm -f "${PART_FILES[@]}"
}

function Process() {
    local TEXT="${1}"
    local INDEX="${2}"
    local FILE="${G_AUDIO_DIR}/${INDEX}.mp3"

    echo "=== ${INDEX}: ${TEXT} ==="
    TextToSpeach "${FILE}" "${TEXT}"
    AddMetadata "${TEXT}" "${FILE}" "${INDEX}"
}

function Main() {
    local N="1"
    cat "${G_FILE}" | while read LINE; do
        Process "${LINE}" $(printf "%02d" "${N}")
        N=$(( N + 1 ))
    done
}

Main
