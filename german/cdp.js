// See pdf_generate.sh for the usage

const CDP = require('chrome-remote-interface');
const writeFile = require('util').promisify(require('fs').writeFile);

const SCALE = 1.2;
const URL = process.argv[2]
const PDF_PATH = process.argv[3];
const INFO_PATH = process.argv[4];

(async() => {
    try {
        var client = await CDP();
        const { Page, Runtime } = client;
        await Promise.all([
            Page.enable(),
            Runtime.enable()
        ]);

        console.log(`=== Start navigating to ${URL} ===`);
        await Page.navigate({url: URL});
        await Page.loadEventFired();
        console.log(`Page is loaded`);

        // Remove some unwanted contents
        console.log(`Executing javascript for cosmetic change ...`);
        const browserCode0 = () => {
            Array.from(document.querySelectorAll('body > div > header, footer'))
            .map(node => node.remove());
        };
        await Runtime.evaluate({
            expression: `(${browserCode0})()`,
        })

        // Extract chapter/section titles
        console.log('Extract chapter/section titles ...');
        const browserCode1 = () => (
            [].concat(
                document.title.split('|')[0].trim(),
                Array.from(document.querySelectorAll('article > header > h1')).map(node => node.innerText)
            ).join("\n")
        );
        const { result } = await Runtime.evaluate({
            expression: `(${browserCode1})()`,
        });
        await writeFile(INFO_PATH, result.value);

        console.log('Rendering HTML to PDF ...');
        const {data} = await Page.printToPDF({
            scale: SCALE,
            marginTop: 0.4, // in inch
            marginBottom: 0.4,
            marginLeft: 0.15,
            marginRight: 0.15,
        });
        console.log('Saving PDF to file ...');
        await writeFile(PDF_PATH, Buffer.from(data, 'base64'));

        console.log('Done');
    } catch (err) {
        console.error(err);
    } finally {
        if (client) {
            await client.close();
        }
    }
})();
