mkdir -p data/sentences/tmp
FILE="data/sentences/data0.txt"
AUDIO_DIR="data/sentences/tmp"

function AddMetadata() {
    TITLE="${1}"
    local FILE="${2}"
    TRACK="${3}"
    ARTIST="German Sentences"
    TMP_OUT=$(mktemp --suffix .mp3)
    ffmpeg -y -hide_banner -i "${FILE}" -metadata "track=${TRACK}" -metadata "title=${TITLE}" -metadata "artist=${ARTIST}" "${TMP_OUT}" < /dev/null
    mv -f "${TMP_OUT}" "${FILE}"
}

# cf. vfatPath from https://github.com/CDrummond/cantata/blob/master/devices/deviceoptions.cpp#L79
function SanitizeFileName() {
    python3 - "${1}" <<EOF
import sys, re

H = {
    '*?<>|":/\\\\': '_',
    '[': '(',
    ']': ')'
}.items()

def replace(s, i, r):
    return s[:i] + r + s[i+1:]

def main():
    s = t = sys.argv[1]
    for i, c in enumerate(s):
        for k, v in H:
            if c in k:
                t = replace(t, i, v)
                break
    print(t)

main()
EOF
}

function Process() {
    DE="${1}"
    EN="${2}"
    INDEX="${3}"

    ## Fixing up mistakes 0 ##
    # echo "=== ${INDEX}: ${DE} --- ${EN} ==="
    # ORIG="${INDEX} - ${DE%.}  -  ${EN%.}.mp3"
    # FILENAME=$(SanitizeFileName "${ORIG}")
    # mv "/home/hiogawa/code/hiogawa/blob/music/German Sentences/${ORIG}" \
    #     "/home/hiogawa/code/hiogawa/blob/music/German Sentences/${FILENAME}"
    # return 0

    ## Fixing up mistakes 1 ##
    # echo "=== ${INDEX}: ${DE} --- ${EN} ==="
    # FILENAME=$(SanitizeFileName "${INDEX} - ${DE%.}  -  ${EN%.}.mp3")
    # AddMetadata "${DE%.}  -  ${EN%.}" "${AUDIO_DIR}/${FILENAME}" "$(( ${INDEX} + 1 ))/5000"
    # return 0

    echo "=== ${INDEX}: ${DE} --- ${EN} ==="
    FILENAME=$(SanitizeFileName "${INDEX} - ${DE%.}  -  ${EN%.}.mp3")
    curl -f -G -H 'User-Agent: Chrome' \
        --data-urlencode "q=${DE}" \
        "http://translate.google.com/translate_tts?client=tw-ob&tl=de" \
        > "${AUDIO_DIR}/${FILENAME}"
    AddMetadata "${DE%.}  -  ${EN%.}" "${AUDIO_DIR}/${FILENAME}" "$(( ${INDEX} + 1 ))/5000"

    if [ "${?}" != "0" ]; then
        echo "FAILED (INDEX: ${INDEX})"
    fi
}

function Main() {
    N="0"
    TMP0=""
    TMP1=""
    cat "${FILE}" | while read -r LINE; do
        if [ "$(( N % 5 ))" == "0" ] ; then
            TMP0="${LINE}"
        elif [ "$(( N % 5 ))" == "1" ] ; then
            TMP1="${LINE}"
        elif [ "$(( N % 5 ))" == "2" ] ; then
            Process "${TMP0}" "${TMP1}" "$(( N / 5 ))"
        fi
        N=$(( N + 1 ))
    done
}

Main
