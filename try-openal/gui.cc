#include <array>
#include <QtCore/QDebug>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMessageBox>

#include "gui.h"
#include "ui_gui.h"
#include "audio.h"

constexpr qreal POINT_SIZE = 10;

View::View(QWidget* parent) :
    QGraphicsView(parent),
    scene_(new QGraphicsScene) {
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

  setScene(scene_);
  source_point_ = scene_->addEllipse(-POINT_SIZE / 2, -POINT_SIZE / 2, POINT_SIZE, POINT_SIZE);
  source_point_->setFlags(QGraphicsItem::ItemIsMovable);
  lines_[0] = scene_->addLine(0, 0, 0, 0);
  lines_[1] = scene_->addLine(0, 0, 0, 0);
}

View::~View() {
  delete scene_;
}

void View::SetSourcePosition(std::array<qreal, 2> position) {
  source_point_->setPos(position[0], position[1]);
}

std::array<qreal, 2> View::SourcePosition() {
  QPointF point = source_point_->pos();
  return {{ point.x(), point.y() }};
}

void View::resizeEvent(QResizeEvent* event) {
  qreal dx = event->size().width() / 2;
  qreal dy = event->size().height() / 2;
  lines_[0]->setLine(-dx, 0, dx, 0);
  lines_[1]->setLine(0, -dy, 0, dy);
  centerOn(0, 0);
  QGraphicsView::resizeEvent(event);
};


Window::Window(audio::Source* source, QWidget* parent) :
    QMainWindow(parent),
    ui_(new Ui::Window),
    source_(source) {
  ui_->setupUi(this);
  startTimer(100);

  connect(ui_->view->scene(), SIGNAL(changed(const QList<QRectF>&)),
          this, SLOT(OnSceneChanged(const QList<QRectF>&)));
  std::array<float, 3> position = source_->Position();
  ui_->view->SetSourcePosition({{ static_cast<qreal>(position[0]) * 5,
                                  static_cast<qreal>(position[1]) * 5 }});
}

Window::~Window() {
  delete ui_;
}

void Window::SetSourcePosition() {
  std::array<qreal, 2> position = ui_->view->SourcePosition();
  auto value = static_cast<float>(ui_->slider->value());
  source_->SetPosition({{ static_cast<float>(position[0]) / 5,
                          static_cast<float>(position[1]) / 5,
                          value / 10 }});
}

void Window::timerEvent(QTimerEvent*) {
  bool ready = source_->IsReady();
  bool playing = source_->IsPlaying();
  ui_->actionOpen->setEnabled(!ready || !playing);
  ui_->actionPlay->setEnabled(ready && !playing);
  ui_->actionStop->setEnabled(playing);
}

void Window::OnSceneChanged(const QList<QRectF>&) {
  SetSourcePosition();
}

void Window::on_slider_valueChanged(int) {
  SetSourcePosition();
}

void Window::on_actionOpen_triggered() {
  QString filename = QFileDialog::getOpenFileName(this);
  if (!filename.isEmpty()) {
    if (!source_->LoadFile(filename.toLocal8Bit().data())) {
      QMessageBox::critical(this, QString("Error"), QString("Failed to load file"));
    }
  }
}

void Window::on_actionPlay_triggered() {
  source_->Play();
}

void Window::on_actionStop_triggered() {
  source_->Stop();
}
