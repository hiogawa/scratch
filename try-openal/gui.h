#pragma once

#include <array>
#include <QtWidgets/QGraphicsEllipseItem>
#include <QtWidgets/QGraphicsLineItem>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QMainWindow>
#include "audio.h"

class View : public QGraphicsView {
  Q_OBJECT

public:
  View(QWidget *parent = nullptr);
  ~View();
  void SetSourcePosition(std::array<qreal, 2>);
  std::array<qreal, 2> SourcePosition();

private:
  // override
  void resizeEvent(QResizeEvent*);
  void wheelEvent(QWheelEvent*) {}

  QGraphicsScene* scene_;
  QGraphicsEllipseItem* source_point_;
  QGraphicsLineItem* lines_[2];
};

namespace Ui { class Window; }

class Window : public QMainWindow {
  Q_OBJECT

public:
  Window(audio::Source* source, QWidget *parent = nullptr);
  ~Window();

private:
  // override
  void timerEvent(QTimerEvent*);

  void SetSourcePosition();

  Ui::Window* ui_;
  audio::Source* source_;

private slots:
  void on_actionOpen_triggered();
  void on_actionPlay_triggered();
  void on_actionStop_triggered();
  void on_slider_valueChanged(int);
  void OnSceneChanged(const QList<QRectF>&);
};
