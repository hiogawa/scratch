#include <array>
#include <iostream>
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>
#include <sndfile.h>
#include "audio.h"

namespace audio {

Source::Source() {
  alGenSources(1, &al_source_);
  alSourcei(al_source_, AL_SOURCE_RELATIVE, AL_TRUE);
  alSource3f(al_source_, AL_POSITION, 0.0f, 0.0f, 0.0f);
  SetLooping(true);
}

Source::~Source() {
  alDeleteSources(1, &al_source_);
  if(alIsBuffer(al_buffer_)) {
    alDeleteBuffers(1, &al_buffer_);
  }
}

void Source::SetPosition(std::array<float, 3> position) {
  alSource3f(al_source_, AL_POSITION, position[0], position[1], position[2]);
}

std::array<float, 3> Source::Position() {
  std::array<float, 3> position;
  alGetSource3f(al_source_, AL_POSITION, &position[0], &position[1], &position[2]);
  return position;
}

void Source::SetLooping(bool looping) {
  alSourcei(al_source_, AL_LOOPING, looping);
}

bool Source::LoadFile(char* filename) {
  if(alIsBuffer(al_buffer_)) {
    alDeleteBuffers(1, &al_buffer_);
  }

  SF_INFO info;
  SNDFILE* sndfile = sf_open(filename, SFM_READ, &info);

  if (!sndfile || info.channels != 1) {
    std::cerr << "Unsupported sound file format\n";
    sf_close(sndfile);
    return false;
  }

  size_t size = static_cast<size_t>(info.frames) * sizeof(short);
  auto samples = reinterpret_cast<short*>(malloc(size));
  sf_seek(sndfile, 0ul, SEEK_SET);
  sf_readf_short(sndfile, samples, info.frames);

  alGenBuffers(1, &al_buffer_);
  alBufferData(al_buffer_, AL_FORMAT_MONO16, samples, static_cast<ALsizei>(size), info.samplerate);

  free(samples);
  sf_close(sndfile);

  alSourcei(al_source_, AL_BUFFER, static_cast<ALint>(al_buffer_));
  return true;
}

void Source::Play() {
  alSourceRewind(al_source_);
  alSourcePlay(al_source_);
}

void Source::Stop() {
  alSourceStop(al_source_);
}

bool Source::IsReady() {
  ALenum state;
  alGetSourcei(al_source_, AL_BUFFER, &state);
  return state != AL_NONE;
}

bool Source::IsPlaying() {
  ALenum state;
  alGetSourcei(al_source_, AL_SOURCE_STATE, &state);
  return state == AL_PLAYING;
}

Utils::Utils() {
  al_device_ = alcOpenDevice(nullptr);
  al_context_ = alcCreateContext(al_device_, nullptr);
  alcMakeContextCurrent(al_context_);

  auto alcResetDeviceSOFT = reinterpret_cast<LPALCRESETDEVICESOFT>(
    alcGetProcAddress(al_device_, "alcResetDeviceSOFT"));

  ALCint hrtf_attr[5] = {ALC_HRTF_SOFT, ALC_TRUE, ALC_HRTF_ID_SOFT, 0, 0};
  alcResetDeviceSOFT(al_device_, hrtf_attr);
  std::cerr << "HRTF: " << alcGetString(al_device_, ALC_HRTF_SPECIFIER_SOFT) << "\n";
}

Utils::~Utils() {
  alcMakeContextCurrent(nullptr);
  alcDestroyContext(al_context_);
  alcCloseDevice(al_device_);
}

} // namespace audio
