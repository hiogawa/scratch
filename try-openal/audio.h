#pragma once

#include <array>
#include <AL/al.h>
#include <AL/alc.h>

namespace audio {

// NOTE: listener's defult attributes are as below:
// - position: {0, 0, 0}
// - orientation: at-vector {0, 0, -1}, up-vector {0, 1, 0}
class Source {
public:
  Source();
  ~Source();
  void SetPosition(std::array<float, 3>);
  std::array<float, 3> Position();
  bool LoadFile(char*);
  void Play();
  void Stop();
  void SetLooping(bool);
  bool IsReady();
  bool IsPlaying();

private:
  ALuint al_source_;
  ALuint al_buffer_;
};

class Utils {
public:
  Utils();
  ~Utils();

private:
  ALCdevice* al_device_;
  ALCcontext* al_context_;
};

} // namespace audio
