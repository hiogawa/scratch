#include <QtWidgets/QApplication>
#include "gui.h"
#include "audio.h"

int main(int argc, char* argv[]) {
  audio::Utils utils;
  audio::Source source;
  QApplication app(argc, argv);
  Window window(&source);
  window.show();
  return app.exec();
}
