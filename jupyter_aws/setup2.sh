#!/bin/bash

type docker >/dev/null 2>&1
if [ "${?}" != "0" ]; then
    sudo yum install docker -y
fi

type docker-compose >/dev/null 2>&1
if [ "${?}" != "0" ]; then
    sudo curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
fi

DEST_DIR="/opt/jupyter_aws"

systemctl is-enabled docker_jupyter.service
if [ "${?}" != "0" ]; then
    sudo systemctl enable "${DEST_DIR}/docker_jupyter.service"
fi

systemctl is-active docker_jupyter
if [ "${?}" != "0" ]; then
    sudo systemctl start docker_jupyter
fi

systemctl is-enabled update_dns.service
if [ "${?}" != "0" ]; then
    sudo systemctl enable "${DEST_DIR}/update_dns.service"
fi
