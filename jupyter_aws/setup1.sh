#!/bin/bash

if [ -z "${DEST}" ]; then
    echo 'Please specify "DEST" variable.'
    exit 1
fi
DEST_DIR="/opt/jupyter_aws"

rsync -r --progress --files-from=rsync.txt --rsync-path="sudo rsync" . "${DEST}:${DEST_DIR}"
ssh "${DEST}" bash "${DEST_DIR}/setup2.sh"
