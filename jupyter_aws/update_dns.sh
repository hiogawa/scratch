#!/bin/bash

function Main() {
    local AWS ZONE_ID NAME ADDRESS TMP_JSON CHANGE_ID

    NAME="jupyter.hiogawa.net"
    ADDRESS=$(\
        aws ec2 describe-instances \
            --filters 'Name=tag:Name,Values=Jupyter' \
            --query 'Reservations[0].Instances[0].PublicIpAddress' \
        | sed -e 's/\"//g')

    if [ "${ADDRESS}" == "null" ]; then
        exit 1
    fi

    ZONE_ID=$(\
        aws route53 list-hosted-zones-by-name \
            --dns-name hiogawa.net \
            --query 'HostedZones[0].Id' \
        | sed -e 's/\/.*\///' \
        | sed -e 's/\"//g')

    TMP_JSON=$(mktemp --suffix=.json)
    trap "rm ${TMP_JSON}" EXIT

    cat > "${TMP_JSON}" <<EOF
{
    "Changes": [
        {
            "Action": "UPSERT",
            "ResourceRecordSet": {
                "Name": "${NAME}",
                "Type": "A",
                "TTL": 300,
                "ResourceRecords": [
                    { "Value": "${ADDRESS}" }
                ]
            }
        }
    ]
}
EOF

    CHANGE_ID=$(\
        aws route53 change-resource-record-sets \
            --hosted-zone-id "${ZONE_ID}" \
            --change-batch file://"${TMP_JSON}" \
            --query 'ChangeInfo.Id' \
        | sed -e 's/\/.*\///' \
        | sed -e 's/\"//g')

    aws route53 wait resource-record-sets-changed --id "${CHANGE_ID}"
}

Main
