Usage

```
$ DEST="ec2-user@jupyter.hiogawa.net" bash setup1.sh
```

TODO

- pull/push notebook from git repositiory (actually you can just install git from conda and do anything from jupyter terminal)
- limit container resource (cpu, memory) so that poor server won't die.
- better launcher
