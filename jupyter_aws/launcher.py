import subprocess
import boto3
import os

CONFIG = {
    'AWS_ACCESS_KEY_ID': '',
    'AWS_SECRET_ACCESS_KEY': '',
    'AWS_DEFAULT_REGION': ''
}

# https://boto3.readthedocs.io/en/latest/guide/ec2-example-managing-instances.html

def lambda_handler(event, context):
    for k, v in CONFIG.items():
        os.putenv(k, v)
    ec2 = boto3.client('ec2')
    # ec2.
    return {
        'statusCode': 200,
        'body': subprocess.getoutput('env')
    }
