#!/bin/bash

G_FILTER='Name=tag:Name,Values=Jupyter'

function Main() {
    local COMMAND RESP ID STATE START_OR_STOP

    COMMAND="${1}"
    RESP=$(\
        aws ec2 describe-instances \
            --filters "${G_FILTER}" \
            --query 'Reservations[0].Instances[0].[InstanceId,State.Name]' \
            --output text)
    ID=$(echo "${RESP}" | awk '{ print $1 }')
    STATE=$(echo "${RESP}" | awk '{ print $2 }')

    if [ "${COMMAND}" == "on" -a "${STATE}" == "stopped" ]; then
        START_OR_STOP="start-instances"
    elif [ "${COMMAND}" == "off" -a "${STATE}" == "running" ]; then
        START_OR_STOP="stop-instances"
    elif [ "${COMMAND}" == "check" ]; then
        echo "${STATE}"
        return 0
    else
        echo "Bad command \"${COMMAND}\" for the current state \"${STATE}\"."
        return 1
    fi
    aws ec2 "${START_OR_STOP}" --instance-id "${ID}"
}

Main "${@}"
