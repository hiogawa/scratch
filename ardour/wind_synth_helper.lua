-- Modified from ardour's midimon.lua and _amp2.lua

ardour {
  ["type"]    = "dsp",
  name        = "Wind Synth Helper",
  category    = "Amplifier",
  license     = "GPLv2",
  author      = "Hiroshi Ogawa",
  description = [[Small helpers to play synth with wind midi controller]]
}

function dsp_ioconfig ()
  return { { midi_in = 1, midi_out = 1, audio_in = -1, audio_out = -1},  }
end


function dsp_params ()
  return
  {
    { ["type"] = "input", name = "Gain max", min = -20, max = 20, default = 20, unit = "dB" },
    { ["type"] = "input", name = "Gain min", min = -20, max = 20, default = 0, unit = "dB" },
    { ["type"] = "input", name = "CC type", min = 0, max = 127, default = 2, integer = true },
    { ["type"] = "input", name = "Constantization switch", default = true, toggled = true },
    { ["type"] = "input", name = "Constant noteon velocity", min = 0, max = 127, default = 100, integer = true },
  }
end

local cc_value = 0

function calculate_gain_coeff ()
  local params = CtrlPorts:array()
  local max = params[1]
  local min = params[2]
  local gain_in_db = min + (max - min) * (cc_value / 127.0)
  return ARDOUR.DSP.dB_to_coefficient (gain_in_db)
end


-- TODO:
-- - consider midi timestamp
-- - interporate cc_value change smoothly
function dsp_runmap (bufs, in_map, out_map, n_samples, offset)
  local params = CtrlPorts:array()
  local cc_type = params[3]
  local constant_mode = params[4]
  local constant_volocity = params[5]

  -- Passthrough in/out buffers (and later modify buffer in-place)
  ARDOUR.DSP.process_map(bufs, in_map, out_map, n_samples, offset, ARDOUR.DataType("audio"))
  ARDOUR.DSP.process_map(bufs, in_map, out_map, n_samples, offset, ARDOUR.DataType("midi"))

  -- Update cc value and constantize noteon velocity
  local midi_buf_idx = out_map:get(ARDOUR.DataType("midi"), 0)
  assert(midi_buf_idx ~= ARDOUR.ChanMapping.Invalid)
  local events = bufs:get_midi(midi_buf_idx):table()
  for _, e in pairs (events) do
    local ev = e:buffer():array()
    -- CC event (0b1011)
    if (ev[1] >> 4) == 11 and (ev[2] == cc_type) then
      cc_value = ev[3]
    end

    if constant_mode then
      -- Noteon event (0b1000)
      if (ev[1] >> 4) == 8 then
        ev[3] = constant_volocity
      end
    end
  end

  -- Apply amp gain
  local gain = calculate_gain_coeff ()
  for i = 1, out_map:count():n_audio() do
    local audio_buf_idx = out_map:get(ARDOUR.DataType ("audio"), i - 1)
    assert(audio_buf_idx ~= ARDOUR.ChanMapping.Invalid)
    local a = bufs:get_audio(audio_buf_idx):data(offset):array()
    for s = 1,n_samples do
      a[s] = a[s] * gain
    end
  end
end
