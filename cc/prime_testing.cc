#include <cmath>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <random>
#include <gtest/gtest.h>
using namespace std;

mt19937 g_rand_gen(0);
int RandRange(int a, int b) {
  return uniform_int_distribution<int>(a, b)(g_rand_gen);
}

int FastExp(int base, int exp, int mod) {
  assert(exp >= 0);

  int result = 1;
  int tmp_power = base;
  while (true) {
    if (exp == 0) { break; }
    if (exp & 1) {
      result = (result * tmp_power) % mod;
    }
    exp = exp >> 1;
    tmp_power = (tmp_power * tmp_power ) % mod;
  }
  return result;
}

int BinGcd(int a, int b) {
  if (a == 0) {
    return b;
  }
  if (b == 0) {
    return a;
  }

  // a: even, b: even
  if (!(a & 1) && !(b & 1)) {
      return BinGcd(a >> 1, b >> 1) << 1;
  }

  // a: even, b: odd
  if (!(a & 1)) {
      return BinGcd(a >> 1, b);
  }

  // a: odd, b: even
  if (!(b & 1)) {
      return BinGcd(a, b >> 1);
  }

  // a: odd, b: odd
  if (a < b) {
    return BinGcd(a, (b - a) >> 1);
  } else {
    return BinGcd((a - b) >> 1, b);
  }
}

int Gcd(int a, int b) {
  return BinGcd(abs(a), abs(b));
}

int8_t JacobiSymbol_NegOne(int b) {
  // pow(-1, (b - 1) / 2);
  return (((b - 1) >> 1) & 1) ? -1 : 1;
}

int8_t JacobiSymbol_Two(int b) {
  // pow(-1, (b * b - 1) / 8);
  return (((b * b - 1) >> 3) & 1) ? -1 : 1;
}

int8_t JacobiSymbolRecWithDivision(int a, int b) {
  assert(a <= b);
  assert(b >= 3);
  assert(b & 1); // b: odd

  if (a == 0) {
    return 0;
  }

  // a: even, b: odd
  if (!(a & 1)) {
    return JacobiSymbol_Two(b) * JacobiSymbolRecWithDivision(a >> 1, b);
  }

  // a: odd, b: odd
  return (
    // aka. reciprocal law: pow(-1, (a - 1) * (b - 1) / 2)
    ((((a - 1) >> 1) & 1) ^ (((b - 1) >> 1) & 1) ? -1 : 1) *
    JacobiSymbolRecWithDivision(b % a, a)
  );
}

int8_t JacobiSymbolRec(int a, int b) {
  assert(a >= 0);
  assert(b > 0);
  assert(b & 1); // b: odd

  if (a == 0) {
    return 0;
  }
  if (a == 1 || b == 1) {
    return 1;
  }

  // a: even, b: odd
  if (!(a & 1)) {
    return JacobiSymbol_Two(b) * JacobiSymbolRec(a >> 1, b);
  }

  // a: odd, b: odd
  // a >= b
  if (a >= b) {
    return JacobiSymbol_Two(b) * JacobiSymbolRec((a - b) >> 1, b);
  }

  // a: odd, b: odd
  // a < b
  return (
    // aka. reciprocal law: pow(-1, (a - 1) / 2 * (b - 1) / 2)
    ((((a - 1) >> 1) & 1) & (((b - 1) >> 1) & 1) ? -1 : 1) *
    JacobiSymbol_Two(a) *
    JacobiSymbolRec((b - a) >> 1, a)
  );
}

int8_t JacobiSymbol(int a, int b) {
  assert(b > 0);
  assert(b & 1); // b: odd

  return (
    (a < 0 ? JacobiSymbol_NegOne(b) : 1) *
    JacobiSymbolRec(abs(a), b)
  );
}

bool IsComposite(int n) {
  // n: even
  if (!(n & 1)) {
    return true;
  }

  // One sided probabilistic testing
  int a = RandRange(1, n - 1);
  if (Gcd(a, n) > 1) {
    // cout << "Gcd(a, N) > 1 with a = " << a << endl;
    return true;
  }

  int jacobi = JacobiSymbol(a, n);
  int euler = FastExp(a, (n - 1) / 2, n);
  // Need to make "jacobi" positive
  if ((jacobi + n) % n != euler % n) {
    // cout << "jacobi (" << jacobi << ") != euler (" << euler << ") with a = " << a << endl;
    return true;
  }

  return false;
}

int IsCompositeTrials(int n, int num_trials) {
  int composite_count = 0;
  for (auto i = 0; i < num_trials; i++) {
    if (IsComposite(n)) {
      composite_count++;
    }
  }
  return composite_count;
}

TEST(PrimeTestingTest, JacobiSymbol) {
  // NOTE: pow(2, 16) - 1's factors are (3, 1), (5, 1), (17, 1), (257, 1)
  EXPECT_EQ(Gcd(pow(2, 16) - 1, 17 * 17 * 250), 17 * 5);

  // cf: https://en.wikipedia.org/wiki/Jacobi_symbol
  EXPECT_EQ(JacobiSymbol(20, 51), 1);
  EXPECT_EQ(JacobiSymbol(21, 51), 0);
  EXPECT_EQ(JacobiSymbol(22, 51), -1);

  int b = 51;
  vector<int8_t> QRs(b, -1);
  for (int a = 0; a < 51; a++) {
      if (Gcd(a, b) != 1) {
          QRs[a] = 0;
      } else {
          QRs[(a * a) % b] = 1;
      }
  }
  for (int a = 0; a < 51; a++) {
    // Actually, they don't necessarily coinside.
    // Quandratic residue =/=> JacobiSymbol = 1
    // EXPECT_EQ(JacobiSymbol(a, 51), QRs[a]);
  }
}

TEST(PrimeTestingTest, IsCompositeTrials) {
  // FIXME
  // int mersenne_prime = pow(2, 17) - 1;
  // EXPECT_EQ(IsCompositeTrials(mersenne_prime, 10), 0);

  int liar_count = 0;
  int num_trials = 1000;
  int mersenne_composite = pow(2, 11) - 1;
  // NOTE: Somehow this mersenne number has no liar (TODO: is it theorem or bug?)
  // int mersenne_composite = pow(2, 23) - 1;
  EXPECT_GT(IsCompositeTrials(mersenne_composite, 1000), 500);
}
