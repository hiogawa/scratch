#include <pybind11/pybind11.h>

int add(const int i, const int j) {
  return i + j;
}

PYBIND11_MODULE(misc_pybind11, m) {
  m.doc() = "pybind11 example plugin";
  m.def("add", &add, "A function which adds two numbers");
}

//
// Test:
// python -c 'import build.misc_pybind11 as m; print(m.add(1, 2))'
//
