cmake_minimum_required(VERSION 3.0.0)
project(scratch LANGUAGES CXX)

set(CMAKE_CXX_COMPILER clang++)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

find_package(Boost 1.70 REQUIRED)
find_package(PkgConfig)
pkg_search_module(GTEST REQUIRED gtest)
pkg_search_module(GBENCH REQUIRED benchmark)

#
# pybind11 (cf. https://github.com/pybind/pybind11/blob/master/tools/pybind11Tools.cmake)
#
#
execute_process(
  COMMAND python -m pybind11 --includes
  OUTPUT_VARIABLE pybind11_includes
  RESULT_VARIABLE pybind11_failed
  ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
if(pybind11_failed)
  message(STATUS "pybind11 not found. pybind11 setup will be skipped.")
else()
  execute_process(
    COMMAND python-config --extension-suffix
    OUTPUT_VARIABLE python_extension_suffix
    OUTPUT_STRIP_TRAILING_WHITESPACE)
  add_library(misc_pybind11 MODULE misc_pybind11.cc)
  set_target_properties(
    misc_pybind11 PROPERTIES PREFIX "" SUFFIX ${python_extension_suffix})
  separate_arguments(pybind11_includes)
  target_compile_options(misc_pybind11 PRIVATE ${pybind11_includes})
endif()

#
# all_src
#
set(all_src
  misc.cc
  primes.cc
  prime_testing.cc
  fft.cc
  misc_boost.cc
)


# TODO: avoid compiling same file twice for two executables

#
# main_gtest
#
add_executable(main_gtest main_gtest.cc ${all_src})
target_link_libraries(main_gtest
  ${GTEST_LDFLAGS}
  ${GBENCH_LDFLAGS}
)
target_compile_options(main_gtest PUBLIC
  ${GTEST_LDFLAGS}
  ${GBENCH_CFLAGS}
)
target_include_directories(main_gtest PRIVATE
  ${Boost_INCLUDE_DIRS}
)

# #
# # main_benchmark
# #
# add_executable(main_benchmark main_benchmark.cc ${all_src})
# target_link_libraries(main_benchmark
#   ${GTEST_LDFLAGS}
#   ${GBENCH_LDFLAGS}
# )
# target_compile_options(main_benchmark PUBLIC
#   ${GTEST_CFLAGS}
#   ${GBENCH_CFLAGS}
# )
# target_include_directories(main_gtest PRIVATE
#   ${Boost_INCLUDE_DIRS}
# )
