#include <cstdlib>
#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <algorithm>

using namespace std;

static size_t NUM_OF_TASKS = 1000;
static vector<int>* TASKS = nullptr;
static vector<int>* DONE_TASKS1 = nullptr;
static vector<int>* DONE_TASKS2 = nullptr;
static size_t TASKS_NEXT = 0;

int takeTask() {
  if (TASKS_NEXT == NUM_OF_TASKS) {
    return -1;
  } else {
    // segfault is probabale
    int task = (*TASKS)[TASKS_NEXT];
    TASKS_NEXT += 1;
    return task;
  }
}

void threadJob(vector<int>* done_tasks) {
  while (true) {
    int task = takeTask();
    if (task == -1) {
      break;
    }
    (*done_tasks).push_back(task);
  }
}

int main() {
  // Initialize shared tasks
  TASKS = new vector<int>(NUM_OF_TASKS);
  for (size_t i = 0; i < NUM_OF_TASKS; i++) {
    (*TASKS)[i] = static_cast<int>(i);
  }
  DONE_TASKS1 = new vector<int>(0);
  DONE_TASKS2 = new vector<int>(0);

  // Spawn threads
  std::thread t1(threadJob, DONE_TASKS1);
  std::thread t2(threadJob, DONE_TASKS2);
  t1.join();
  t2.join();

  // Check which task is duplicated/missed
  vector<int> done_tasks;
  done_tasks.insert(done_tasks.end(), (*DONE_TASKS1).begin(), (*DONE_TASKS1).end());
  done_tasks.insert(done_tasks.end(), (*DONE_TASKS2).begin(), (*DONE_TASKS2).end());
  sort(done_tasks.begin(), done_tasks.end());

  auto last = done_tasks.end();
  auto dups = adjacent_find(done_tasks.begin(), last);
  if (dups != last) {
    cout << *dups << endl;
  }

  // Finish off
  delete TASKS;
  delete DONE_TASKS1;
  delete DONE_TASKS2;
  return 0;
}
