#!/bin/bash

COMMAND="${1}"; shift;
case "${COMMAND}" in
  b|build)
    cmake -S . -B build -G Ninja "${@}" && ninja -C build
  ;;
  clean)
    rm -rf build
  ;;
  ls|list)
    cmake -S . -B build -G Ninja && ninja -C build && ./build/main_gtest --gtest_list_tests
  ;;
  f|filter)
    cmake -S . -B build -G Ninja && ninja -C build && ./build/main_gtest "--gtest_filter=${@}"
  ;;
  bench)
    cmake -S . -B build -G Ninja && ninja -C build && ./build/main_benchmark "${@}"
  ;;
  --)
    cmake -S . -B build -G Ninja && ninja -C build && "${@}"
  ;;
esac
