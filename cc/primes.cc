#include <cmath>
#include <cassert>
#include <vector>
#include <memory>
#include <gtest/gtest.h>
using namespace std;

unique_ptr<vector<bool>> PrimeSieve(uint32_t n) {
  assert(n >= 2);
  auto sieve = make_unique<vector<bool>>(n + 1, true);
  (*sieve)[0] = (*sieve)[1] = false;
  for (auto k = 2; k * k <= n; k++) {
    if ((*sieve)[k]) {
      for (auto l = 0; ; l++) {
        auto composite = k * (k + l);
        if (composite > n) { break; }
        (*sieve)[composite] = false;
      }
    }
  }
  return sieve;
}

TEST(PrimeSieveTest, A) {
  auto n = pow(2, 20) - 1;
  auto sieve = PrimeSieve(n);
  EXPECT_EQ((*sieve)[pow(2, 11) - 1], false);
  EXPECT_EQ((*sieve)[pow(2, 13) - 1], true);
}
