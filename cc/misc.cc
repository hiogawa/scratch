#include <complex>
#include <numeric>
#include <random>

#include <gtest/gtest.h>
#include <boost/format.hpp>

using namespace std;
using boost::format;

const float pi = acos(-1);

// complex
TEST(MiscTest, A) {
  auto i = complex<float>(0, 1);
  cout << exp(i * 2.f * pi) << endl;
}

// numeric_limits
TEST(MiscTest, B) {
  using nl = numeric_limits<float>;
  cout
      << nl::digits << "\t"
      << nl::max_exponent << "\t"
      << nl::min_exponent << "\t"
      << endl;
}

// Integer-to-Float conversion with loss of precision
TEST(MiscTest, C) {
  uint32_t i =  0xffffffu;
  for (uint32_t j = 0; j < 10; j++) {
    cout << i + j << " => " << fixed << static_cast<float>(i + j) << endl;
  }
}

// Dump float as bits
TEST(MiscTest, D) {
  auto f = static_cast<float>(0xffffffu);
  auto f_ = reinterpret_cast<uint32_t &>(f);
  for (auto i = 31; i >= 0; i--) {
    cout << (((f_ & (1u << i)) == 0) ? "0" : "1");
  }
  cout << endl;
}

// Arithmetic with infinity
TEST(MiscTest, E) {
  cout << ":: numeric_limits<float>::XXX" << endl;
  cout <<
    numeric_limits<float>::has_infinity << endl <<
    numeric_limits<float>::has_quiet_NaN << endl <<
    numeric_limits<float>::has_signaling_NaN << endl << endl;

  cout << ":: numeric_limits<double>::infinity" << endl;
  auto inf = numeric_limits<double>::infinity();
  cout << inf << endl;
  cout << -inf << endl;

  cout << ":: binary operation" << endl;
  cout << inf + (-inf) << endl;
  cout << (-inf) + inf << endl;
  cout << inf - (-inf) << endl;
  cout << (-inf) - inf << endl;
  cout << inf * (-inf) << endl;
  cout << (-inf) * inf << endl;
  cout << inf / (-inf) << endl;
  cout << (-inf) / inf << endl;
}

// array
TEST(MiscTest, F) {
  //
  // size at compile-time (allocation on stack)
  //
  array<float, 1 << 10 > A1;

  constexpr auto N = 1 << 10;
  array<float, N> A2;

  float A3[N];
  float A4[N][N];

  //
  // size at runtime (allocation on heap)
  //
  auto M = 1 << 10;

  auto A5 = new float[M];
  delete[] A5;

  // - 2nd axis has to be constexpr
  // auto A6 = new float[M][M]; // <- compile error
  auto A6 = new float[M][N];
  delete[] A6;

  {
    auto A = new float[M * M];
    auto B = (float**)(A);
    auto C = reinterpret_cast<float**>(A);
    auto i = 1 << 2;
    auto j = 1 << 3;
    cout << A[i * M + j] << endl;
    cout << B[i] << endl;
    // NOTE: doesn't work that way
    // cout << B[i][j] << endl;
    // cout << C[i][j] << endl;
    // EXPECT_EQ(A[i * M + j], B[i][j]);
    // EXPECT_EQ(B[i][j], C[i][j]);
    delete[] A;
  }
}

// Matrix as "Cheap value" container (aka CopyConstructible, CopyAssignable)
template <typename T>
class Matrix {
  public:
  // Uninitialized matrix
  Matrix() :
    M(0), N(0),
    offset_i(0), offset_j(0),
    stride_i(0), stride_j(0),
    base(nullptr) {}

  // Initialization with allocation
  Matrix(size_t M, size_t N) :
    M(M), N(N),
    offset_i(0), offset_j(0),
    stride_i(N), stride_j(1),
    base(shared_ptr<T[]>(new T[M * N])) {}

  // Copy constructor as copy assignment
  Matrix(const Matrix& other) { *this = other; }

  // Copy assignment
  // - "Implicitly-declared copy assignment operator" should work.
  //   cf. https://en.cppreference.com/w/cpp/language/copy_assignment
  Matrix& operator=(const Matrix& other) = default;

  // setter/getter via lvalue reference
  inline T& data(size_t i, size_t j) {
    auto index = stride_i * (offset_i + i) + stride_j * (offset_j + j);
    return static_cast<T &>(base.get()[index]);
  }

  void fill_uniform_random() {
    mt19937 generator(0);
    for (size_t i = 0; i < M; i++) {
      for (size_t j = 0; j < N; j++) {
        data(i, j) = uniform_real_distribution<T>(0, 1)(generator);
      }
    }
  }

  void print(ostream& os = cout) {
    for (size_t i = 0; i < M; i++) {
      for (size_t j = 0; j < N; j++) {
        os << data(i, j) << " ";
      }
      os << endl;
    }
  }

  Matrix copy() {
    Matrix other = *this;
    other.base = shared_ptr<T[]>(new T[M * N]);
    for (size_t i = 0; i < M; i++) {
      for (size_t j = 0; j < N; j++) {
        other.data(i, j) = data(i, j);
      }
    }
    return other;
  }

  //
  // TODO: view
  //
  // Matrix view(size_t i, size_t j) {
  //   auto other = *this;
  //   other.offset_i = i;
  //   other.offset_j = j;
  //   return other;
  // }

  shared_ptr<T[]> base;
  size_t M, N;
  size_t offset_i, offset_j;
  size_t stride_i, stride_j;
};

//
// Gauss elim. without pivoting (aka LR decomp.)
//
template <typename T>
void lr_decomp_template(Matrix<T> A) {
  assert(A.M == A.N);
  for (size_t k = 0; k < A.M - 1; k++) {
    auto a = A.data(k, k);
    assert(abs(a) > numeric_limits<T>::epsilon());

    for (size_t i = k + 1; i < A.M; i++) {
      auto b = A.data(i, k) / a;

      // Elimination
      for (size_t j = k + 1; j < A.N; j++) {
        A.data(i, j) -= b * A.data(k, j);
      }

      // Save L's column in the unused space
      A.data(i, k) = b;
    }
  }
}

// Matrix
TEST(MiscTest, G1) {
  auto M = 1 << 2;
  auto N = 1 << 3;
  Matrix<float> A(M, N);
  for (auto i = 0; i < M; i++) {
    for (auto j = 0; j < N; j++) {
      A.data(i, j) = static_cast<float>(i * N + j);
    }
  }
  for (auto i = 0; i < M; i++) {
    for (auto j = 0; j < N; j++) {
      cout << static_cast<int>(A.data(i, j)) << " ";
    }
    cout << endl;
  }
  A.fill_uniform_random();
  A.print();
}

// LR decomposition
TEST(MiscTest, G2) {
  auto M = 6;
  Matrix<double> A(M, M);
  A.fill_uniform_random();

  auto LR = A.copy();
  lr_decomp_template(LR);

  cout << ":: A.print()" << endl;
  A.print();
  cout << endl;

  cout << ":: LR.print()" << endl;
  LR.print();
  cout << endl;

  Matrix<double> B(M, M);

  // Restore A from LR
  for (size_t i = 0; i < M; i++) {
    for (size_t j = 0; j < M; j++) {
      B.data(i, j) = 0;
      for (size_t k = 0; k <= min(i, j); k++) {
        auto l = ((k < i) ? LR.data(i, k) : 1);
        auto r = LR.data(k, j);
        B.data(i, j) += l * r;
      }
    }
  }

  cout << ":: B.print()" << endl;
  B.print();
  cout << endl;
}


// Initialization example
using std::cout;
class C {
  public:
  C() { cout << "C()" << endl; }
  ~C() { cout << "~C()" << endl; }

  // Copy constructor
  C(const C& other) {
    cout << "C(const C& other)" << endl;
  }

  // Copy assignment
  C& operator=(const C& other) {
    cout << "C& operator=(const C& other)" << endl;
    return static_cast<C&>(*(new C()));
  }
};

TEST(MiscTest, H) {
  // direct initialization on stack
  {
    cout << ":: C x" << endl;
    C x;

    cout << ":: C y({})" << endl;
    C y({});
  }

  // direct initialization on heap
  {
    cout << ":: auto x = new C()" << endl;
    auto x = new C();
    delete x;
  }

  // direct initialization
  {
    cout << ":: C()" << endl;
    C();
  }

  // direct initialization
  {
    cout << ":: auto x = C()" << endl;
    auto x = C();
  }

  // direct initialization
  {
    cout << ":: C x{ C() };" << endl;
    C x{ C() };
  }

  // copy initialization
  {
    cout << ":: C x({ C() });" << endl;
    C x({ C() });
  }


  // copy assignment
  {
    cout << ":: C x" << endl;
    C x;

    cout << ":: C y" << endl;
    C y;

    cout << ":: x = y" << endl;
    x = y;
  }

  // smart pointer
  {
    cout << "auto p = make_shared<C>();" << endl;
    auto p = make_shared<C>();
  }

  // smart pointer (array)
  {
    cout << "auto p = shared_ptr<C[]>(new C[3]);" << endl;
    auto p = shared_ptr<C[]>(new C[3]);

    // This syntax not supported (maybe from C++20 ?)
    // auto p = make_shared<C[]>(3);

    // This is not implemented
    // (or probably it's impossible to implement because "array" type is essential for proper deletion)
    // auto p = shared_ptr<C*>(new C[3]);
  }
}
