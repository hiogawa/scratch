Usage

```
# Building
bash scripts.sh b
bash scripts.sh b -DCMAKE_BUILD_TYPE=Debug

# List test
bash scripts.sh l

# Run test with filter
bash scripts.sh f 'Misc*.B'

# Run Benchmark
bash scripts.sh bench

# Debugger
coredumpctl debug --debugger=lldb
```
