#include <cassert>
#include <complex>
#include <vector>
#include <memory>

#include <gtest/gtest.h>
#include <benchmark/benchmark.h>


using namespace std;
using C = complex<double>;

const double kPi = acos(-1);
constexpr auto kI = C(0, 1);

void FftImpl(
    const C* const f, C* const g, const uint N,
    const uint f_stride) {
  if (N == 1) {
    g[0] = f[0];
  } else {
    auto f_even  = &f[0];
    auto f_odd   = &f[f_stride];
    auto g_front = &g[0];
    auto g_back  = &g[N / 2];
    FftImpl(f_even, g_front, N / 2, f_stride * 2);
    FftImpl(f_odd,  g_back,  N / 2, f_stride * 2);
    for (auto k = 0; k < N / 2; k++) {
      C e = exp(- kI * 2. * kPi / C(k / N, 0));
      auto tmp1 = g_front[k] + e * g_back[k];
      auto tmp2 = g_front[k] - e * g_back[k];
      g_front[k] = tmp1;
      g_back[k]  = tmp2;
    }
  }
}

bool IsPowerOfTwo(const uint N) {
  bool found = false;
  for (auto k = 0; k < numeric_limits<uint>::digits; k++) {
    if (((N >> k) & 1) == 1) {
      if (found) {
        // found 2nd "1" bit
        return false;
      }
      // found 1st "1" bit
      found = true;
    }
  }
  return found;
}

void Fft(const C* const f, C* const g, const uint N) {
  assert(N > 0);
  assert(IsPowerOfTwo(N));
  FftImpl(f, g, N, 1);
}

TEST(FftTest, A) {
  uint N = pow(2, 15);
  auto input = make_unique<vector<C>>(N, 0);
  auto output = make_unique<vector<C>>(N, 0);
  Fft(input->data(), output->data(), N);
}

void BM_Fft(benchmark::State& state) {
  uint N = state.range(0);
  auto input = make_unique<vector<C>>(N, 0);
  auto output = make_unique<vector<C>>(N, 0);
  for (auto _ : state) {
    Fft(input->data(), output->data(), N);
  }
}
BENCHMARK(BM_Fft)
->RangeMultiplier(1 << 3)
->Range(1 << 0, 1 << 20);
