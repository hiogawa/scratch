YTSUB https://s3-ap-northeast-1.amazonaws.com/hiogawa-assets/misc/ytsub.html


References

- https://developers.google.com/youtube/iframe_api_reference
- https://github.com/youtube/api-samples/tree/master/player
- https://github.com/rg3/youtube-dl/blob/master/youtube_dl/extractor/youtube.py#L1237

Examples URLs

- https://www.youtube.com/watch?v=4fpOXK3Znxs
- https://video.google.com/timedtext?hl=en&type=list&v=IteFQdZgGFQ
- https://www.youtube.com/api/timedtext?lang=ru&v=IteFQdZgGFQ&fmt=ttml
