var YT_PLAYER = null;
var CONFIG_P = null;
var DIV_text_list = document.getElementById('text_list');
var DIV_translate_link = document.getElementById('translate_link');
var LOOPING = null;

function onYouTubeIframeAPIReady() {
    let video_id = document.getElementById('video_id').value;
    YT_PLAYER = new YT.Player('player', {
        height: '480',
        width: '853',
        playerVars: {
            cc_lang_pref: 'ru',
            cc_load_policy: 1
        },
        events: {
            'onReady': () => {
                LoadVideo(video_id);
                window.setInterval(ProcessPeriodically, 500);
            }
        }
    });
    // DIV_text_list.addEventListener("mouseup", ShowTranslateLink);
    // document.addEventListener("selectionchange", ShowTranslateLink);
    // DIV_translate_link.hidden = false;
}

function TimeText2Number(text) {
    let f = (h_m_s_f) => ((h_m_s_f[0] * 60 + h_m_s_f[1]) * 60) + h_m_s_f[2] + h_m_s_f[3] * 0.001;
    let begin = text.split(' ')[0].split(/[\:\.]/).map(Number);
    let end = text.split(' ')[2].split(/[\:\.]/).map(Number);
    return [f(begin), f(end)];
}

function GetConfigP(video_id) {
    let p = fetch('https://cors-anywhere.herokuapp.com/https://www.youtube.com/watch?v=' + video_id);
    p = p.then(r => r.text());
    p = p.then(text => {
        let mobj = text.match(/;ytplayer\.config\s*=\s*({.+?});ytplayer/);
        return JSON.parse(mobj[1]);
    });
    return p;
}

function GetSubtitleP(video_id, lang) {
    let p = fetch('https://www.youtube.com/api/timedtext?fmt=ttml&lang=' + lang + '&v=' + video_id).then(r => r.text());
    p = p.then(text => {
        if (text.length == 0) {
            throw 0;
        } else {
            return text;
        }
    })
    p = p.catch(() => {
        return CONFIG_P.then((config) => {
            let player_response = JSON.parse(config.args.player_response);
            let url = player_response.captions.playerCaptionsTracklistRenderer.captionTracks[0].baseUrl;
            return fetch(url + '&fmt=ttml&tlang=' + lang).then(r => r.text());
        })
    });
    return p;
}

function LoadSubtitle(video_id) {
    let ru_text = GetSubtitleP(video_id, 'ru');
    let en_text = GetSubtitleP(video_id, 'en');

    Promise.all([ru_text, en_text])
        .then(ru_en => {
            Array.from(DIV_text_list.getElementsByClassName('text_element')).forEach(c => c.remove());
            let xml_doc_ru = (new DOMParser()).parseFromString(ru_en[0], 'text/xml');
            let xml_doc_en = (new DOMParser()).parseFromString(ru_en[1], 'text/xml');

            ru_list = Array.from(xml_doc_ru.getElementsByTagName('p'));
            en_list = Array.from(xml_doc_en.getElementsByTagName('p'));

            ru_list.forEach((p) => {
                let DIV_text_element = document.createElement('div');
                DIV_text_element.className = 'text_element';
                DIV_text_element.innerHTML = '\
                    <div>\
                        <div class="text_header">\
                            <div class="time_pointer time_pointer--seek" title="Jump to here">@</div>\
                            <div class="time_pointer time_pointer--loop" title="Loop it">#</div>\
                            <div class="time"></div>\
                        </div>\
                        <div class="text">\
                            <div class="russian"></div>\
                            <div class="english"></div>\
                        </div>\
                    </div>\
                '
                DIV_text_element.getElementsByClassName('time')[0].innerText = p.attributes['begin'].value + ' - ' + p.attributes['end'].value;
                DIV_text_element.getElementsByClassName('russian')[0].innerText = p.textContent;

                let p_en = en_list.find(q => q.attributes['begin'].value == p.attributes['begin'].value);
                if (p_en) {
                    DIV_text_element.getElementsByClassName('english')[0].innerText = p_en.textContent;
                }
                DIV_text_list.appendChild(DIV_text_element);

                let seek_video = () => {
                    LOOPING = null;
                    let begin_end_time = TimeText2Number(DIV_text_element.getElementsByClassName('time')[0].textContent);
                    YT_PLAYER.seekTo(begin_end_time[0]);
                };
                DIV_text_element.getElementsByClassName('time_pointer--seek')[0].addEventListener('click', seek_video);

                let loop_subtitle = () => {
                    LOOPING = (LOOPING == DIV_text_element) ? null : DIV_text_element;
                };
                DIV_text_element.getElementsByClassName('time_pointer--loop')[0].addEventListener('click', loop_subtitle);
            });
        });
}

function LoadVideo() {
    let video_id = document.getElementById('video_id').value;
    if (video_id.length != 11) {
        let url = new URL(video_id);
        if (url.hostname == 'youtu.be') {
            video_id = url.pathname.substr(1);
        } else {
            video_id = url.search.match(/v=(.{11})/)[1];
        }
    }
    YT_PLAYER.loadVideoById(video_id);
    CONFIG_P = GetConfigP(video_id);
    LoadSubtitle(video_id);
}

function SetSize(width, height) {
    YT_PLAYER.setSize(width, height);
}

function ProcessPeriodically() {
    Array.from(DIV_text_list.getElementsByClassName('text_element')).forEach(c => {
        c.classList.remove('highlighted');
        c.classList.remove('looped');
    });
    if (LOOPING) {
        LoopSubtitle();
    } else {
        HighlightCurrentSubtitle();
    }
}

function HighlightCurrentSubtitle() {
    if (YT_PLAYER.getPlayerState() == YT.PlayerState.PLAYING ||
        YT_PLAYER.getPlayerState() == YT.PlayerState.PAUSED ||
        YT_PLAYER.getPlayerState() == YT.PlayerState.BUFFERING) {
        let current_time = YT_PLAYER.getCurrentTime();
        let DIV_hightlight;
        // for (let e of Array.from(DIV_text_list.getElementsByClassName('text_element'))) {
        //     let begin_end_time = TimeText2Number(e.getElementsByClassName('time')[0].textContent);
        //     if (current_time < begin_end_time[1]) {
        //         DIV_hightlight = e;
        //         break;
        //     }
        // }
        // This works better for auto caption time stamps
        for (let e of Array.from(DIV_text_list.getElementsByClassName('text_element')).reverse()) {
            let begin_end_time = TimeText2Number(e.getElementsByClassName('time')[0].textContent);
            if (begin_end_time[0] < current_time) {
                DIV_hightlight = e;
                break;
            }
        }
        if (DIV_hightlight) {
            DIV_hightlight.classList.add('highlighted');
        }
    }
}

function LoopSubtitle() {
    if (YT_PLAYER.getPlayerState() == YT.PlayerState.PLAYING ||
        YT_PLAYER.getPlayerState() == YT.PlayerState.PAUSED ||
        YT_PLAYER.getPlayerState() == YT.PlayerState.BUFFERING) {
        let current_time = YT_PLAYER.getCurrentTime();
        let begin_end_time = TimeText2Number(LOOPING.getElementsByClassName('time')[0].textContent);
        if (current_time < begin_end_time[0] || begin_end_time[1] < current_time) {
            YT_PLAYER.seekTo(begin_end_time[0]);
        }
        LOOPING.classList.add('looped');
    }
}

function ShowTranslateLink() {
    let s = document.getSelection();
    let r = s.rangeCount == 1 && s.getRangeAt(0);
    if (r &&
        r.startContainer.nodeName == '#text' &&
        r.startContainer.parentNode.className == 'russian' &&
        r.startContainer == r.endContainer &&
        r.startOffset < r.endOffset) {
        let query = r.startContainer.textContent.substr(r.startOffset, r.endOffset - r.startOffset);
        let translate_url = 'https://translate.google.com/#ru/en/' + query;

        // Forget the sticky link and just ruthlessly open the url
        window.open(translate_url, 'popup', 'left=1000,width=840,height=630')

        // DIV_translate_link.getElementsByTagName('a')[0].href = translate_url;
        // // making it stick to the scrolling list
        // DIV_translate_link.style.left = r.getClientRects()[0].left - DIV_text_list.getClientRects()[0].left + 'px';
        // DIV_translate_link.style.top = r.getClientRects()[0].bottom - DIV_text_list.getClientRects()[0].top + 'px';
        // DIV_translate_link.classList.remove('hidden');
    } else {
        // DIV_translate_link.classList.add('hidden');
    }
}
