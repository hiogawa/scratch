#include <QObject>
#include <QList>
#include <QByteArray>
#include <QWebSocketServer>
#include <QWebSocket>

class EchoServer : public QObject
{
  Q_OBJECT

public:
  explicit EchoServer(quint16 port, QObject *parent = Q_NULLPTR);
  ~EchoServer();

signals:
  void closed();

private slots:
  void onNewConnection();
  void processTextMessage(QString message);
  void processBinaryMessage(QByteArray message);
  void socketDisconnected();

private:
  QWebSocketServer *m_pWebSocketServer;
  QList<QWebSocket *> m_clients;
};
