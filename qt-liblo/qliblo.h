#include <QObject>
#include <QList>
#include <QByteArray>
#include <QWebSocketServer>
#include <QWebSocket>

class QLibloServer : public QObject
{
  Q_OBJECT

public:
  explicit QLibloServer(QString url, QObject *parent = Q_NULLPTR);
  ~QLibloServer();

signals:

private slots:

private:
  QWebSocketServer *m_pWebSocketServer;
  QList<QWebSocket *> m_clients;
};
