Following https://docs.microsoft.com/en-us/visualstudio/msbuild/walkthrough-creating-an-msbuild-project-file-from-scratch
to know better unreal engine build system.

```
$ xbuild

>>>> xbuild tool is deprecated and will be removed in future updates, use msbuild instead <<<<

XBuild Engine Version 14.0
Mono, Version 5.0.0.0
Copyright (C) 2005-2013 Various Mono authors

Build started 10/20/2017 7:30:44 PM.
__________________________________________________
Project "/home/hiogawa/code/hiogawa/scratch/try-csharp/helloworld.csproj" (default target(s)):
        Target Build:
                Created directory "Bin/"
                Tool /usr/lib/mono/4.5/csc.exe execution started with arguments:  /out:Bin/MSBuildSample.exe helloworld.cs
                Microsoft (R) Visual C# Compiler version 2.0.0.61404
                Copyright (C) Microsoft Corporation. All rights reserved.
Done building project "/home/hiogawa/code/hiogawa/scratch/try-csharp/helloworld.csproj".

Build succeeded.
         0 Warning(s)
         0 Error(s)

Time Elapsed 00:00:01.4196280

$ mono Bin/MSBuildSample.exe
Hello, World!
```
