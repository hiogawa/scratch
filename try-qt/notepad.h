#include <QMainWindow>

namespace Ui {
  class Notepad;
}

class Notepad : public QMainWindow
{
  Q_OBJECT

public:
  explicit Notepad(QWidget *parent = 0);
  ~Notepad();

private slots:
    void on_quitButton_clicked();

private:
  Ui::Notepad *ui;
};
