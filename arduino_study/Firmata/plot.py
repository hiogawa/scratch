import pandas as pd
import time

csv_path = '/home/hiogawa/code/hiogawa/scratch/arduino_study/Firmata/analog.csv'
df = pd.read_csv(csv_path, header=None, names=['time', 'A0'])
while True:
    df.tail(10000).plot(x='time')
    time.sleep(1)
