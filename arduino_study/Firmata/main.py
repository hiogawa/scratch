import asyncio
import time
from pymata_aio.pymata_core import PymataCore
from pymata_aio.constants import Constants

PORT = '/dev/ttyACM0'
LED_BUILTIN = 13
PIN_A0 = 14 - 14

async def blink(board : PymataCore):
    async def setup():
        await board.set_pin_mode(LED_BUILTIN, Constants.OUTPUT)

    async def loop():
        await board.digital_write(LED_BUILTIN, 1)
        await board.sleep(1.0)
        await board.digital_write(LED_BUILTIN, 0)
        await board.sleep(1.0)

    await setup()
    while True:
        await loop()

async def log_analog(board : PymataCore):
    csv = open('analog.csv', 'w')
    init_time = time.time()

    async def setup():
        await board.set_pin_mode(PIN_A0, Constants.ANALOG)

    async def loop():
        value = await board.analog_read(PIN_A0)
        csv.write("{},{}\n".format(time.time() - init_time, value))
        await board.sleep(10 ** (-3))

    await setup()
    while True:
        await loop()

async def plot_log():
    pass

async def main():
    board = PymataCore(com_port=PORT)
    await board.start_aio()
    await asyncio.gather(blink(board), log_analog(board))

if __name__ == '__main__':
    asyncio.run(main())
