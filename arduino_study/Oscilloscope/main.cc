#include <Arduino.h>

const int analogInPin = A0;
const int analogOutPin = 3;
static_assert digitalPinHasPWM(analogOutPin);

void setup() {
  pinMode(analogOutPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(analogInPin);
  int outputValue = map(sensorValue, 0, 1023, 0, 255);
  analogWrite(analogOutPin, outputValue);

  Serial.print("sensor = ");
  Serial.print(sensorValue);
  Serial.print("\t output = ");
  Serial.println(outputValue);

  delay(1);
}
