#include <Arduino.h>

const int out_pin = 2;
const int key_interval = 1000;
unsigned long last_time;

// Generated from python
// [int(440 * (2 ** (n / 12))) for n in range(12)]
const int keys[12] = { 440, 466, 493, 523, 554, 587, 622, 659, 698, 739, 783, 830 };
#define _A 0
#define _B 1
#define _C 2
#define _D 3
#define _E 4
#define _F 5
#define _G 6
const int play[4] = { _A, _C, _E, _C };
const int play_length = 4;
int play_index = 0;


void setup() {
  last_time = millis();
}

void loop() {
  unsigned long now = millis();
  if (now > last_time + key_interval) {
    tone(out_pin, play[play_index]);
    last_time = now;
    play_index = (play_index + 1) % play_length;
  }
}
