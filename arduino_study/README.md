# TODO

- firmata smooth shutdown

# Overview

- arduino-cli to download libraries
- arduino-mk to build and upload (because it's much simpler what it does)
- core and avr tools are from archlinux repo


# Building

```
$ make Blink
$ make Blink t=upload
```

```
$ arduino-cli lib install hd44780
$ make LCD_HelloWorld
$ make LCD_HelloWorld t=upload
```


# Setup

Tools installation

```
$ yaourt -S arduino-mk arduino-cli # arduino-avr-core, avr-gcc, avr-libc is installed as dependency of arduino-mk
```


Modify permission to access device file (e.g. /dev/ttyACM0)

```
$ usermod -a -G uucp $USER
```


# References

- https://www.arduino.cc/en/Hacking/HomePage
- https://www.arduino.cc/en/Tutorial/BuiltInExamples
    - https://github.com/arduino/Arduino/tree/master/build/shared/examples
- https://github.com/arduino/arduino-cli
- https://github.com/arduino/Arduino/blob/master/build/shared/manpage.adoc
