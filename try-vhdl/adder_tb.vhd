entity adder_tb is
end adder_tb;

architecture Behavioral of adder_tb is
   component adder
     port (i0, i1 : in bit; ci : in bit; s : out bit; co : out bit);
   end component;

   signal TB_i0, TB_i1, TB_ci, TB_s, TB_co : bit;
begin
    mapping: adder port map(TB_i0, TB_i1, TB_ci, TB_s, TB_co);
    process
    begin
      TB_i0 <= '0';
      TB_i1 <= '0';
      TB_ci <= '0';
      wait for 1 ns;
      TB_ci <= '1';
      wait for 1 ns;
      TB_i1 <= '1';
      wait for 1 ns;
      TB_ci <= '0';
      wait for 1 ns;

      TB_i0 <= '1';
      TB_i1 <= '0';
      TB_ci <= '0';
      wait for 1 ns;
      TB_ci <= '1';
      wait for 1 ns;
      TB_i1 <= '1';
      wait for 1 ns;
      TB_ci <= '0';
      wait for 1 ns;
    end process;
end Behavioral;
