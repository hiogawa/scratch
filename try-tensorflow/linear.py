import matplotlib.pyplot as plt
import numpy as np
import sympy as sp
import tensorflow as tf


def main():
    # Prepare samples
    n = 100
    w = 0.5
    b = 2.5
    xs = np.random.random(n) * 10
    ys = w * xs + b + np.random.normal(0, 0.5, n)

    # Equivalent implementations but using different layer of things
    # w__, b__ = solve_manually(xs, ys)
    # w__, b__ = solve_sp(xs, ys)
    w__, b__ = solve_tf_manually(xs, ys)
    # w__, b__ = solve_tf_optimizer(xs, ys)

    # Plotting
    xmin = ymin = 0
    xmax = ymax = 10
    plt.axis([xmin, xmax, ymin, ymax])
    plt.plot(xs, ys, 'ro', markersize=1)
    plt.plot([xmin, xmax], w * np.array([xmin, xmax]) + b, 'b')
    plt.plot([xmin, xmax], w__ * np.array([xmin, xmax]) + b__, 'g')
    plt.show()


INIT = [.3, -.3]
STEP = 0.0001


def solve_manually(xs, ys):
    n = xs.shape[0]
    v = np.array(INIT)
    X = np.vstack([xs, np.ones(n)])
    Y = ys

    # Score function
    def f(v):
        r = v.dot(X) - Y
        return r.dot(r)

    # Differential of f
    def diff_f(v):
        return - 2 * X.dot(Y) + 2 * v.dot(X.dot(X.T))

    for i in range(1024):
        v = v - STEP * diff_f(v)

    return v


def solve_sp(xs, ys):
    # Derive gradient using sympy's symbolic math
    n = xs.shape[0]
    v0, v1 = sp.symbols('v0, v1')
    v = sp.Matrix([[v0, v1]])
    X = sp.Matrix(np.vstack([xs, np.ones(n)]))
    Y = sp.Matrix([ys])

    r = v * X - Y
    f = (r * r.T)[0]
    diff_f = sp.Matrix([[f.diff(v0), f.diff(v1)]])

    def compute_diff_f(v):
        result = diff_f.subs({v0: v[0], v1: v[1]})
        return np.array(list(map(float, result)))

    v_ = np.array(INIT)
    for i in range(1024):
        v_ = v_ - STEP * compute_diff_f(v_)

    return v_


def solve_tf_manually(xs, ys):
    # Graph construction
    w_ = tf.Variable(INIT[0], dtype=tf.float32, name='w')
    b_ = tf.Variable(INIT[1], dtype=tf.float32, name='b')
    x_ = tf.placeholder(tf.float32, name='x')
    linear_model_ = w_ * x_ + b_
    y_ = tf.placeholder(tf.float32, name='y')
    loss_ = tf.reduce_sum(tf.square(linear_model_ - y_), name='loss')

    # # a. Expand `minimize`
    # opt = tf.train.GradientDescentOptimizer(STEP)
    # grads_and_vars = opt.compute_gradients(loss_, var_list=[w_, b_])
    # train_ = opt.apply_gradients(grads_and_vars)

    # # b. Expand `compute_gradients`
    # opt = tf.train.GradientDescentOptimizer(STEP)
    # dl_dw_, dl_db_ = tf.gradients(loss_, [w_, b_])
    # train_ = opt.apply_gradients([(dl_dw_, w_), (dl_db_, b_)])

    # # c. Expand `apply_gradients`
    # opt = tf.train.GradientDescentOptimizer(STEP)
    # dl_dw_, dl_db_ = tf.gradients(loss_, [w_, b_])
    # opt._prepare()
    # train_ = tf.group([opt._apply_dense(dl_dw_, w_), opt._apply_dense(dl_db_, b_)])

    # # d. Expand `_apply_dense`
    # dl_dw_, dl_db_ = tf.gradients(loss_, [w_, b_])
    # step_ = tf.constant(STEP)
    # train_ = tf.group([w_.assign(w_ - step_ * dl_dw_), b_.assign(b_ - step_ * dl_db_)])

    # TODO: Expand `gradients`
    dl_dw_, dl_db_ = tf.gradients(loss_, [w_, b_])
    step_ = tf.constant(STEP)
    train_ = tf.group([w_.assign(w_ - step_ * dl_dw_), b_.assign(b_ - step_ * dl_db_)], name='update')

    with tf.Session() as sess:
        writer = tf.summary.FileWriter("/run/user/1000/linear-tf", sess.graph)

        # TODO: Understand `run`
        sess.run(tf.global_variables_initializer())
        for i in range(1024):
            w__, b__, _ = sess.run([w_, b_, train_], {x_: xs, y_: ys})

        writer.close()

    return w__, b__


def solve_tf_optimizer(xs, ys):
    # Graph construction
    w_ = tf.Variable(INIT[0], dtype=tf.float32)
    b_ = tf.Variable(INIT[1], dtype=tf.float32)
    x_ = tf.placeholder(tf.float32)
    linear_model_ = w_ * x_ + b_
    y_ = tf.placeholder(tf.float32)
    loss_ = tf.reduce_sum(tf.square(linear_model_ - y_))
    train_ = tf.train.GradientDescentOptimizer(STEP).minimize(loss_)

    # Training
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    for i in range(1024):
        w__, b__, _, _ = sess.run([w_, b_, loss_, train_], {x_: xs, y_: ys})

    return w__, b__


if __name__ == '__main__':
    main()
