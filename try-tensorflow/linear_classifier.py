import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import datetime

# Linear classifier (or Perceptron)

class Plot:
    def __init__(self, xys0, xys1, xmin, xmax, ymin, ymax, w, b):
        self.count = 0
        self.timestamp = str(datetime.datetime.now().timestamp())
        self.xmin = xmin
        self.xmax = xmax

        self.fig = plt.gcf()
        self.ax = self.fig.gca()
        self.ax.axis([xmin, xmax, ymin, ymax])
        self.ax.plot(xys0[0], xys0[1], 'ro', markersize=1)
        self.ax.plot(xys1[0], xys1[1], 'bo', markersize=1)
        self.ax.plot([xmin, xmax], w * np.array([xmin, xmax]) + b, 'b')
        self.anim_line = self.ax.plot([], [], 'g')[0]

    def update(self, w__, b__):
        self.anim_line.set_data([self.xmin, self.xmax], w__ * np.array([self.xmin, self.xmax]) + b__)
        self.fig.savefig("plots/tmp_{}_{:04d}.png".format(self.timestamp, self.count))
        print('.', end='', flush=True)
        self.count += 1


def main():
    # Prepare samples
    n = 100
    w = 0.5
    b = 2.5
    xys = (np.random.random(2 * n) * 10).reshape((2, n))
    mask = w * xys[0] + b >= xys[1]
    xys0 = xys[:, mask]  # positive class
    xys1 = xys[:, ~mask] # negative class
    xmin = ymin = 0
    xmax = ymax = 10
    plot = Plot(xys0, xys1, xmin, xmax, ymin, ymax, w, b)

    # Two implementations with and without tensorflow
    w__, b__ = solve_perceptron(xys0, xys1, plot)
    # w__, b__ = solve_tf_optimizer(xs, ys, plot)


def solve_perceptron(xys0, xys1, plot):
    v = np.array([1., 1., 1.])

    def v2wb(v):
        return - v[[0, 2]] / v[1]

    # # Naively
    # xys0 = np.vstack([xys0, np.ones(xys0.shape[1])])
    # xys1 = np.vstack([xys1, np.ones(xys1.shape[1])])

    # for i in range(1024):
    #     pos_mask = v.dot(xys0) >= 0
    #     v = v + xys0.dot(~pos_mask)
    #     neg_mask = v.dot(xys1) < 0
    #     v = v - xys1.dot(~neg_mask)
    #     loss = pos_mask[~pos_mask].shape[0] + neg_mask[~neg_mask].shape[0]
    #     print("loss: {}".format(loss))
    #     if loss == 0:
    #         break

    # Flip one class to the other
    xys0 = np.vstack([xys0, np.ones(xys0.shape[1])])
    xys1 = np.vstack([xys1, np.ones(xys1.shape[1])])
    xys = np.hstack([xys0, -xys1])

    plot.update(*v2wb(v))

    while True:
        mask = v.dot(xys) >= 0
        v = v + xys.dot(~mask)
        plot.update(*v2wb(v))
        loss = mask[~mask].shape[0]
        if loss == 0:
            break

    return v2wb(v)


def solve_tf_optimizer(xys0, xys1):
    pass


if __name__ == '__main__':
    main()
