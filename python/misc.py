#!/usr/bin/env python
# coding: utf-8

get_ipython().system('jupyter nbconvert misc.ipynb --to python --no-prompt')


import IPython.display as ipyd
import ipywidgets as ipyw

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
plt.ioff()


#
# ipywidget animation example
#
play = ipyw.Play(
    interval=100,
    value=23,
    min=0,
    max=100,
)
slider = ipyw.IntSlider()
ipyw.jslink((play, 'value'), (slider, 'value'))
ipyd.display(play, slider)


#
# ipywidget "interact" example
#

fig, ax = plt.subplots(figsize=(6, 6))

@ipyw.interact(N=(10,200,10))
def __(*, N=50):
    u = np.random.rand(N)
    u_rad = 2 * np.pi * u
    cos = np.cos(u_rad)    
    sin = np.sin(u_rad)
    
    lim = np.array([-1, 1]) * 1.5
    ax.clear()
    ax.set(aspect=1, xlim=lim, ylim=lim)
    ax.grid()
    ax.scatter(cos, sin)
    ipyd.display(fig)


# Backward propagation of _solve_
# 
# ---
# 
# $
# \text{solve}: \text{Invertibles}(F^{n \times n}) \times F^{n \times k} \to F^{n \times k}
# $
# 
# $
# X = \text{solve}(A, B) = A^{-1} B
# $
# 
# where
# 
# $
# A \in \text{Invertibles}(F^{n \times n})
# $
# 
# $
# X, B \in F^{n \times k}
# $
# 
# 
# ---
# 
# For $f: F^{n \times k} \to F$, we write:
# 
# $
# G_{X, i, j} \equiv \mathbf{D}_{X_{i, j}}f(X)
# \in F^{n \times k}
# $
# 
# Then,
# 
# $
# \begin{align}
# \mathbf{D}_{B_{i, j}}f(X)
# &= \sum_{k, l} \mathbf{D}_{X_{k, l}}f(X) \;\; \mathbf{D}_{B_{i, j}}(A^{-1} B)_{k, l} \\
# &= \text{Tr}(G_{X}^T \; A^{-1} \; \delta_{i, j}) \\
# &= (G_{X}^T \; A^{-1})^T \\
# &= (A^{T})^{-1} \; G_{X}
# \end{align}
# $

# Convinient tricks
# 
# Define
# 
# $
# \Delta_{i, j} \in F^{n \times n} 
# $
# 
# s.t. 
# 
# $
# (\Delta_{i, j})_{k, l} \equiv
# \begin{cases}
# 1 &\text{if $i = k$ and $j = l$,} \\
# 0 &\text{o.w.}
# \end{cases}
# $
# 
# $
# \begin{align}
# \end{align}
# $
# 
# 1.
# 
# $
# \begin{align}
# \sum_{i, j} X_{i, j} Y_{i, j} 
# &= \text{Tr}(X^T Y) \\
# &= \text{Tr}(Y \; X^T) \\
# &= \text{Tr}(Y^T X) \\
# &= \text{Tr}(X \; Y^T)
# \end{align}
# $
# 
# 2.
# 
# $
# \begin{align}
# X_{i, j} = \text{Tr}(X^T \Delta_{i, j})
# \end{align}
# $
# 
# 3.
# 
# $
# \mathbf{D}_{X_{i, j}} X = \Delta_{i, j}
# $
# 
# 4.
# 
# $
# \mathbf{D}_{X_{i, j}} A \; X = A \; \Delta_{i, j}
# $
# 
# 5.
# 
# $
# \begin{align}
# \mathbf{D}_{X_{i, j}} f(A \; X)
# &= \sum_{k, l} \; \mathbf{D}_{Y, k, l} f(Y) \;\; \mathbf{D}_{X_{i, j}} (A \; X)_{k, l} \\
# &= \text{Tr}( (\mathbf{D}_{Y} f(Y))^T \;\; A \;\; \Delta_{i, j}) \\
# &= ((\mathbf{D}_{Y} f(Y))^T \;\; A)^T \\
# &= A^T \;\; \mathbf{D}_{Y} f(Y)
# \end{align}
# $

import torch

torch.arange(-1, 1, 10)


get_ipython().run_line_magic('pinfo', 'torch.arange')


import numpy as np

np.random.randn(1)


# BitGenerator (PCG64)
# Generator

# - Get BitGenerator (PCG64) state

# - Use bitgen interface directly

# what is seed_seq??


npr = np.random.default_rng()
npr.normal(size=(3, 5))


#
# lstsq example
# TODO:
# - what is QR here? where does QR come from?
# - what is z here?
#
import torch

A = torch.randn(5, 3)
b = torch.randn(5)
solution, QR = torch.lstsq(b, A)

x, z = torch.split(solution, A.size(1))
residue = A @ x.reshape(-1) - b

assert torch.allclose(torch.norm(residue), torch.norm(z))

A, b, solution, residue, QR


#
# lu (cf. torch.functional.lu_unpack)
#
w = torch.randn(5, 5)
lu, pivot = torch.lu(w)
w, lu, pivot


#
# lu_solve
#
b = torch.randn(5, 2)
x = torch.lu_solve(b, lu, pivot)
b, ' '*20, x, w @ x


# Latex Macros
# 
# $
# \newcommand{\iprod}[2] { \langle #1, #2 \rangle }
# $
# 
# - `\iprod{x}{y}` $\iprod{x}{y}$
# 
# $$
# \begin{align}
# f(x) &= \frac{1}{2} \iprod{x}{Ax} - \iprod{b}{x} \\
# g(x) &= Df_x = Ax - b
# \end{align}
# $$
# 
# $$
# \begin{align}
# r_{k+1} &= g(x) = A x_{k} - b \\
# p_{k+1} &= r_{k+1} - \sum_{i \leq k} \frac{\iprod{p_i}{r_{k+1}}}{\iprod{p_i}{p_i}} p_i \\
# x_{k+1} &= x_k + \alpha_{k+1} p_{k+1} \\
# &\text{where} \;
# \alpha_{k+1} = \text{argmin}_{\alpha}{f(x + \alpha p_{k+1})} \\
# &(i.e. D_{\alpha}f(x + \alpha p_{k+1}) = 0)
# \end{align}
# $$

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


# Interpolate
# f(0) = 0  f'(0) = 0
# f(1) = 1  f'(1) = 0

x = np.linspace(-.5, 1.5, 20)
y = - 2 * (x ** 3) + 3 * (x ** 2)

df = pd.DataFrame({'x': x, 'f1': y })
df.plot.line(x='x', y='f1')


x = np.linspace(- np.pi / 2, np.pi / 2)
y = np.tanh((1.5) * np.tan(x))
plt.plot(x, y)


# Simple animation on notebook

import numpy as np
import matplotlib.pyplot as plt
import IPython
import time

fig, ax = plt.subplots()

for N in range(5, 20):
    x = np.linspace(-.5, 1.5, N)
    y = - 2 * (x ** 3) + 3 * (x ** 2)
    fig.clear()
    ax.plot(x, y)
    fig.add_axes(ax)
    IPython.display.clear_output(wait=True)
    IPython.display.display(fig)
    time.sleep(0.1)

fig.clear()


# Contour plot 2-dim quadratic form

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-1, 1)
y = np.linspace(-1, 1)
xx, yy = np.meshgrid(x, y)

Q11 = 1
Q22 = 1.5
z = Q11 * xx ** 2 + Q22 * yy ** 2

plt.contourf(x, y, z)
None

