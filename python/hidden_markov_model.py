#!/usr/bin/env python
# coding: utf-8

get_ipython().system('jupyter nbconvert hidden_markov_model.ipynb --to python --no-prompt')


#
# Hidden Markov Model
# - Forward/Backward algorithm
# - Viterbi algorithm
#
# TODO:
# - Strive more rigor on "conditional independency"
# - Simulate HMM by random generator for rough test
# - Take limited precision into account
#   - for longer sequence, currently probability gets too small
#   - use more conditional form of data? (i.e. normalization)
#
# Disclaimer:
# - As always, probability comes with sketchy notation... but you'll get it
# - Array-indexing in program and comment isn't really exact (probably i should be i-1 in comment)


# 
# Given these data:
# 
# ---
# 
# $
# \begin{align}
# x_0 \in &X \; \text{(initial state without emission)} \\
# P(x' | x): \; &X \to \text{Prob}(X) \\
# P(y | x): \; &X \to \text{Prob}(Y) \\
# y_1,...,&y_k \in Y
# \end{align}
# $
# 
# ---
# 
# Solve these problems.
# 
# ---
# 
# (1) Aka. forward:
# 
# $
# \quad \
# P(y_1, ..., y_i, X_i = x)
# \;\; \text{for} \; x \in X \;\text{and}\; i \in \{ 1...k \}
# $
# 
# (2) Aka. backward;
# 
# $
# \quad \
# P(y_{i}, ..., y_k | X_i = x)
# \;\; \text{for} \; x \in X \;\text{and}\; i \in \{ 1...k \}
# $
# 
# (3) Aka. smoothing, forward/backward
# 
# $
# \quad 
# P(y_1, ..., y_k, X_i = x)
# \;\; \text{for} \; x \in X \;\text{and}\; i \in \{ 1...k \}
# $
# 
# (4) Marginalize (1) with $i = k$
# 
# $
# \quad \
# P(y_1, ..., y_k) = \sum_{x} P(y_1, ..., y_k, X_k = x)
# $
# 
# (5) Cf. Viterbi algorithm
# 
# $
# \quad \
# (\text{max}, \text{argmax})_{x_1,...,x_{i-1}} P(y_1, ..., y_{i-1}, y_i, x_1, ..., x_{i-1}, X_i = x)
# \;\; \text{for} \; x \in X \;\text{and}\; i \in \{ 1...k \}
# $
# 
# (6) Special case of (5) with $i = k$
# 
# $
# \quad \
# (\text{max}, \text{argmax})_{x_1,...,x_k} P(y_1, ..., y_k, x_1, ..., x_k)
# $

import numpy as np


#
# Example model
#

# |X|, |Y|
C1 = 2**3
C2 = 2**3

# Initial state
x0 = np.random.randint(0, C1)

# Transition
# T[x, x'] = P(x'| x)
T = np.random.rand(C1, C1)
T = T / T.sum(axis=1, keepdims=True)

# Emission
# E[x, y] = P(y | x)
E = np.random.rand(C1, C2)
E = E / E.sum(axis=1, keepdims=True)

#
# Example observations of length L
#
L = 2**3
ys = np.random.randint(0, C1, size=L)


# 
# (1) F[i, x] = P(y1..yi, Xi = x)
#
F = np.zeros(shape=(L, C1))

for x in range(C1):
    F[0, x] = T[x0, x] * E[x, ys[0]]
    
for i in range(1, L):
    for x in range(C1):
        # i.e. F[i, x] = np.dot(F[i-1, :], A[:, x]) * B[x, ys[i]]
        for x_ in range(C1):
            F[i, x] += F[i-1, x_] * T[x_, x]
        F[i, x] *= E[x, ys[i]]
            
#
# (4) FP = P(y1..yk)
#
FP = F[L - 1].sum()
FP, F


#
# (2) B[i, x] = P(yi..yk | Xi = x)
#
B = np.zeros(shape=(L, C1))

for x in range(C1):
    B[L-1, x] = E[x, ys[L-1]]
    
for i in range(L - 1)[::-1]:
    for x in range(C1):
        for x_ in range(C1):
            B[i, x] += T[x, x_] * B[i+1, x_]
        B[i, x] *= E[x, ys[i]]

#
# Some cheap test
# sum_x P(y1..yk | X1 = x) * P(X1 = x) = P(y1..yk)
#
assert np.allclose(np.dot(B[0], T[x0, :]), FP)

B


#
# (3) S[i, x] = P(y1..yk, Xi = x)
#
# NOTE: it's possible to compute S in the same loop as B
#
S = np.zeros(shape=(L, C1))

for x in range(C1):
    S[L-1, x] = F[L-1, x]

for i in range(L - 1)[::-1]:
    for x in range(C1):
        for x_ in range(C1):
            S[i, x] += T[x, x_] * B[i+1, x_]
        S[i, x] *= F[i, x]

#
# Some cheap test
#
assert np.allclose((S / FP).sum(axis=1), 1)
  
#
# P(Xi = x | y1..yk)
#
S / FP


#
# (5)  V[i, x] = max_{x1..xi-1} P(y1..yi, x1..xi-1, Xi = x)
#     Vb[i, x] = argmax_{xi-1} max_{x1..xi-2} P(...)
#
# NOTE: essentially `Vb` keeps backtracking path attaining "max".
#
V = np.zeros(shape=(L, C1))
Vb = np.zeros(shape=(L, C1), dtype=np.int)

for x in range(C1):
    V[0, x] = T[x0, x] * E[x, ys[0]]
    
for i in range(1, L):
    for x in range(C1):
        for x_ in range(C1):
            candidate = V[i-1, x_] * T[x_, x]
            if candidate >= V[i, x]:
                V[i, x] = candidate
                Vb[i, x] = x_
        V[i, x] *= E[x, ys[i]]

V, Vb


#
# (6)
#
xs = np.zeros_like(ys)

xs[L-1] = np.argmax(V[L-1])
for i in range(1, L)[::-1]:
    xs[i-1] = Vb[i, xs[i]]

np.max(V[L-1]) / FP, xs

