#!/usr/bin/env python
# coding: utf-8

get_ipython().system('jupyter nbconvert linear_algebra.ipynb --to python --no-prompt')


#
# Utilities 1
#

import numpy as np
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

def matshow(A):
    fig, ax = plt.subplots()
    fig.colorbar(ax.matshow(A))

def is_right_triangle(A):
    M, N = A.shape
    mask = np.array([[i <= j for j in range(N)] for i in range(M)])
    return np.allclose(A * (~ mask), np.zeros_like(A))

def is_unitary(Q, debug=False):
    M, N = Q.shape
    I = np.conj(Q.T) @ Q
    if debug:
        matshow(I - np.eye(M))
    return M == N and np.allclose(I, np.eye(M))

def is_upper_bidiagonal(A):
    M, N = A.shape
    mask = np.array([[(i == j or i + 1 == j) for j in range(N)] for i in range(M)])
    return np.allclose(A * (~ mask), np.zeros_like(A))

def mask_hessenberg(A):
    M, N = A.shape
    mask = np.array([[(i <= j + 1) for j in range(N)] for i in range(M)])
    return A * mask

def is_hessenberg(A):
    M, N = A.shape
    mask = np.array([[(i <= j + 1) for j in range(N)] for i in range(M)])
    return np.allclose(A * (~ mask), np.zeros_like(A))

def mask_tridiag(A):
    M, N = A.shape
    mask = np.array([[ (j - 1 <= i and i <= j + 1) for j in range(N) ] for i in range(M) ])
    return A * mask

def is_tridiag(A):
    M, N = A.shape
    mask = np.array([[ (j - 1 <= i and i <= j + 1) for j in range(N) ] for i in range(M) ])
    return np.allclose(A * (~ mask), np.zeros_like(A))


#
# Utilities 2
#

def get_norm(x):
    return np.sqrt(np.sum(x * np.conj(x)))

#
# Output:
# - v: reflector
# - v_hss: |v| / 2 (half-square-sum)
#
# Assumption
# - x != 0 (i.e |x| > 0)
#
def get_reflector(x):
    x_norm = get_norm(x)
    
    if x_norm == 0:
        # already zero without reflector
        v = np.zeros_like(x)
        return v, 0
    
    # v: non-normalized reflector (= x - k * e1 where k = - (x1 / x1_abs) * x_norm)
    v = x.copy()
    x1 = x[0]
    x1_abs = np.abs(x1)
    if x1_abs == 0:
        # A bit exceptional case but this is reflector
        v[0] = x1_abs
    else:
        # Cool formula for (x - k * e1)_1
        x1_unit = x1 / x1_abs
        v[0] = x1_unit * ( x1_abs + x_norm)

    # Cool formula for |v| / 2 (half-square-sum)
    v_hss = x_norm * (x_norm + x1_abs)
    return v, v_hss

#
# Single step of left Householder matrix multiplication (inplace)
# with returning its reflector
#
# Output:
# - A (inplace)
# - v: reflector (if first column x = 0 already, then v = 0)
#
def householder(A):
    x = A[0:, 0]
    v, v_hss = get_reflector(x)
    
    if v_hss == 0:
        # if v = 0, then Householder unitary is identity
        return v

    # Update A by mul with Householder unitary (I - v_hss * v * v^H)
    v_ = v.reshape(-1, 1)
    vH = np.conj(v.reshape(1, -1))
    A -= (v_ @ vH @ A) / v_hss

    return v / np.sqrt(v_hss * 2)


#
# Householder QR decomposition
#

import numpy as np

#
# Q A = R
#
# Output:
# - A (inplace) => R: right triangle matrix
# - qs = ( q1, q2, ..,q_{n-1} ) reflectors as left triangle
#   where
#   - Q = Qn-1 .. Q1
#   - Qi = diag(I, I - 2 qi qi^H)
#
def qr(A):
    M, N = A.shape
    L = min(M, N)
    qs = []
    for i in range(L):
        # Last step is not necessarily when A is not tall one
        if M <= N and i == L - 1:
            break

        A_ = A[i:, i:]
        q = householder(A_) # inplace
        qs.append(q)

    return qs

#
# TODO:
#
# Multiply Q B
# where Q is given as reflectors of `qr`
#
# probably faster than reconstruct_q and multiply
#
def mul_q(qs, B):
    N = len(qs)
    return None

#
# Reconstruct Q from reflectors qs
#
def reconstruct_q(qs):
    assert len(qs) >= 1
    M = len(qs[0])
    Q = np.eye(M)
    for i, q in enumerate(qs):
        q_ = q.reshape(-1, 1)
        qH = np.conj(q.reshape(1, -1))
        Q_ = Q[i:, :]
        Q_ -= (q_ @ qH @ Q_) * 2
    return Q


import unittest
class __0(unittest.TestCase):
    # Square case
    def test_0(self):
        N = 2**7
        A = np.random.normal(size=(N, N))

        # qr uses A inplace
        R = A.copy()
        qs = qr(R)
        Q = reconstruct_q(qs)
        assert is_right_triangle(R)
        assert is_unitary(Q)
        assert np.allclose(Q @ A, R)        
        
    # Rectangular case
    def test_1(self):
        M = 4
        N = 3
        A = np.random.normal(size=(M, N))
        
        # Tall case
        R = A.copy()
        qs = qr(R)  
        Q = reconstruct_q(qs)
        assert R.shape == (M, N)
        assert Q.shape == (M, M)
        assert is_right_triangle(R)
        assert is_unitary(Q)
        assert np.allclose(Q @ A, R)
    
        # Wide case
        A = A.T
        R = A.copy()
        qs = qr(R) 
        Q = reconstruct_q(qs)
        assert R.shape == (N, M)
        assert Q.shape == (N, N)
        assert is_right_triangle(R)
        assert is_unitary(Q)
        assert np.allclose(Q @ A, R)        
            

if __name__ == '__main__':
    unittest.main(argv='XXX -v -k __0.test_'.split(), exit=False)


#
# Householder Bidiagonalization
#

from pprint import pprint

#
# Q A P = B
# 
# Output:
# - A (inplace) => B: bidiagonalized matrix
# - qs: list of reflectors for left Householder unitary matrices
# - ps: list of reflectors for right Householder unitary matrices
#
# Assumption:
# - M >= N (i.e. A: tall rectangular)
#
def upper_bidiagonalize(A):
    M, N = A.shape
    qs = []
    ps = []
    ps.append(np.zeros(N))
    for i in range(N):
        # Last step is not necessarily when A is square
        if M == N and i == N - 1:
            break
            
        # Householder from left
        A_ = A[i:, i:]
        q = householder(A_) # inplace
        qs.append(q)
        
        # "left" only for the last two steps
        if i >= N - 2:
            continue

        # Householder from right
        A_ = A[i:, i+1:].T
        p = householder(A_) # inplace
        ps.append(np.conj(p))

    return qs, ps


import unittest
class __1(unittest.TestCase):
    def test_0(self):
        M = 2**7
        N = M
        A = np.random.normal(scale=10, size=(M, N))

        B = A.copy()
        qs, ps = upper_bidiagonalize(B)
        
        Q = reconstruct_q(qs)
        PH = reconstruct_q(ps)
        PT = np.conj(PH)
        QAP = (PT @ (Q @ A).T).T
        
        assert is_upper_bidiagonal(B)
        assert is_unitary(Q)
        assert is_unitary(PH)
        assert np.allclose(QAP, B)

    def test_1(self):
        M = 2**6
        N = 2**4
        A = np.random.normal(scale=10, size=(M, N))

        B = A.copy()
        qs, ps = upper_bidiagonalize(B)
        
        Q = reconstruct_q(qs)
        PH = reconstruct_q(ps)
        PT = np.conj(PH)
        QAP = (PT @ (Q @ A).T).T
        
        assert is_upper_bidiagonal(B)
        assert is_unitary(Q)
        assert is_unitary(PH)
        assert np.allclose(QAP, B)        

if __name__ == '__main__':
    unittest.main(argv='XXX -v -k __1.test_'.split(), exit=False)


#
# Hessenberg form transf.
#

#
# Q A Q^H = B
# 
# Output:
# - A (inplace) => H: Hessenberg matrix
# - qs: list of reflectors making Q
#
def hessenberg(A):
    M, N = A.shape
    assert M == N
    qs = []
    qs.append(np.zeros(M))
    for i in range(M - 2):
        # Householder from left
        A_ = A[i+1:, i:]
        q = householder(A_)
        qs.append(q)
        
        # Apply same reflector from right
        # (upper right isn't zero so need to update there)
        A_ = A[:, i+1:]
        q_ = q.reshape(-1, 1)
        qH = np.conj(q.reshape(1, -1))
        A_ -= (A_ @ q_ @ qH) * 2
    return qs

import unittest
class __2(unittest.TestCase):
    def test_0(self):
        M = 2**7
        N = M
        A = np.random.normal(scale=10, size=(M, N))

        B = A.copy()
        qs = hessenberg(B)

        # For debug
        if False:
            matshow(B)

        assert is_hessenberg(B)

        Q = reconstruct_q(qs)
        assert is_unitary(Q)

        QH = np.conj(Q).T

        # For debug
        if False:
            pprint(qs)
            diff = Q @ A @ QH - B
            print(diff)
            matshow(diff)

        assert np.allclose(Q @ A @ QH, B)

if __name__ == '__main__':
    unittest.main(argv='XXX -v -k __2.test_'.split(), exit=False)


#
# Hessenberg-preserving unitary equivalence via Householder QR decomp
#
# Q A  = R
# A    = QH R
# A'   = R QH = Q A QH
# with 
# - A, A': Hessenberg
# - R: right triangle
# - Q: unitary
#
# Output:
# - A => A' (inplace)
# - Q (if return_q=True)
#
# hermitian=True return_q=False also=False --> O(N)
# other cases                              --> O(N^2)
#
# By passing `also=X`, it computes `Q X` inplace (O(N^2)).
#
def qr_hessenberg(A, hermitian=False, return_q=False, also=None):
    M, N = A.shape; assert M == N
    if return_q:
        Q = np.eye(M).astype(A.dtype)
    if also is not None:
        X = also
    us = [] # Keep reflectors for the later right multiplications
    for i in range(M - 1):
        # Householder from left
        # - if Hessenberg, 2x(n-i) block changes
        # - if Hermitian, 2x3 block changes
        if hermitian:
            A_ = A[i:i+2, i:i+3]
        else:
            A_ = A[i:i+2, i:]
        u = householder(A_)
        us.append(u)
        if return_q:
            Q_ = Q[i:i+2, :i+2]
            u_ = u.reshape(-1, 1)
            uH = np.conj(u.reshape(1, -1))
            Q_ -= (u_ @ (uH @ Q_)) * 2
        if also is not None:
            X_ = X[i:i+2, :]
            u_ = u.reshape(-1, 1)
            uH = np.conj(u.reshape(1, -1))
            X_ -= (u_ @ (uH @ X_)) * 2            

    for i in range(M - 1):
        # Apply same reflectors from right
        # - if Hessenberg, ix2 block changes
        # - if Hermitian, 3x2 block changes
        if hermitian:
            A_ = A[max(0, i-1):i+2, i:i+2]
        else:
            A_ = A[:i+2, i:i+2]
        u = us[i]
        u_ = u.reshape(-1, 1)
        uH = np.conj(u.reshape(1, -1))
        A_ -= ((A_ @ u_) @ uH) * 2

    if return_q:
        return Q

import unittest
class __3(unittest.TestCase):
    def test_0(self):
        M = 4
        N = M
        A = mask_hessenberg(np.random.normal(scale=10, size=(M, N)))

        B = A.copy()
        Q = qr_hessenberg(B, return_q=True)
        if False:
            matshow(Q)
            matshow(B)
        assert is_hessenberg(B)
        assert np.allclose(B, Q @ A @ np.conj(Q.T))

        _B = A.copy()
        _Q = np.eye(N)
        qr_hessenberg(_B, also=_Q)
        assert np.allclose(Q, _Q)

    def test_1(self):
        M = 2**5
        N = M
        A = mask_tridiag(np.random.normal(scale=10, size=(M, N)))
        A = np.conj(A.T) + A
        assert is_tridiag(A)

        B = A.copy()
        Q = qr_hessenberg(B, hermitian=True, return_q=True)
        if False:
            matshow(Q)
            matshow(B)
        assert np.allclose(np.conj(Q.T) @ Q, np.eye(M))
        assert np.allclose(B, Q @ A @ np.conj(Q.T))        
        assert is_tridiag(B)
        
    def test_2(self):
        M = 2**5
        N = M
        A = mask_tridiag(np.random.normal(scale=10, size=(M, N)))
        A = np.conj(A.T) + A
        assert is_tridiag(A)

        B = A.copy()
        qr_hessenberg(B, hermitian=True)
        assert is_tridiag(B)        

    def test_3(self):
        """ Complex matrix """
        M = 4
        N = M
        Ar = np.random.normal(scale=10, size=(M, N))
        Ai = np.random.normal(scale=10, size=(M, N)) * 1j
        A = mask_hessenberg(Ar + Ai)

        B = A.copy()
        Q = qr_hessenberg(B, return_q=True)
        assert is_hessenberg(B)
        assert np.allclose(Q @ np.conj(Q.T), np.eye(N))
        assert np.allclose(B, Q @ A @ np.conj(Q.T))        
        
if __name__ == '__main__':
    unittest.main(argv='XXX -v -k __3.test_'.split(), exit=False)


#
# QR eigenvalue/vector method (Hessenberg case)
#

#
# A = PH R P
#
# Output:
# - A (inplace) => R: Eigenvalues of A appear in R's diagonal.
#                     If A is hermitian, then R is hermitian (i.e. R is diagonal in this case).
# - P: Unitary
#
# Assumption
# - A: invertible
# - A: distinct eigen values
# - A = X D X^-1 where X^-1 has no-pivot LR decomp.
#
def eigen_hessenberg(A, hermitian=False, return_p=False, num_steps=2**5):
    N, M = A.shape; assert N == M
    if return_p:
        P = np.eye(N).astype(A.dtype)
    for i in range(num_steps):
        if return_p:
            qr_hessenberg(A, hermitian=hermitian, also=P)
        else:
            qr_hessenberg(A, hermitian=hermitian)
    if return_p:
        return P

import unittest
class __4(unittest.TestCase):
    def test_0(self):
        np.random.seed(0)
        # NOTE: 
        # luckly for this case (M = 4), A has only real eigen values, so the diagonal converges.
        # And result coincides with numpy (i.e. lapack geev).
        # For other case (e.g. M = 5), it hits complex eigen values, so the assertion fails.
        # NOTE:
        # Probably { real diagonalizables } are not measure zero in R^NxN,
        # even though { diagonalizables } are measure zero in C^NxN.
        M = 4 
        N = M
        A = mask_hessenberg(np.random.normal(scale=10, size=(M, N)))

        B = A.copy()
        P = eigen_hessenberg(B, return_p=True)
        diag = np.diag(B)
        egvals = np.linalg.eigvals(A)
        if False:
            offdiag = np.diag(B, k=-1)
            print(diag)
            print(offdiag)
            print(np.sort(diag))
            print(np.sort(np.linalg.eigvals(A)))
        assert np.allclose(np.conj(P.T) @ P, np.eye(M))
        assert np.allclose(A, np.conj(P.T) @ B @ P)
        assert np.allclose(np.sort(diag), np.sort(np.linalg.eigvals(A)))
        
    def test_1(self):
        np.random.seed(0)
        M = 7 # fails M = 8
        N = M
        A = mask_tridiag(np.random.normal(scale=10, size=(M, N)))
        A = np.conj(A.T) + A

        B = A.copy()
        P = eigen_hessenberg(B, hermitian=True, return_p=True, num_steps=2**8)
        diag = np.diag(B)
        egvals = np.linalg.eigvals(A)
        if False:
            offdiag = np.diag(B, k=-1)
            matshow(B)
            print(diag)
            print(offdiag)
            print(np.sort(diag))
            print(np.sort(np.linalg.eigvals(A)))
        assert np.allclose(np.sort(diag), np.sort(np.linalg.eigvals(A)))
        
    def test_2(self):
        """ Complex matrix """
        np.random.seed(0)
        M = 4
        N = M
        Ar = np.random.normal(scale=10, size=(M, N))
        Ai = np.random.normal(scale=10, size=(M, N)) * 1j
        A = mask_hessenberg(Ar + Ai)

        B = A.copy()
        P = eigen_hessenberg(B, return_p=True, num_steps=2**8) # 2**7 isn't enough
        diag = np.diag(B)
        egvals = np.linalg.eigvals(A)
        if False:
            offdiag = np.diag(B, k=-1)
            print(get_norm(offdiag))
        assert np.allclose(np.conj(P.T) @ P, np.eye(M))
        assert np.allclose(A, np.conj(P.T) @ B @ P)
        assert np.allclose(np.sort(diag), np.sort(np.linalg.eigvals(A)))        

    def test_3(self):
        """ Complex matrix, Hermitian """
        np.random.seed(0)
        M = 2*5
        N = M
        Ar = np.random.normal(scale=10, size=(M, N))
        Ai = np.random.normal(scale=10, size=(M, N)) * 1j
        A = mask_tridiag(Ar + Ai)
        A = np.conj(A.T) + A

        B = A.copy()
        eigen_hessenberg(B, hermitian=True, num_steps=2**7)
        diag = np.diag(B)
        offdiag = np.diag(B, k=-1)
        egvals = np.linalg.eigvals(A)
        if False:
            print(get_norm(offdiag))
        assert np.allclose(np.sort(diag), np.sort(np.linalg.eigvals(A)))             
        
if __name__ == '__main__':
    unittest.main(argv='XXX -v -k __4.test_'.split(), exit=False)


#
# Inverse (via Householder + Gauss elim.)
#

#
# B A = A B = I
# Output: B
#
# Assumption: A: invertible
#
def inverse(A):
    M, N = A.shape; assert M == N
    # Compute inverse as follows
    # - Q A = R  (Householder)
    # - L RT = D (pivot-free diagonal-preserving Gauss elim.)
    # => Q A LT = D
    # => A LT D-1 Q = I
    #      ~~~~~~~~
    R = A.copy()
    qs = qr(R) # inplace R
    Q = reconstruct_q(qs)
    if np.any(np.abs(np.diag(R)) <= 1e-10):
        raise RuntimeError('Not invertible')
    L = left_gauss_elim(R.T)
    D_inv = np.diagflat(1 / np.diag(R))
    return L.T @ D_inv @ Q

#
# L' L = D
# where
# - diag(D) = diag(L) (thus diag(L') = diag(I))
#
# Output:
# - L'
#
def left_gauss_elim(L):
    M, N = L.shape; assert M == N
    X = np.eye(M)
    for i in range(M - 1):
        a1 = L[i, i]
        u = L[i+1:, i] / a1
        v = X[i, :i+1]
        X[i+1:, :i+1] -= np.outer(u, v)
    return X
    
import unittest
class __5(unittest.TestCase):
    """ Inverse """
    def test_0(self):
        M = N = 2**6
        A = np.random.normal(scale=10, size=(M, N))
        _A = inverse(A)
        assert np.allclose(_A @ A, np.eye(M))
        
if __name__ == '__main__':
    unittest.main(argv='XXX -v -k __5.test_'.split(), exit=False)


#
# TODO:
# Linear least square 
# - full rank case (i.e. injective or surjective)
# - non-full rank case reduces to the same problem for non-invertible square right triangle,
#   which I can't solve...
#

def lstsq_qr():
    # case 1. injective (thus M >= N)
    # case 2. surjective (thus M <= N)
    # o.w.
    pass

def lstsq_pinv():
    # assumption: injective
    pass

