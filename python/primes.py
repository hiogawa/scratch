#!/usr/bin/env python
# coding: utf-8

get_ipython().system('jupyter nbconvert primes.ipynb --to python --no-prompt')


"""
Unbounded prime number generation
(cf. https://wiki.haskell.org/Prime_numbers)

TODO:
- Implementation which avoids stack overflow
    - cf. http://code.activestate.com/recipes/474088/
"""

from itertools import count
from collections import deque

class peekable:
    """Cf. https://github.com/erikrose/more-itertools/blob/master/more_itertools/more.py"""
    def __init__(self, iterable):
        self._it = iter(iterable)
        self._cache = deque()

    def __next__(self):
        if self._cache:
            return self._cache.popleft()
        return next(self._it)
    
    def peek(self):
        if not self._cache:
            self._cache.append(next(self._it))
        return self._cache[0]

def ordered_minus(l1, l2):
    pl1 = peekable(l1)
    for y in l2:
        while True:
            # Direct use of itertools.takewhile doesn't work
            # since it would filter out first "x > y" element too.
            # So, in order to keep such element, we need peekable iterator.
            x = pl1.peek()
            if x > y:
                break
            if x == y:
                next(pl1)
                break
            next(pl1)
            yield x

#
# Straight-forward implementation
#           
def sieve(ls):
    p = next(ls)
    yield p
    yield from ordered_minus(sieve(ls), count(start=p, step=p))

def primes():
    return sieve(count(start=2))

#
# Optimized implementation
#
def sieve_v2(ls):
    p = next(ls)
    yield p
    yield from ordered_minus(sieve_v2(ls), count(start=p*p, step=p*2))

def primes_v2():
    yield 2
    yield from sieve_v2(count(start=3, step=2))

#
# Testing
#
def test_primes():
    from itertools import islice, takewhile
    assert list(islice(primes(), 6)) == [2, 3, 5, 7, 11, 13]
    assert len(list(takewhile(lambda x: x <= 2**7, primes()))) == 31
    assert list(islice(primes_v2(), 6)) == [2, 3, 5, 7, 11, 13]
    assert len(list(takewhile(lambda x: x <= 2**7, primes_v2()))) == 31
    try:
        list(islice(primes(), 2**8))
    except RuntimeError as e:
        assert e.args == ('maximum recursion depth exceeded', )
    
test_primes()  


def minus(l1, l2):
    return [x for x in l1 if not x in l2]

def primes_bounded(m):
    yield 2
    ls = list(range(3, m, 2))
    while ls:
        p = ls.pop(0)
        yield p
        ls = minus(ls, list(range(p*p, m, p*2)))


import numpy as np

def primes_bounded_bool(m):
    assert m >= 2
    B = np.ones(m, dtype=np.bool)
    B[0] = B[1] = False
    B[4::2] = False
    for x in range(3, m):
        if B[x]:
            B[x*x::x*2] = False
    return B

def primes_bounded_v2(m):
    assert m >= 2
    B = np.ones(m, dtype=np.bool)
    B[0] = B[1] = False
    B[4::2] = False
    for x in range(3, m):
        if B[x]:
            yield x
            B[x*x::x*2] = False        


import itertools as itt

get_ipython().run_line_magic('timeit', '-n1 -r2 list(itt.takewhile(lambda x: x < 2**9, primes()))')
get_ipython().run_line_magic('timeit', '-n1 -r2 list(itt.takewhile(lambda x: x < 2**9, primes_v2()))')
get_ipython().run_line_magic('timeit', '-n1 -r2 list(primes_bounded(2**9))')
get_ipython().run_line_magic('timeit', '-n1 -r2 list(primes_bounded_v2(2**9))')
get_ipython().run_line_magic('timeit', '-n1 -r2 primes_bounded_bool(2**20)')
get_ipython().run_line_magic('timeit', '-n1 -r2 list(primes_bounded_v2(2**20))')


#
# Simple data browser
#

import IPython.display as ipyd
import ipywidgets as ipyw
import pandas as pd

def browse(df):
    N = len(df)
    @ipyw.interact(offset=(0, N, 10), num_rows=(10, N, 10))
    def _(offset=0, num_rows=10):
        ipyd.display_html(df[offset:offset+num_rows].to_html(), raw=True)
        
browse(pd.DataFrame(primes_bounded_v2(2**17)))


#
# Look for some fun-looking prime
#

def selector(x):
    l = list(str(x))
    s = set(l)
    # [ 4 distinct non-zero digits with length 4 ]
    # b = (not '0' in s) and len(l) == len(s) and len(s) == 4
    # [ 2 digits ]
    # b = len(s) == 2
    # [ Altenating 2 digits ]
    b = len(set(l[0::2])) == 1 and len(set(l[1::2])) == 1
    return b

_ = np.array(list(primes_bounded_v2(2**21)))
__ = np.vectorize(selector, otypes=[np.bool])(_)

browse(pd.DataFrame(_[__]))

