#!/bin/bash

#
# Usage:
#
# pipenv install --skip-lock
# bash scripts.sh j
#

function Main() {
  case "${1}" in
    j|jupyter)
      pipenv run jupyter notebook
    ;;
  esac
}

Main "${@}"
