#!/usr/bin/env python
# coding: utf-8

get_ipython().system('jupyter nbconvert linear_algebra2.ipynb --to python --no-prompt')


#
# - Conjugate gradient descent
# - Cholskey decomposition
# - LR decomp with pivot selection (Gauss elim.)
# - Bidiagonalization of positive definite (with numba)
#


import numpy as np
import itertools
import unittest
import numba


def conjugate_gradient_descent(A, b, num_steps=2**10, debug=False):
    M, N = A.shape
    K, = b.shape
    assert M == N and M == K
    loop = itertools.count() if num_steps is None else range(num_steps)

    x = np.zeros_like(b)
    r = A @ x - b
    dot_r = np.dot(r, r)
    if np.allclose(dot_r, 0): return x
    p = r
    
    for i in loop:
        # line search minimum
        alpha = - np.dot(p, r) / np.dot(p, A @ p)
        x += alpha * p
        
        # gradient
        r_ = A @ x - b
        
        # conjugate gradient
        dot_r_ = np.dot(r_, r_)
        beta = dot_r_ / dot_r
        p = r_ + beta * p

        # update
        r = r_
        dot_r = dot_r_
        
        if debug and (i + 1) % 2**10 == 0:
            print(f":: #iters: {i}, error: {dot_r}")
        if np.allclose(dot_r, 0):
            break

    if debug: print(f":: #iters: {i}, error: {dot_r}")
    return x


def conjugate_gradient_descent_preconditioned(A, b, C, num_steps=2**10, debug=False):
    """
    Equivalent problem
    - (M A MH) y = M b
    - x = MH y
    - r = M A MH y - M b
    - r = M s 
    - q = MH p
    - C = MH M
    
    Follow equivalent problem's `r` and `q` in comments within code.
    It's amazing that we don't use M and MH for computation and C saficies.
    """
    M, N = A.shape
    K, = b.shape
    assert M == N and M == K
    loop = itertools.count() if num_steps is None else range(num_steps)

    x = np.zeros_like(b)
    s = A @ x - b
    # r = M @ s
    # p = r
    # q = MH @ p = MH @ M @ s = C @ s
    q = C @ s
    
    # <r, r> = <M s, M s> = <s, MH M s> = <s, C s>
    cs = C @ s
    dot_r = np.dot(s, cs)
    if np.allclose(dot_r, 0): return x
    
    for i in loop:
        # line-search minimum
        alpha = - np.dot(q, s) / np.dot(q, A @ q)
        x += alpha * q

        # gradient
        s_ = A @ x - b
        cs_ = C @ s_
        
        # conjugate gradient
        # - p' = r' + (<r', r'> / <r, r>) p = M s' + (<s', C s'> / <s, C s>) p
        # - q' = MH p' = MH M s' + (<s', C s'> / <s, C s>) MH p
        #              = C s' + (<s', C s'> / <s, C s>) q
        dot_r_ = np.dot(s_, cs_)
        beta = dot_r_ / dot_r
        q = cs_ + beta * q

        # update
        s = s_
        cs = cs_
        dot_r = dot_r_
        
        if debug and (i + 1) % 2**10 == 0:
            print(f":: #iters: {i}, error: {dot_r}")
        if np.allclose(dot_r, 0):
            break        

    if debug: print(f":: #iters: {i}, error: {dot_r}")
    return x


class __0(unittest.TestCase):
    """Conjugate Gradient Descent"""
    
    def test_0(self):
        """Standard"""
        
        np.random.seed(0)
        N = 2**8 # 2**11 worked
        A = np.random.randn(N, N)
        A = A.T @ A # this should be positive definite with probability 1
        b = np.random.randn(N)
        
        x = conjugate_gradient_descent(A, b, debug=True, num_steps=None)
        r = A @ x - b
        e = np.dot(r, r)
        assert np.allclose(e, 0)
        
    def test_1(self):
        """Preconditioned"""
        
        np.random.seed(0)
        N = 2**7 # 2**11 worked
        A = np.random.randn(N, N)
        A = A.T @ A # this should be positive definite with probability 1
        b = np.random.randn(N)
        
        # Use random pos. def. as preconditioner
        C = np.random.randn(N, N)
        C = C.T @ C 
        
        x = conjugate_gradient_descent_preconditioned(A, b, C, debug=True, num_steps=None)
        r = A @ x - b
        e = np.dot(r, r)
        assert np.allclose(e, 0)        

if __name__ == '__main__':
    unittest.main(argv='XXX -v -k __0.test_'.split(), exit=False)


def lower_tri_mask(A):
    M, N = A.shape
    return np.array([[i >= j for j in range(N)] for i in range(M)])

def cholesky(A):
    """
    A = L LH
    where
    - L: left triangle
    
    Input:
    - A: positive definite

    Output:
    - L
    """
    M, N = A.shape; assert M == N
    pass

def cholesky_nonstandard(A):
    """
    It seems this is "non standard" version of existence proof.
    Run induction from right bottom part of sub matrix, which I feel more natural.
    """
    M, N = A.shape; assert M == N
    for i in range(M):
        a = A[i, i]
        b = A[i+1:, i]
        A[i+1:, i+1:] -= np.outer(b, np.conj(b)) / a # only lower triangle should be updated
        A[i:, i] /= np.sqrt(a)
    A *= lower_tri_mask(A) # keep only lower triangle

class __1(unittest.TestCase):
    """Cholesky decomposition test"""
    
    def test_0(self):
        """Real matrix"""
        np.random.seed(0)
        N = 2**7
        A = np.random.randn(N, N)
        A = np.conj(A.T) @ A # this should be positive definite with probability 1
        
        L = A.copy()
        cholesky_nonstandard(L)
        assert np.allclose(L @ np.conj(L.T), A)
        
    def test_1(self):
        """Complex matrix"""
        np.random.seed(0)
        N = 4
        Ar = np.random.randn(N, N)
        Ai = np.random.randn(N, N) * 1j
        A = Ar + Ai
        A = np.conj(A.T) @ A # this should be positive definite with probability 1

        L = A.copy()
        cholesky_nonstandard(L)
        assert np.allclose(L @ np.conj(L.T), A)
        

if __name__ == '__main__':
    unittest.main(argv='XXX -v -k __1.test_'.split(), exit=False)


#
# LR decomposition (via Gaussian elim.)
#
import numpy as np

def pivot_policy__maximum_abs(A):
    v = np.abs(A[:, 0])
    i = np.argmax(v)
    if v[i] < np.finfo(v.dtype).eps:
        return None
    else:
        return i

#
# Proof sketch
#   L0 P0 A0 = A1
#   L1 P1 A1 = A2
#   => 
#   A2 = L1 P1 A1
#      = L1 P1 L0 P0 A0
#      = L1 (P1 L0 P1) (P1 P0) A0
#            ~~~~~~~~ = L0'
#
def lr_decomp(A):
    M, N = A.shape; assert M == N
    pivots = np.zeros((M,), dtype=np.int)
    pivots[-1] = M - 1

    for i in range(M - 1):
        pivot = pivot_policy__maximum_abs(A[i:, i:])
        
        # None means the column is already "eliminated"
        if pivot is None:
            pivots[i] = i
            continue
        
        # Save pivot
        j = i + pivot
        pivots[i] = j
        
        # Pivoting
        if i != j:
            A[i], A[j] = A[j], A[i].copy()
        
        # Elimination
        a = A[i, i]
        b = A[i+1:, i]
        l = b / a
        c = A[i, i+1:]
        A[i+1:, i+1:] -= np.outer(l, c)

        # Save L's column in the unused space
        A[i+1:, i] = l

    return pivots

def separate_lr(LR):
    M, N = LR.shape; assert M == N
    idx_diff = np.arange(M).reshape(-1, 1) - np.arange(M)
    left_mask = idx_diff > 0
    diag_mask = idx_diff == 0
    L = LR * left_mask + np.eye(M)
    R = LR * ~left_mask
    return L, R    

def pivots_to_permutation(pivots):
    N, = pivots.shape
    P = np.eye(N)
    for i in range(N):
        j = pivots[i]
        if i != j:
            P[i], P[j] = P[j], P[i].copy()
    return P

def test():
    A = np.random.randn(5, 5)
    LR = A.copy()
    pivots = lr_decomp(LR) # inplace
    L, R = separate_lr(LR)
    P = pivots_to_permutation(pivots)
    print(L, R, P, sep='\n\n')
    assert np.allclose(P @ A, L @ R)

test()


import numba

def band_mask(n, b):
    k = b - 1
    r = np.arange(n)
    mask = np.abs(r[:, np.newaxis] - r) <= k
    return mask

@numba.njit('f8[:](f8[:],)')
def householder_vector(v):
    # assert v.shape[0] >= 1
    v_norm = np.linalg.norm(v)
    if v_norm == 0:
        return v
    a = v[0]
    a_abs = np.abs(a)
    u = v.copy()
    if a_abs == 0:
        u[0] = v_norm
    else:
        u[0] = (a / a_abs) * (a_abs + v_norm)
    u_norm = np.sqrt(2 * v_norm * (v_norm + a_abs))
    return u / u_norm

@numba.njit('Tuple((f8[:, :], f8[:, :]))(f8[:, :])')
def bidiag(A):
    # assert A.shape[0] == A.shape[1]
    # assert A is hermitian
    m = A.shape[0]
    Q = np.eye(m)
    for i in range(m - 2):
        u = householder_vector(A[i+1:, i])
        Q[i+1:, :] -= 2 * np.outer(u, u @ Q[i+1:, :])
        A[i+1:, i:] -= 2 * np.outer(u, u @ A[i+1:, i:])
        A[i:, i+1:] -= 2 * np.outer(A[i:, i+1:] @ u, u)
    return Q, A

def _test_bidiag(A, verbose=False):
    x = np.linspace(0, 1, 2**2)

    # "squared exp" covariance function
    cov = np.exp(- (x[:, np.newaxis] - x) ** 2)
    Q, B = bidiag(A.copy())

    # B: bidiagonal
    assert np.allclose(B[np.tril_indices(B.shape[0], -2)], 0)
    assert np.allclose(B[np.triu_indices(B.shape[0], +2)], 0)
    
    # Q: unitary
    if verbose:
        plt.colorbar(plt.matshow(Q @ Q.T))
        plt.colorbar(plt.matshow(B))
        plt.colorbar(plt.matshow(Q))
        
    assert np.allclose(Q @ Q.T, np.eye(Q.shape[0]))

    # Unitary equivalence
    assert np.allclose(Q @ A @ Q.T, B)
    
def test_bidiag_ex1():
    # "squared exp" covariance function
    x = np.linspace(0, 1, 2**5)
    cov = np.exp(- 1 * (x[:, np.newaxis] - x) ** 2)
    _test_bidiag(cov, verbose=False)
    
def test_bidiag_ex2():
    A = np.random.randn(2**7, 2**7)
    _test_bidiag(A @ A.T)

def test_bidiag_ex3():
    A = np.random.randn(8, 4)
    _test_bidiag(A @ A.T, verbose=False)

test_bidiag_ex1()
test_bidiag_ex2()
test_bidiag_ex3()

