#!/usr/bin/env python
# coding: utf-8

get_ipython().system('jupyter nbconvert mnist.ipynb --to python --no-prompt')


# Cf. http://yann.lecun.com/exdb/mnist/
get_ipython().system('wget -P ~/.cache/wget/yann.lecun.com/exdb/mnist -c http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz')
get_ipython().system('wget -P ~/.cache/wget/yann.lecun.com/exdb/mnist -c http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz')
get_ipython().system('wget -P ~/.cache/wget/yann.lecun.com/exdb/mnist -c http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz')
get_ipython().system('wget -P ~/.cache/wget/yann.lecun.com/exdb/mnist -c http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz')
get_ipython().system('gunzip  ~/.cache/wget/yann.lecun.com/exdb/mnist/*.gz')


import os
import numpy as np
import matplotlib.pyplot as plt
import ipywidgets as ipyw


G_data_dir = os.path.expanduser('~/.cache/wget/yann.lecun.com/exdb/mnist/')
G_data_filenames = {
    'train_x': 'train-images-idx3-ubyte',
    'train_y': 'train-labels-idx1-ubyte',
    'test_x': 't10k-images-idx3-ubyte',
    'test_y': 't10k-labels-idx1-ubyte'
}
G_data_filenames = dict((k, G_data_dir + v) for k, v in G_data_filenames.items())


class DS(object):
    def __init__(self, filenames):
        self.train_x = self.read_x(filenames['train_x'])
        self.test_x = self.read_x(filenames['test_x'])
        self.train_y = self.read_y(filenames['train_y'])
        self.test_y = self.read_y(filenames['test_y'])

    def read_x(self, filename):
        data = np.frombuffer(
            open(filename, mode='rb').read(),
            offset=16,
            dtype=np.uint8)
        return data.reshape(-1, 28, 28)
        
    def read_y(self, filename):
        return np.frombuffer(
            open(filename, mode='rb').read(),
            offset=8,
            dtype=np.uint8)
    
    def make_loader(self, batch_size=2**5, test=False):
        x = self.test_x if test else self.train_x 
        y = self.test_y if test else self.train_y
        x = torch.from_numpy(x).float() / (2**8)
        y = torch.from_numpy(y).long()
        return torch.utils.data.DataLoader(list(zip(x, y)), batch_size=batch_size, drop_last=True)
    
    def check(self):
        assert np.all((0 <= G_dataset.train_y) * (G_dataset.train_y < 10))
        assert np.all((0 <= G_dataset.test_y) * (G_dataset.test_y < 10))
    
    def browse(self, test=False):
        x = self.test_x if test else self.train_x 
        y = self.test_y if test else self.train_y
        
        @ipyw.interact(offset=(0, len(x), 1))
        def widget(offset=0, nrows=2, ncols=2, figsize=6):
            fig, axes = plt.subplots(nrows, ncols, figsize=(figsize, figsize))
            for i in range(nrows):
                for j in range(ncols):
                    k = offset * nrows * ncols + i * nrows + j
                    axes[i, j].matshow(x[k], cmap='Greys')
                    axes[i, j].set(title=f"({k}) {y[k]}", xticks=[], yticks=[])            
        
G_dataset = DS(G_data_filenames)
G_dataset.check()


import functools
compose = lambda fs: lambda x: functools.reduce(lambda x, f: f(x), fs, x) 

class Model(torch.nn.Module):
    def __init__(self, in_w=28, in_h=28, out=10):
        super(Model, self).__init__()
        # 28x28 -> 3x21x21 -> 3x14x14 -> 10
        self.conv1 = torch.nn.Conv2d(in_channels=1, out_channels=3, kernel_size=(8, 8))
        self.conv2 = torch.nn.Conv2d(in_channels=3, out_channels=3, kernel_size=(8, 8))
        self.fc = torch.nn.Linear(3 * 14 * 14, out)

    def forward(self, x):
        fs = [
            lambda x: x.unsqueeze(1),
            self.conv1,
            torch.relu,
            self.conv2,
            torch.relu,
            lambda x: x.reshape(x.size(0), -1),
            self.fc
        ]
        return compose(fs)(x)
    
G_model = Model()
G_model, list(map(torch.numel, G_model.parameters()))


G_optim = torch.optim.Adam(G_model.parameters())
G_num_epochs = 5


loss = torch.nn.functional.cross_entropy

accuracy = lambda y, y_target:     (torch.argmax(y, dim=1) == y_target).float().mean()


def check_train():
    loader = G_dataset.make_loader()
    x, y_target = next(iter(loader))
    y = G_model(x)
    l = loss(y, y_target)
    acc = accuracy(y, y_target)
    print(l, acc)

check_train()


float(torch.argmax(torch.tensor([1, 2, 0])))


class Average(object):
    def __init__(self):
        self.n = self.v = 0
        
    def update(self, next_v):
        n = self.n + 1
        self.v = ((n - 1) / n) * self.v + 1 / n * next_v
        self.n = n


from tqdm import tqdm

def train():
    G_model.train(True)
    batch_size = 2**5
    loader = G_dataset.make_loader(batch_size=batch_size)
    avg_loss = Average()
    avg_accuracy = Average()    
    for i in range(G_num_epochs):
        with tqdm(loader, desc=f"Epoch ({i})",
                  total=len(loader), unit_scale=batch_size, unit='samples') as bar:
            for x, y_target in loader:
                G_optim.zero_grad()
                y = G_model(x)
                l = loss(y, y_target)
                l.backward()
                G_optim.step()
                
                acc = accuracy(y, y_target)
                
                avg_loss.update(float(l))
                avg_accuracy.update(float(acc))

                bar.update()
                bar.set_postfix(loss=avg_loss.v, accuracy=avg_accuracy.v)
            
train()


def test():
    G_model.train(False)
    batch_size = 2**5
    loader = G_dataset.make_loader(batch_size=batch_size, test=True)
    avg_loss = Average()
    avg_accuracy = Average()
    with tqdm(loader, desc="Test",
              total=len(loader), unit_scale=batch_size, unit='samples') as bar:
        for x, y_target in loader:
            y = G_model(x)
            l = loss(y, y_target)
            acc = accuracy(y, y_target)
            
            avg_loss.update(float(l))
            avg_accuracy.update(float(acc))
            bar.update()
            bar.set_postfix(loss=avg_loss.v, accuracy=avg_accuracy.v)

test()


def browse_prediction(test=True, prob=False):
    x = G_dataset.test_x if test else G_dataset.train_x 
    y = G_dataset.test_y if test else G_dataset.train_y
    
    print("""
Label Format:
  <sample-index> - <target> - <prediction>
""")
    
    @ipyw.interact(offset=(0, len(x), 1))
    def widget(offset=0, nrows=4, ncols=4, figsize=9):
        _, axes = plt.subplots(nrows, ncols, figsize=(figsize, figsize))
        if prob:
            _, axes_p = plt.subplots(nrows, ncols, figsize=(figsize, figsize))
        for i in range(nrows):
            for j in range(ncols):
                k = offset * nrows * ncols + i * nrows + j
                if k < len(x):
                    x_ = torch.from_numpy(x[k]).float()
                    y_logits = G_model(x_.unsqueeze(0)).squeeze().detach()
                    y_pred = torch.argmax(y_logits)
                    y_prob = torch.softmax(y_logits, 0)
                    ax = axes[i, j]
                    ax.matshow(x_, cmap='Greys')
                    title = f"{k} - {y[k]} - {y_pred}"
                    ax.set(title=title, xticks=[], yticks=[])
                    if y[k] != y_pred:
                        ax.title.set_color('red')
                    if prob:
                        ax_p = axes_p[i, j]
                        ax_p.bar(range(10), y_prob)

browse_prediction(test=True, prob=False)

