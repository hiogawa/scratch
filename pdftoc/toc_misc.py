import re, sys

count = 1
for l in open(sys.argv[1]):
    l = l.strip()
    if not l or re.match('^\-', l):
        print(l)
        count = 1
    else:
        print('-- {}. {}'.format(count, l))
        count = count + 1
