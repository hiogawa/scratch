- 1 Presheaves 1
-- 1.1 Recovering the category from its presheaves? 8
-- 1.2 The Logic of Presheaves 9
--- 1.2.1 First-order structures in categories of presheaves 11
-- 1.3 Two examples and applications 14
--- 1.3.1 Kripke semantics 14
--- 1.3.2 Failure of the Axiom of Choice 16
- 2 Sheaves 19
-- 2.1 Examples of Grothendieck topologies 27
-- 2.2 Structure of the category of sheaves 28
-- 2.3 Application: a model for the independence of the Axiom of Choice 33
-- 2.4 Application: a model for “every function from reals to reals is continuous” 36
- 3 The Effective Topos 41
-- 3.1 Some subcategories and functors 43
-- 3.2 Structure of Eff 44
--- 3.2.1 Finite products 44
--- 3.2.2 Exponentials 45
--- 3.2.3 Natural numbers object 46
--- 3.2.4 Finite Coproducts 47
--- 3.2.5 Finite limits 48
--- 3.2.6 Monics and the subobject classifier 49
-- 3.3 Intermezzo: interpretation of languages and theories in toposes 53
-- 3.4 Elements of the logic of Eff 60
- 4 Morphisms between toposes 64
- 5 Literature 69
