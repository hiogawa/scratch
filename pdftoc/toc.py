import re, sys, subprocess

offset = int(sys.argv[2])

template = '''
BookmarkBegin
BookmarkTitle: {}
BookmarkLevel: {}
BookmarkPageNumber: {}
'''.strip()

for l in open(sys.argv[1]):
    l = l.strip()
    if not l:
        next
    else:
        m = re.search('^(\-*)\ (.*)\ (\d*)$', l)
        if not m:
            raise ValueError('ill formatted line found.', l)
        level, title, page = m.groups()
        print(template.format(title.strip(), len(level), int(page) + offset - 1))
