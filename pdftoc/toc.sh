#!/bin/bash

# Usage:
# $ bash toc.sh in.pdf out.pdf <toc-start-page> <toc-end-page> <first-page-number>
# e.g.
# $ bash toc.sh toposes_triples_theories.pdf toposes_triples_theories_out.pdf 5 6 14

G_THIS_FILE=$(readlink -f "${0}")
G_THIS_DIR="${G_THIS_FILE%/*}"

function Main() {
    local IN_PDF="${1}"
    local OUT_PDF="${2}"
    local TOC_START="${3}"
    local TOC_END="${4}"
    local FIRST_PAGE="${5}"

    local TOC_TXT=$(mktemp -p . --suffix=.txt)
    local METADATA_TXT=$(mktemp --suffix=.txt)
    trap "rm ${TOC_TXT} ${METADATA_TXT}" EXIT

    pdftotext -raw -f "${TOC_START}" -l "${TOC_END}" -nopgbrk -q "${IN_PDF}" - > "${TOC_TXT}"
    pdftk "${IN_PDF}" dump_data_utf8 output "${METADATA_TXT}"

    echo "Metadata is here: ${METADATA_TXT}"
    echo -n "After finished modifying ${TOC_TXT}, press 'y' to continue: "
    read -en 1
    printf "\n=== JUST IN CASE ===\n"
    cat "${TOC_TXT}"
    printf "\n====================\n"

    if [ "${REPLY}" != "y" ]; then exit 1; fi

    # python3 "${G_THIS_DIR}/toc.py" "${TOC_TXT}" "${FIRST_PAGE}" > "${METADATA_TXT}"
    grep -v 'Bookmark' "${METADATA_TXT}" | sponge "${METADATA_TXT}"
    python3 "${G_THIS_DIR}/toc.py" "${TOC_TXT}" "${FIRST_PAGE}" >> "${METADATA_TXT}"
    pdftk "${IN_PDF}" update_info_utf8 "${METADATA_TXT}" output "${OUT_PDF}"
}

Main "${@}"
