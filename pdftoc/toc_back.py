# Usage:
# $ pdftk <input.pdf> dump_data_utf8 output <output.txt>
# $ _py toc_back.py example3.txt 8

import re, sys

pattern = re.compile('''
BookmarkBegin
BookmarkTitle: (.*)
BookmarkLevel: (.*)
BookmarkPageNumber: (.*)
'''.strip())

txt = open(sys.argv[1]).read()
offset = int(sys.argv[2])

for title, level, page in re.findall(pattern, txt):
    print('{} {} {}'.format(
            '-' * int(level),
            title,
            int(page) - offset + 1))
