"""
Shannon's entropy
"""

import numpy as np
import matplotlib.pyplot as plt

xmin, xmax = 0, 0.5
xstep = (xmax - xmin) / 100
xticks = np.arange(xmin, xmax, (xmax - xmin) / 10)

ymin, ymax = 0, 1

xs = np.arange(xmin, xmax, xstep)

yss = [
    ['r', ((- xs * np.log2(xs)) + (- (1 - xs) * np.log2(1 - xs)))],
]

def draw():
    for opts, ys in yss:
        plt.plot(xs, ys, opts)

    plt.axis([xmin, xmax, ymin, ymax])
    plt.grid(True)
    plt.xticks(xticks)

    plt.show()

if __name__ == '__main__':
    draw()
