import numpy as np
import matplotlib.pyplot as plt

xmax = 2. * np.pi
xmin = -xmax
xstep = 0.01
xstep_tick = np.pi / 2.

ymax = 1.
ymin = -ymax

xs = np.arange(xmin, xmax, xstep)

yss = [
    ['r', np.cos(xs)],
    ['g', np.sin(xs)],
    ['b', np.cos(xs / 2)],
    ['c', np.cos(xs / 2) ** 2],
    ['m', - 1 + 2 * (np.cos(xs / 2) ** 2)] # analytically coincides with 'r'
]

def draw():
    for opts, ys in yss:
        plt.plot(xs, ys, opts)

    plt.axis([xmin, xmax, ymin, ymax])
    plt.grid(True)
    plt.xticks(np.arange(xmin, xmax, xstep_tick))

    plt.show()

if __name__ == '__main__':
    draw()
