import numpy as np
import matplotlib.pyplot as plt

xmin, xmax = -1, 1
xstep = (xmax - xmin) / 100
xticks = np.arange(xmin, xmax, (xmax - xmin) / 10)

ymin, ymax = -5, 5

xs = np.arange(xmin, xmax, xstep)

yss = [
    ['r', np.tan(xs * np.pi / 2)],
    ['g', xs],
]

def draw():
    for opts, ys in yss:
        plt.plot(xs, ys, opts)

    plt.axis([xmin, xmax, ymin, ymax])
    plt.grid(True)
    plt.xticks(xticks)

    plt.show()

if __name__ == '__main__':
    draw()
