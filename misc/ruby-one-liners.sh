# Ruby one liners
# - filter line with "#"
#   - ruby -ne 'puts $_ if !$_.match("^#")'
# - sort
#   - ruby -e 'puts STDIN.read.lines.sort'
# - unique
#   - ruby -e 'puts STDIN.read.lines.uniq'
# - nth column
#   - ruby -ne 'puts $_.strip.split[n]'
# - show unicode index
#   - $_.codepoints
# - list characters with unicode
#   - ruby -e 'STDIN.each_char.uniq.reject{ |c| c.match(/[\r\n\t\ ]/) }.sort.each{ |c| printf "%s U+%05x\n", c, c.codepoints[0] }'
# - list unicode within range
#   - ruby -e '(0x0430..0x451).to_a.pack("U*").each_char{ |c| puts c }'
