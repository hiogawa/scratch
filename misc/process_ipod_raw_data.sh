#!/bin/bash

function _sanitize_filename() {
    if [ -z "${1}" ]; then
        echo "Usage: _sanitize_filename <base directory>"
        return 1
    fi

    python3 - "${1}" << EOF
import pathlib, sys, re
pattern = r'[\"\:\?]'
for f in pathlib.Path(sys.argv[1]).glob('**/*'):
    if re.search(pattern, f.name):
        g = f.with_name(re.sub(pattern, ' ', f.name))
        print("{}\t==>\t{}".format(f.name, g.name))
        f.rename(g)
EOF
}


# ruby -r json -r i18n -e 'puts I18n::Backend::Transliterator::HashTransliterator::DEFAULT_APPROXIMATIONS.to_json'
function _parameterize() {
    ruby - "${1}" <<EOF
require "active_support/inflector"
puts(ActiveSupport::Inflector.parameterize(ARGV[0], separator: "_", preserve_case: true))
EOF
}

function _resolve_conflict_by_numbering() {
    python3 - "${1}" << EOF
import pathlib, sys, re
p = pathlib.Path(sys.argv[1])
if not p.parent.exists():
    print(p)
    exit(0)
i = 0
part0 = p.name[0:len(p.name) - len(p.suffix)]
part1 = p.suffix
for f in p.parent.iterdir():
    if f.name == p.name:
        i = i + 1
    pattern = '{}\.BACKUP\_\d*{}'.format(re.escape(part0), re.escape(part1))
    if re.search(pattern, f.name):
        i = i + 1
if i == 0:
    print(p)
else:
    print(p.with_name('{}.BACKUP_{}{}'.format(part0, i, part1)))
EOF
}

function _organize_ipod_data() {
    local ORIG_DIR="${HOME}/Documents/mnt/ipod_data/iPod_Control/Music"
    local DEST_DIR="ipod"
    local START_DIR_N="0"
    local LAST_DIR_N="49"
    local WORKAROUND0="_NO_METADATA"
    local WORKAROUND1="_NON_ASCII"
    local NUM_FFMPEG_WORKERS="4"

    # Convert to .opus from .m4a or .mp3
    for (( i = "${START_DIR_N}"; i <= "${LAST_DIR_N}"; i++ )); do
        ii=$(printf "%02d" $i)
        d="${ORIG_DIR}/F${ii}"
        e="${DEST_DIR}/F${ii}"
        mkdir -p "${e}"

        # Parallelly drive ffmpeg via python
        python3 - "${d}" "${e}" << EOF
import pathlib, sys, subprocess, multiprocessing, os

orig_dir = pathlib.Path(sys.argv[1])
dest_dir = pathlib.Path(sys.argv[2])

def call_ffmpeg(orig):
    orig = pathlib.Path(orig)
    dest = dest_dir.joinpath(orig.name + '.opus')
    print('Python (PID {}) spawns ffmpeg: {} ==> {}'.format(os.getpid(), orig, dest))
    command = 'ffmpeg -n -hide_banner -loglevel warning -i {} {}'.format(orig, dest)
    proc = subprocess.run(command.split())
    if proc.returncode != 0:
        print("Command ({}) have failed".format(command))

multiprocessing.set_start_method('fork')
with multiprocessing.Pool(${NUM_FFMPEG_WORKERS}) as pool:
    pool.map(call_ffmpeg, orig_dir.glob('*'))
EOF
    done

    for (( i = "${START_DIR_N}"; i <= "${LAST_DIR_N}"; i++ )); do
        ii=$(printf "%02d" $i)
        orig_d="${DEST_DIR}/F${ii}"
        dest_d="${DEST_DIR}/library"
        for f in "${orig_d}/"*.opus; do
            echo "== Processing ${f} =="
            ffprobe_tmp=$(ffprobe -hide_banner "${f}" 2>&1)
            artist=$(echo "${ffprobe_tmp}" | grep ARTIST | awk '{ for(i=3;i<=NF;++i) printf $i FS }')
            artist_d=$(_parameterize "${artist}")
            if [ -z "${artist}" ]; then
                echo "-- No ARTIST found in ${f} --"
                artist_d="${WORKAROUND0}"
            elif [ -z "${artist_d}" ]; then
                echo "-- \"${artist}\" is all non ascii --"
                artist_d="${WORKAROUND1}"
            else
                echo "1. ARTIST: ${artist} ==> ${artist_d}"
            fi

            album=$(echo "${ffprobe_tmp}" | grep ALBUM  | awk '{ for(i=3;i<=NF;++i) printf $i FS }')
            album_d=$(_parameterize "${album}")
            if [ -z "${album}" ]; then
                echo "-- No ALBUM found in ${f} --"
                album_d="${WORKAROUND0}"
            elif [ -z "${album_d}" ]; then
                echo "-- \"${album_d}\" is all non ascii --"
                album_d="${WORKAROUND1}"
            else
                echo "2. ALBUM : ${album} ==> ${album_d}"
            fi

            title=$(echo "${ffprobe_tmp}" | grep TITLE  | awk '{ for(i=3;i<=NF;++i) printf $i FS }')
            title_f=$(_parameterize "${title}")
            if [ -z "${title}" ]; then
                echo "-- No TITLE found in ${f} --"
                title_f="${WORKAROUND0}"
            elif [ -z "${title_f}" ]; then
                echo "-- \"${title}\" is all non ascii --"
                title_f="${WORKAROUND1}"
            else
                echo "3. TITLE : ${title} ==> ${title_f}"
            fi
            title_f="${title_f}.opus"

            final_dest_d="${dest_d}/${artist_d}/${album_d}"
            final_dest=$(_resolve_conflict_by_numbering "${final_dest_d}/${title_f}")
            mkdir -p "${final_dest_d}"
            echo "${f} ==> ${final_dest}"
            mv --backup=numbered "${f}" "${final_dest}"
        done
    done
}
