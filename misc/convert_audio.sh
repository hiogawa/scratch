#!/bin/bash

function Run() {
    local IN_FILE="${1}"
    local OUT_FILE="${IN_FILE%\.*}".opus

    echo "=== Processing: ${IN_FILE} ==="
    ffmpeg -v warning -y -hide_banner -i "${IN_FILE}" "${OUT_FILE}"
    echo "(Finished)"
    rm -f "${IN_FILE}"
}

function Main() {
    for FILE in "${@}"; do
        if [ "${FILE##*\.}" == "opus" ]; then
            echo "== (Skipped: ${FILE})  =="
            continue
        fi

        Run "${FILE}"
    done
    echo "### ALL DONE ###"
}

function MainWrapper() {
    local arg="${1}"
    case "${arg}" in
        "--dialog")
            shift
            Main "${@}" 2>&1 | bash ~/code/hiogawa/scratch/misc/dialog.sh --title="Convert to opus" --no-cancel
        ;;
        "--recursive")
            shift
            shopt -s globstar nullglob
            echo "== Are you sure to convert these files? =="
            for FILE in **/*.{mp3,m4a,flac,ogg,mkv,web}; do echo "${FILE}" ; done
            echo -n "== PRESS 'y' to continue: "
            read -en 1
            if [ "${REPLY}" != "y" ]; then exit 1; fi
            for FILE in **/*.{mp3,m4a,flac,ogg,mkv,web}; do Run "${FILE}" ; done
        ;;
        *)
            Main "${@}"
        ;;
    esac
}

MainWrapper "${@}"
