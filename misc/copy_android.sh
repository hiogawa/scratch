#!/bin/bash

G_ORIG_BASE="${HOME}/code/hiogawa/blob/music"
G_DEST_BASE="/run/media/hiogawa/SS/music"

# function Run() {
#     local IN_FILE="${1}"
#     local OUT_FILE="out/${IN_FILE}"

#     local FFPROBE_OUT=$(ffprobe -hide_banner -show_streams "${IN_FILE}" 2>&1)
#     local FAKE_ARTIST="Various Artists"
#     local TITLE=$(echo "${FFPROBE_OUT}" | perl -ne '/TAG:TITLE=(.*)/ && print "$1\n";')
#     local ARTIST=$(echo "${FFPROBE_OUT}" | perl -ne '/TAG:ARTIST=(.*)/ && print "$1\n";')

#     mkdir -p "${OUT_FILE%/*}"
#     ffmpeg -v warning -y -hide_banner -i "${IN_FILE}" \
#         -metadata:s:a:0 "TITLE=${ARTIST} - ${TITLE}" \
#         -metadata:s:a:0 "ARTIST=${FAKE_ARTIST}" \
#         -codec copy \
#         "${OUT_FILE}"
# }

function Main() {
    # echo -n ":: Proceed to convert? [Y/n]"
    # read -en 1
    # if [ "${REPLY}" != "y" ]; then
    #     echo ":: Canceled"
    #     exit 1
    # fi

    # echo ":: Processing..."
    # local I=0
    # for FILE in **/*.opus; do
    #     I=$(( I + 1 ))
    #     printf "(${I}/${NUM_OF_FILES})\t${FILE}\n"
    #     Run "${FILE}"
    # done

    local FILES=("${@}")

    echo ":: Copying files..."
    for (( i=0; i<"${#FILES[@]}"; i++ )) do
        local FILE="${FILES[$i]}"
        local FILE_LIB_PATH="${FILE##${G_ORIG_BASE}}"
        local FILE_DEST="${G_DEST_BASE}${FILE_LIB_PATH}"
        local MSG="($((i+1))/${#FILES[@]})\t${FILE_LIB_PATH}\t"
        if test -f "${FILE_DEST}"; then
            MSG="${MSG}** FILE EXISTS!! **"
        else
            mkdir -p "${FILE_DEST%/*}"
            cp "${FILE}" "${FILE_DEST}"
            MSG="${MSG}** DONE **"
        fi
        printf "${MSG}\n"
    done
    echo ":: Finished"
}

Main "${@}" 2>&1 | bash ~/code/hiogawa/scratch/misc/dialog.sh --title="Copy files" --no-cancel
