#!/bin/bash

# Setup
# - $ ln -sf $PWD/fix_audio_file.sh ~/.local/bin/fix_audio_file.sh
# - Add this to Cantata's custom actions

function Run() {
    local f="${1}"
    tmp_mp3=$(mktemp --suffix=.mp3)
    tmp_opus=$(mktemp --suffix=.opus)

    title=$(ffprobe "${f}" -v quiet -show_streams | grep 'TAG:TITLE' | cut -d '=' -f 2- | head -n 1)
    artist=$(ffprobe "${f}" -v quiet -show_streams | grep 'TAG:ARTIST' | cut -d '=' -f 2- | head -n 1)

    ffmpeg -y -hide_banner -i "${f}" "${tmp_mp3}"
    ffmpeg -y -hide_banner -i "${tmp_mp3}" -metadata "title=${title}" -metadata "artist=${artist}" "${tmp_opus}"

    rm -f "${tmp_mp3}"
    mv -f "${tmp_opus}" "${f}"
    sleep 3
}

for f in "${@}"; do
    if [ "${f##*\.}" != "opus" -a "${f##*\.}" != "ogg" ]; then
        Xdialog --title="fix_audio_file.sh" --no-buttons --infobox "File (\"${f##*/}\") has to be opus or ogg" 0x0 0
        continue
    fi

    Run "${f}" | \
        Xdialog --title="fix_audio_file.sh" --no-close --no-buttons --infobox " Fixing \"${f##*/}\" ... " 0x0 0
done
