#!/bin/bash

# Global variables
G_TEXT=$(xclip -out)
G_LANG="${1}" # e.g. German (de), English (en-us)
G_SAVE="${2}"
G_CACHE_DIR="${HOME}/.cache/speak_sh"
G_SAVE_DIR="${HOME}/code/hiogawa/blob/music"

function Msg() {
    Xdialog --title="speak.sh" --msgbox "${1}" 0x0
}

function Hash() {
    python3 - "${1}" <<EOF
import hashlib, sys
print(hashlib.md5(sys.argv[1].encode('utf-8')).hexdigest())
EOF
}

function TextToSpeach() {
    local FILE="${1}"

    # Unofficial google translate text-to-speech api
    # References
    # - https://elinux.org/RPi_Text_to_Speech_(Speech_Synthesis)
    # - or you can inspect how translate.google.com use the endpoint
    curl -f -G -H 'User-Agent: Chrome' \
        --data-urlencode "q=${G_TEXT}" \
        "http://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=${G_LANG}" \
        > "${FILE}"

    if [ "${?}" != "0" ]; then
        Msg "curl \"translate.google.com\" failed."
        return 1
    fi
}

function TextToSpeachCached() {
    local CACHE_DIR="${G_CACHE_DIR}/${G_LANG}"
    local HASH=$(Hash "${G_TEXT}")
    local CACHE_AUDIO="${CACHE_DIR}/${HASH}.mp3"
    local CACHE_TEXT="${CACHE_DIR}/${HASH}.txt"

    mkdir -p "${CACHE_DIR}"
    if [ ! -f "${CACHE_AUDIO}" ]; then
        TextToSpeach "${CACHE_AUDIO}"
        if [ "${?}" != "0" ]; then
            rm "${CACHE_AUDIO}"
            return 1
        fi
        echo "${G_TEXT}" > "${CACHE_TEXT}"
    fi
    mplayer -ao alsa -really-quiet -noconsolecontrols "${CACHE_AUDIO}"

    if [ -n "${G_SAVE}" ]; then
        Save "${CACHE_AUDIO}"
    fi
}

function Save() {
    local IN_MP3="${1}" # .mp3
    local ARTIST="German Practice"
    local ALBUM="Vocabulary $(date +"%Y-%m")"
    local TITLE="${G_TEXT}"
    local OUT_DIR="${G_SAVE_DIR}/${ARTIST}/${ALBUM}"
    mkdir -p "${OUT_DIR}"
    # Just a rough numbering (if some file happens to be deleted, create some script to renumber them based on creation time)
    local TRACK=$(( $(ls "${OUT_DIR}" | wc -l) + 1 ))
    # Borrowing hashed filename so that there won't be duplicated file here
    local FILE="${IN_MP3##*/}"; FILE="${FILE%.mp3}"
    local OUT_OPUS="${OUT_DIR}/${FILE}.opus"
    ffmpeg -y -hide_banner -i "${IN_MP3}" \
        -metadata "artist=${ARTIST}" -metadata "album=${ALBUM}" \
        -metadata "title=${TITLE}" -metadata "track=${TRACK}" \
        "${OUT_OPUS}"
}

function Main() {
    if [ -z "${G_TEXT}" ]; then
        Msg "Text is not selected."
        return 1
    fi
    TextToSpeachCached
}

Main
