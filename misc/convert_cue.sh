#!/bin/bash

function Run() {
    local IN_FILE="${1}"
    local TRACK="${2}"
    local TITLE="${3}"
    local START="${4}"
    local END="${5}"
    local OUT_FILE="${TRACK} - ${TITLE}".opus

    ffmpeg -y -hide_banner -i "${IN_FILE}" -ss "${START}" -to "${END}" \
        -metadata track="${TRACK}" -metadata title="${TITLE}" "${OUT_FILE}"
}

FILE1="CD1/Keith Jarrett , Gary Peacock , Jack DeJohnette - After The Fall (CD-1).flac"
Run "${FILE1}" "1" "The Masquerade Is Over"  "00:00" "15:49"
Run "${FILE1}" "2" "Scrapple From The Apple" "15:49" "24:36"
Run "${FILE1}" "3" "Old Folks"               "24:36" "33:59"
Run "${FILE1}" "4" "Autumn Leaves"           "33:59" "47:17"

FILE2="CD2/Keith Jarrett , Gary Peacock , Jack DeJohnette - After The Fall (CD - 2).flac"
Run "${FILE2}" "1" "Bouncin' With Bud"             "00:00" "10:01"
Run "${FILE2}" "2" "Doxy"                          "10:01" "18:49"
Run "${FILE2}" "3" "I'll See You Again"            "18:49" "26:37"
Run "${FILE2}" "4" "Late Lament"                   "26:37" "31:36"
Run "${FILE2}" "5" "One For Majid"                 "31:36" "38:23"
Run "${FILE2}" "6" "Santa Claus Is Coming To Town" "38:23" "46:11"
Run "${FILE2}" "7" "Moment's Notice"               "46:11" "52:52"
Run "${FILE2}" "8" "When I Fall In Love"           "52:52" "58:29"
