#!/bin/bash

# How to setup:
# $ ln -sr $PWD/lookup.sh ~/.local/bin
# $ # go to gnome setting and add custom shortcut

G_TEXT=$(xclip -out)

if [ -z "${G_TEXT}" ]; then
    zenity --error --no-wrap --text="lookup.sh: text is not selected."
    exit 1
fi

function Main() {
    case "${1}" in
        # bash misc/lookup.up define
        # bash misc/lookup.up etymology
        define|etymology)
            xdg-open "https://www.google.com/search?gws_rd=cr&q=${1} ${G_TEXT}"
        ;;

        # bash misc/lookup.sh example deu eng
        example)
            local FROM="${2}" # deu
            local TO="${3}"   # eng
            xdg-open "https://tatoeba.org/eng/sentences/search?query=${G_TEXT}&from=${FROM}&to=${TO}&orphans=no&unapproved=no&user=&tags=&list=&has_audio=&trans_filter=limit&trans_to=eng&trans_link=&trans_user=&trans_orphan=&trans_unapproved=&trans_has_audio=&sort=random"
        ;;
    esac
}

Main "${@}"
