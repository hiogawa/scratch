#!/bin/bash

TEXT=$(xclip -out)

if [ -z "$TEXT" ]; then
    Xdialog --title="translate.sh" --no-buttons --infobox "${1}" 0x0 0
    exit 1
fi

xdg-open "https://translate.google.com/#${1}/${2}/$TEXT"
