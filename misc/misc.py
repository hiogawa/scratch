import pathlib
import shutil
import subprocess
import tempfile

def run(cmd):
    proc = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if proc.returncode != 0:
        print('-- FAILED: {} --'.format(cmd))
        print(proc.stdout)
        print(proc.stderr)

def add_metadata(file, **kwargs):
    with tempfile.NamedTemporaryFile(suffix='.webm') as tmp:
        print('Executing ffmpeg ...')
        kwargs['artist'] = ''
        kwargs['album'] = ''
        # kwargs['title'] = ''
        kwargs['track'] = ''
        run('''
            ffmpeg -y -v quiet -i '{infile}' \
            -metadata 'artist={artist}' -metadata 'album={album}' \
            -metadata 'title={title}' -metadata 'track={track}' \
            -codec copy \
            '{outfile}'
        '''.strip().format(infile=file, outfile=tmp.name, **kwargs))
        shutil.copy(tmp.name, file)

def main():
    pass

if __name__ == '__main__':
    main()
    exit(0)

'''
python3 - <<EOF
import re

s = ''

ls = s.strip().split('\n')
buf = ''
for i, l in enumerate(ls):
    if re.search('^[0-9]', l):
        if buf:
            print(buf)
        buf = l
    else:
        buf = buf + ' ' + l
print(buf)

EOF
'''

'''
function TextToSpeach() {
    local FILE="${1}"
    local TEXT="${2}"
    local LANG="${3}"
    curl -f -G -H 'User-Agent: Chrome' \
        --data-urlencode "q=${TEXT}" \
        "http://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=${LANG}" \
        > "${FILE}"
    if [ "${?}" != "0" ]; then
        echo "=== FAILED ==="
        echo "${FILE}"
        echo "${TEXT}"
        return 1
    fi
}
N="1"
cat german/data/sentences/data1.txt | while read LINE; do
    if [ -n "${LINE}" ]; then
        LINE="${LINE#*\.\ }"
        FILE=$(printf "%02d.mp3" "${N}")
        FILE="german/data/sentences/tmp/${FILE}"
        ffprobe -v quiet "${FILE}"
        if [ "${?}" != "0" ]; then
            ff
            # TextToSpeach "${FILE}" "${LINE}" de
        fi
        N=$(( N + 1 ))
    fi
done
'''
