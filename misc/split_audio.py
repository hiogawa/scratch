'''
Split audio files downloaded from youtube (e.g. https://www.youtube.com/watch?v=Bo1uygwCnrE)

Usage:
$ python3 split_audio.y 'Complete Beethoven Piano Sonatas - Claudio Arrau-Bo1uygwCnrE.opus' 'Claudio Arrau' 'Complete Beethoven Piano Sonatas & Concertos'
'''

# Copied timestamps from the description
'''
Piano Sonata No. 1
- Allegro 00:00
- Adagio 05:23
- Menuetto - Allegretto 11:08
- Prestissimo - 14:09

Piano Sonata No. 2
- Allegro vivace 21:42
- Largo appassionato 32:28
- Scherzo: Allegretto 40:41
- Rondo: Grazioso 43:59

Piano Sonata No. 3
- Allegro con brio 50:58
- Adagio 1:01:44
- Scherzo: Allegro 1:12:02
- Allegro assai 1:15:21

Piano Sonata No. 4
- Allegro molto e con brio 1:21:07
- Largo, con gran espressione 1:29:49
- Allegro 1:39:33
- Rondo: Poco allegretto e grazioso 1:44:47

Piano Sonata No. 5
- Allegro molto e con brio 1:52:37
- Adagio molto 1:58:25
- Prestissimo 2:06:59

Piano Sonata No. 6
- Allegro 2:11:38
- Allegretto 2:19:58
- Presto 2:24:00

Piano Sonata No. 7
- Presto 2:28:06
- Largo e mesto 2:34:51
- Menuetto: Allegro 2:45:17
- Rondo: Allegro 2:48:18

Piano Sonata No. 8 "Pathétique"
- Grave - Allegro di molto e con brio 2:52:33
- Adagio cantabile 3:01:39
- Rondo: Allegro 3:07:58

Piano Sonata No. 9
- Allegro 3:12:34
- Allegretto 3:19:16
- Rondo - Allegro comodo 3:22:48

Piano Sonata No. 10
- Allegro 3:26:17
- Andante 3:33:43
- Scherzo: Allegro assai 3:39:56

Piano Sonata No. 11
- Allegro con brio 3:43:23
- Adagio con molto espressione 3:50:53
- Menuetto 4:01:40
- Rondo: Allegretto 4:04:59

Piano Sonata No. 12
- Andante con variazioni 4:11:50
- Scherzo, allegro molto 4:20:38
- Marcia funebre 4:23:13
- Allegro 4:29:20

Piano Sonata No. 13
- Andante - Allegro - Andante 4:32:48
- Allegro molto e vivace 4:38:48
- Adagio con espressione 4:41:03
- Allegro vivace 4:44:32

Piano Sonata No. 14 "Moonlight"
- Adagio sostenuto 4:50:21
- Allegretto 4:57:08
- Presto agitato 4:59:36

Piano Sonata No. 15 "Pastorale"
- Allegro 5:07:29
- Andante 5:17:02
- Scherzo: Allegro vivace 5:24:54
- Rondo: Allegro ma non troppo 5:26:58

Piano Sonata No. 16
- Allegro vivace 5:32:05
- Adagio grazioso 5:38:40
- Rondo: Allegretto - Presto 5:50:58

Piano Sonata No. 17 "Tempest"
- Largo - Allegro 5:57:25
- Adagio 6:06:04
- Allegretto 6:16:26

Piano Sonata No. 18
- Allegro 6:24:08
- Scherzo: Allegretto vivace 6:32:49
- Menuetto: Moderato e grazioso 6:38:26
- Presto con fuoco 6:43:46

Piano Sonata No. 19
- Andante 6:48:53
- Rondo: Allegro 6:53:28

Piano Sonata No. 20
- Allegro ma non troppo 6:57:10
- Tempo di Menuetto 7:02:08

Piano Sonata No. 21 "Waldstein"
- Allegro con brio 7:06:12
- Introduzione: Adagio molto - attacca 7:18:00
- Rondo: Allegretto moderato - Prestissimo 7:22:38

Piano Sonata No. 22
- Tempo d'un menuetto 7:33:11
- Allegretto - Più allegro 7:39:25

Piano Sonata No. 23 "Appassionata"
- Allegro assai 7:45:53
- Andante con moto 7:57:07
- Allegro ma non troppo - Presto 8:04:28

Piano Sonata No. 24 "For Therese"
- Adagio cantabile - Allegro ma non troppo 8:12:51
- Allegro vivace 8:20:20

Piano Sonata No. 25
- Presto alla tedesca 8:23:25
- Andante 8:28:20
- Vivace 8:31:26

Piano Sonata No. 26 "Les Adieux"
- Das Lebowohl 8:33:33
- Abwesenheit 8:40:35
- Das Wiedersehen 8:44:23

Piano Sonata No. 27
- Mit Lebhaftigkeit 8:50:49
- Nicht zu geschwind 8:56:01

Piano Sonata No. 28
- Etwas lebhaft 9:04:07
- Lebhaft, Marschmäßig 9:08:52
- Langsam und sehnsuchtsvoll 9:14:58
- Geschwind 9:18:48

Piano Sonata No. 29 "Hammerklavier"
- Allegro 9:26:48
- Scherzo: Assai vivace 9:37:34
- Adagio sostenuto 9:40:07
- Introduzione: Largo - Fuga: Allegro risoluto 10:00:38

Piano Sonata No. 30
- Vivace ma non troppo 10:12:24
- Prestissimo 10:16:22
- Andante molto cantabile 10:18:59

Piano Sonata No. 31
- Moderato cantabile 10:35:32
- Allegro molto 10:42:29
- Adagio ma non troppo 10:44:51
- Fuga - Allegro ma non troppo 10:48:47

Piano Sonata No. 32
- Maestoso - Allegro con brio 10:56:07
- Arietta: Adagio molto 11:05:23
'''

# extracted timestamps where to cut
timestamps = '''
00:00
21:42
50:58
1:21:07
1:52:37
2:11:38
2:28:06
2:52:33
3:12:34
3:26:17
3:43:23
4:11:50
4:32:48
4:50:21
5:07:29
5:32:05
5:57:25
6:24:08
6:48:53
6:57:10
7:06:12
7:33:11
7:45:53
8:12:51
8:23:25
8:33:33
8:50:49
9:04:07
9:26:48
10:12:24
10:35:32
10:56:07
11:25:04
'''.strip().split('\n')

import subprocess
import sys
import shlex

IN_FILE=sys.argv[1]
ARTIST=sys.argv[2]
ALBUM=sys.argv[3]
TITLE='Piano Sonata No. {}'

def execute(cmd):
    proc = subprocess.run(cmd, shell=True, executable='/bin/bash', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if proc.returncode != 0:
        print('-- FAILED: {} --'.format(cmd))
        print(proc.stdout); print(proc.stderr)

def Q(**kwargs):
    for k in kwargs.keys():
        kwargs[k] = shlex.quote(str(kwargs[k]))
    return kwargs

def main():
    for i in range(len(timestamps) - 1):
        start = timestamps[i]
        end = timestamps[i+1]
        execute('-- EXECUTING FFMPEG ({}) --'.format(i+1))
        execute('''
            ffmpeg -y -hide_banner -i {infile} -ss {start} -to {end} \
            -metadata artist={artist} -metadata album={album} \
            -metadata title={title} -metadata track={track} \
            -codec copy \
            {outfile}
        '''.strip().format(**Q(
            infile=IN_FILE, outfile='{}.opus'.format(i+1),
            start=start, end=end,
            artist=ARTIST, album=ALBUM,
            title=TITLE.format(i+1), track=str(i+1))))

main()
