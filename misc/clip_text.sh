#!/bin/bash

# Summary: screenshot + ocr (+ lookup.sh/speak.sh)
# Usage: bash clip_text.sh deu

# Reference:
# - "mogrify" tips from https://askubuntu.com/questions/868089/how-can-i-use-ocr-on-a-partial-screen-capture-to-get-text/869297

function Msg() {
    Xdialog --title="clip_text.sh" --msgbox "${1}" 0x0
}

function Main() {
    local LANG="${1}"
    local TEXT
    local IMG_FILE=$(mktemp --suffix=.png)
    trap "rm ${IMG_FILE}" EXIT

    gnome-screenshot -a -f "${IMG_FILE}"
    if [ "$(file -b ${IMG_FILE})" == "empty" ]; then
        return 0
    fi

    mogrify -modulate 100,0 -resize 400% "${IMG_FILE}"
    TEXT=$(tesseract "${IMG_FILE}" stdout -l "${LANG}")
    if [ -z "${TEXT}" ]; then
        Msg "Text is not recognized."
        return 0
    fi

    killall xclip >/dev/null 2>&1
    echo "${TEXT}" | xclip -i
    echo "${TEXT}" | xclip -selection clipboard -i
}

Main "${@}"
