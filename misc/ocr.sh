#!/bin/bash

# how to use:
# - in evince, save image as /tmp/tesseract/in.png
# - run script (from gnome shell shortcut)

IN_FILE="/tmp/tesseract/in.png"
OUT_FILE_BASE="/tmp/tesseract/out"
OUT_FILE="$OUT_FILE_BASE.pdf"

tesseract $IN_FILE $OUT_FILE_BASE -l eng pdf
evince $OUT_FILE
