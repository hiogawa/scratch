#!/bin/bash

# Usage:
# $ bash various_artists.sh

function Run() {
    local IN_FILE="${1}"
    local OUT_FILE="out/${IN_FILE}"

    local FFPROBE_OUT=$(ffprobe -hide_banner -show_streams "${IN_FILE}" 2>&1)
    local FAKE_ARTIST="Various Artists"
    local TITLE=$(echo "${FFPROBE_OUT}" | perl -ne '/TAG:TITLE=(.*)/ && print "$1\n";')
    local ARTIST=$(echo "${FFPROBE_OUT}" | perl -ne '/TAG:ARTIST=(.*)/ && print "$1\n";')

    mkdir -p "${OUT_FILE%/*}"
    ffmpeg -v warning -y -hide_banner -i "${IN_FILE}" \
        -metadata:s:a:0 "TITLE=${ARTIST} - ${TITLE}" \
        -metadata:s:a:0 "ARTIST=${FAKE_ARTIST}" \
        -codec copy \
        "${OUT_FILE}"
}

function Main() {
    shopt -s globstar nullglob
    local NUM_OF_FILES=0

    echo ":: Checking audio files to convert..."
    for FILE in **/*.opus; do
        NUM_OF_FILES=$(( NUM_OF_FILES + 1 ))
        echo "${FILE}"
    done

    echo -n ":: Proceed to convert? [y/n]"
    read -en 1
    if [ "${REPLY}" != "y" ]; then
        echo ":: Canceled"
        exit 1
    fi

    echo ":: Processing..."
    local I=0
    for FILE in **/*.opus; do
        I=$(( I + 1 ))
        printf "(${I}/${NUM_OF_FILES})\t${FILE}\n"
        Run "${FILE}"
    done

    echo ":: Finished"
}

Main "${@}"
