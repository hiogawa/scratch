Some notes on pdftk usage

- General workflow to embed table-of-content metadata to pdf

```
# Extract TOC part (page_start to page_end) of pdf as plain text
$ pdftotext -raw -f <page_start> -l <page_end> -nopgbrk -q file.pdf - > toc.txt

# Process this text data into pdftk compatible metadata txt format
$ ... text processing or manually do something ... > toc_metadata.txt

# Generate metadata file from original
$ pdftk file.pdf dump_data_utf8 output metadata.txt

# Create "Bookmark" metadata and merge it
$ cat metadata.txt toc_metadata.txt > metadata_updated.txt
$ pdftk file.pdf update_info_utf8 metadata_updated.txt output file_updated.pdf
```
