def tput(arg):
    capname, *params = arg.split()
    params = map(int, params)
    _bytes = curses.tparm(curses.tigetstr(capname), *params)
    # Convert to str for "print"-friendliness and formatting
    return str(_bytes, 'utf-8')

print(tput('setaf 3'), 'color-3', tput('sgr0'), 'reset', tput('bold'), 'bold')
