# GOAL
# - something like Zotero but just simply on top of a native file system with symbolic link

# TODO
# - import book info from amazon
# - pdf meta data extraction
# - title name conflict

# Usage:
# $ python3 main.py <category> <url>

# e.g.
# $ python3 main.py papers https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3705962/
# $ tree papers
# papers
# ├── Principles_for_designing_ideal_protein_structures
# │   ├── _backup.html
# │   ├── _meta.json
# │   └── Principles_for_designing_ideal_protein_structures.pdf
# ├─..

# Some tested website
# - PMC (PubMed Central) https://www.ncbi.nlm.nih.gov/pmc/
# - Paperity http://paperity.org/
# - arXiv
# - PLOS

import json
from lxml import etree
import os
import re
import requests
import shutil
import subprocess
import sys
from urllib.parse import urlparse, parse_qs


# Taken from Chromium
_HEADERS = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36',
    # 'user-agent': 'curl/7.58.0',
    # 'accept': '*/*',
    # 'host': 'www.youtube.com'
}


def get_meta(doc, url):
    h = {}

    h['__URL__'] = [url]
    h['__TITLE__'] = []
    h['__PDF_URL__'] = []

    for e in doc.cssselect('meta'):
        key = e.get('name')
        value = e.get('content')
        if key:
            if h.get(key):
                h[key].append(value)
            else:
                h[key] = [value]

            if key == 'citation_title':
                h['__TITLE__'].append(value)

            if key == 'citation_pdf_url':
                h['__PDF_URL__'].append(value)

    for e in doc.cssselect('link'):
        if e.get('rel') == 'alternate' and e.get('type') == 'application/pdf':
            h['__PDF_URL__'].append(e.get('href'))

    if not h['__TITLE__']:
        h['__TITLE__'].append(doc.cssselect('head > title')[0].text)

    if h['__PDF_URL__']:
        if not urlparse(h['__PDF_URL__'][0]).scheme:
            url_p = urlparse(url)
            h['__PDF_URL__'][0] = url_p.scheme + '://' + url_p.netloc + '/' + h['__PDF_URL__'][0]

    return h


def clean_url(url):
    return urlparse(url)._replace(params='', query='', fragment='').geturl()


def url2params(url):
    return {k: vs[0] for k, vs in parse_qs(urlparse(url).query).items()}


def main(base_dir, url):
    os.makedirs(base_dir, exist_ok=True)

    # import pdb; pdb.set_trace()
    resp = requests.get(clean_url(url), params=url2params(url), headers=_HEADERS)
    doc = etree.HTML(resp.content)
    meta = get_meta(doc, url)
    title_sanitized = re.sub(r'[^A-Za-z0-9]', '_', meta['__TITLE__'][0])
    content_dir = base_dir + '/' + title_sanitized

    os.makedirs(content_dir, exist_ok=True)

    with open(content_dir + '/_backup.html', 'wb') as f:
        f.write(resp.content)

    with open(content_dir + '/_meta.json', 'w') as f:
        f.write(json.dumps(meta, sort_keys=True, indent=4))

    if meta['__PDF_URL__']:
        with open(content_dir + '/' + title_sanitized + '.pdf', 'wb') as f:
            print('downloading pdf ...')
            resp_pdf = requests.get(clean_url(meta['__PDF_URL__'][0]), params=url2params(meta['__PDF_URL__'][0]), headers=_HEADERS, stream=True)
            resp_pdf.raw.decode_content = True
            shutil.copyfileobj(resp_pdf.raw, f)

    # if re.search('www.youtube.com', meta['__URL__'][0]):
    #     print('=== EXECUTING youtube-dl ===')
    #     cmd = "youtube-dl -o \"{dir}/{title}.%(ext)s\" {audio_opts} --prefer-free-formats {url}".format(
    #         dir=content_dir,
    #         title=title_sanitized,
    #         url=meta['__URL__'][0],
    #         audio_opts='' # '--extract-audio'
    #     )
    #     subprocess.run(cmd, shell=True)
    #     print('=== DONE ===')


main(sys.argv[1], sys.argv[2])
