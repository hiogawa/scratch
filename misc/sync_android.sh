#!/bin/bash

# Setup
# - $ ln -sf $PWD/sync_android.sh ~/.local/bin/sync_android.sh
# - Add this to Cantata's custom actions

function Msg() {
    Xdialog --title="sync_android.sh" --no-buttons --infobox "${1}" 0x0 0
}

function AskYesNo() {
    Xdialog --title="sync_android.sh" --no-buttons --yesno "${1}" 0x0 0
}


mkdir -p ~/.cache/sync_android_sh
function AskInputCached() {
    local CACHE OUT
    CACHE=~/.cache/sync_android_sh/"${1}"
    touch "${CACHE}"
    OUT=$(Xdialog --title="sync_android.sh" --inputbox "${1}" 0x0 $(< "${CACHE}") 2>&1)
    echo "${OUT}" > "${CACHE}"
    echo "${OUT}"
}

LOG_FILE=""
LOG_DIALOG_PID=""
function Log() {
    local command="${1}"
    case "${command}" in
        initialize)
            LOG_FILE=$(mktemp)
            Xdialog --title="sync_android.sh" --no-buttons --tailbox "${LOG_FILE}" 700x400 &
            DIALOG_PID="${!}"
        ;;
        append)
            echo "${2}" >> "${LOG_FILE}"
        ;;
        finalize)
            wait "${DIALOG_PID}"
            rm -f "${LOG_FILE}"
        ;;
    esac
}

function Add() {
    local files=("${@}")

    local NMCLI_NAME="hiogawa-asus"
    local SSHD_IP
    local SSHD_PORT="2222"
    local DATA_ORIG_BASE="${HOME}/code/hiogawa/blob/music/"
    local DATA_DEST_BASE="/storage/9C33-6BBD/Android/data/com.arachnoid.sshelper/synced_data/music/"
    local RSYNC_OPTS=(-r -az --no-perms --no-times --size-only
                      -e "ssh -p ${SSHD_PORT}" -h --progress)
    local SCP_OPTS=(-r -P "${SSHD_PORT}")

    # Check IP connectivity via tethering
    nmcli --terse -f NAME c show --active | grep "${NMCLI_NAME}" 2>&1 >/dev/null
    if [ "${?}" != "0" ]; then
        SSHD_IP=$(AskInputCached 'Input Remote IP Address')
        if [ -z "${SSHD_IP}" ]; then return 1; fi
    else
        SSHD_IP=$(ip ro | head -n 1 | awk '{print $3}')
    fi

    # Going for file transport
    Log initialize
    Log append "== Started at $(date -Isec) =="
    while [ "${#files[@]}" != "0" ]; do
        f="${files[0]}"
        if [ -f "${f}" ]; then
            f_parts="${f##${DATA_ORIG_BASE}}"
            f_orig="${DATA_ORIG_BASE}${f_parts}"
            f_dest=$(printf "%q" "${DATA_DEST_BASE}${f_parts}")
            f_dest_dir="${DATA_DEST_BASE}${f_parts%/*}"
            Log append "Sending \"${f_parts}\""
            out=$( ( ssh -p "${SSHD_PORT}" "${SSHD_IP}" "mkdir -p \"${f_dest_dir}\"" && \
                     rsync "${RSYNC_OPTS[@]}" "${f_orig}" "${SSHD_IP}:${f_dest}" \
                   ) 2>&1 )
            if [ "${?}" != "0" ]; then
                Log append "--- Failed to send \"${f_parts}\" ---"
                Log append "${out}"
                Log append "-------------------------------------"
                AskYesNo "Do you try to send \"${f_parts}\" again?"
                if [ "${?}" != "0" ]; then
                    Log append "Skipped following:"
                    for f in "${files[@]}"; do Log append "${f##${DATA_ORIG_BASE}}"; done
                    break
                else
                    continue
                fi
            fi
        else
            Log "Skipped. \"${f}\" is not a regular file."
        fi
        files=("${files[@]:1}") # shift
    done
    Log append "=== Finished at $(date -Isec) ==="
    Log finalize
}

function Delete() {
    Msg "'delete' command is not yet implemented"
}

function Check() {
    Msg "'check' command is not yet implemented"
}

function Main() {
    # Maybe we could just open these on nautilus
    # nautilus "${HOME}/code/hiogawa/blob/music/" sftp://192.168.43.1:2222/storage/9C33-6BBD/Android/data/com.arachnoid.sshelper/synced_data/music/
    # nautilus sftp://192.168.43.1:2222/storage/9C33-6BBD/Android/data/com.arachnoid.sshelper/synced_data/music/

    local command="${1}"
    shift
    case "${command}" in
        add)
            Add "${@}"
        ;;
        delete)
            Delete "${@}"
        ;;
        check)
            Check "${@}"
        ;;
    esac
}

Main "${@}"
