import requests # https://github.com/requests/requests
from lxml import etree # https://github.com/lxml/lxml
from cssselect import GenericTranslator # https://github.com/scrapy/cssselect

url_base = 'http://www.garlandscience.com'
url_main_path = '/garlandscience_resources/book_resources.jsf?chapter=ALL_CHAPTERS&selectedPage=1&landing=student&resultsPerPage=100&isbn=9780815344926&tabId=ALL_RESOURCES&conversationId=1062924'

def query(document, selector):
    return document.xpath(GenericTranslator().css_to_xpath(selector))

def main():
    resp = requests.get(url_base + url_main_path)
    doc = etree.HTML(resp.text)
    for e in query(doc, '.tab-content#video .resource-info > h4 > a'):
        resp_sub = requests.get(url_base + e.attrib['href'])
        doc_sub = etree.HTML(resp_sub.text)
        print({
            'title': query(doc_sub, '.resource-details > h2')[0].text,
            'script': etree.tostring(query(doc_sub, '.resource-details > .media-description > dd')[0], method="text"),
            'url': query(doc_sub, '.resource-details > .video-view > video')[0].attrib['src']
        })

main()
