from time import sleep
from random import random

total = 100
width = 30
for i in range(total):
    _i = int(i * width / total)
    bar = '[{done}{rest}]'.format(done=('+' * _i), rest=('-' * (width - _i)))
    loss = random()
    print('\r{bar} loss: {loss:.4f}, ({i}/{tot})'.format(
        bar=bar, loss=loss, i=i, total=total)), end='')
    sleep(1)
