#!/bin/bash

# Setup
# $ ln -sf $PWD/youtube_dl_wrapper.sh ~/.local/bin/youtube_dl_wrapper.sh
# Usage
# $ youtube_dl_wrapper.sh audio https://www.youtube.com/watch?v=GnmlTfUFXPU

THIS_FILE=$(readlink -f "${0}")
THIS_DIR="${THIS_FILE%/*}"

# ruby -r json -r i18n -e 'puts I18n::Backend::Transliterator::HashTransliterator::DEFAULT_APPROXIMATIONS.to_json' > transliterate.json
function Parameterize() {
    python3 - "${1}" <<EOF
import re, sys, json
s0 = s1 = sys.argv[1].strip()
h = json.load(open('${THIS_DIR}/transliterate.json'))
def replace(s, m):
    return s[:m.span()[0]] + (h.get(m.group()) or '_') + s[m.span()[1]:]
for m in re.finditer(r'[^0-9a-zA-Z.]', s0):
    s1 = replace(s1, m)
print(s1)
EOF
}

function DownloadAudio() {
    local URL="${1}"
    local BASE_DIR="${HOME}/code/hiogawa/blob/music/__tmp__"
    local TMP_DIR=$(mktemp -d)
    local OPTS=("${URL}" -o "${TMP_DIR}/%(title)s.%(ext)s" -f bestaudio)
    local FILE0 FILE1 FILE2 FILE3 FILE4 FILE5

    # Download file
    FILE0=$(youtube-dl "${OPTS[@]}" --get-filename)
    FILE1="${FILE0##${TMP_DIR}/}"
    youtube-dl "${OPTS[@]}"

    # Rename file
    FILE2=${TMP_DIR}/$(Parameterize "${FILE1}")
    mv "${FILE0}" "${FILE2}"

    # Convert to opus and add metadata
    FILE3="${FILE2%.*}.opus"
    if [ "${FILE2}" != "${FILE3}" ]; then
        ffmpeg -v quiet -y -i "${FILE2}" "${FILE3}"
    fi
    FILE4=$(mktemp -p "${TMP_DIR}" --suffix=.opus)
    ffmpeg -v quiet -y -i "${FILE3}" -codec copy "${FILE4}"

    FILE5="${BASE_DIR}${FILE3##${TMP_DIR}}"
    mv -f "${FILE4}" "${FILE5}"
    rm -r "${TMP_DIR}"
}

function Main() {
    local COMMAND="${1}"
    shift
    case "${COMMAND}" in
        audio)
            for URL in "${@}"; do
                DownloadAudio "${URL}"
            done
        ;;
    esac
}

Main "${@}"
