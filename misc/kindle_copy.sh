#!/bin/bash

# Usage:
# $ bash kindle_copy.sh

# Prefix with timestamp so not to overwrite what's taken
TIMESTAMP=$(date -Is | sed 's/\:/_/g' | sed 's/T/__/' | sed 's/\+/__/')

DIR="/home/hiogawa/Documents/principles_of_neurobiology/${TIMESTAMP}"

mkdir -p ${DIR}

# Focus window named "Default - Wine desktop"
xdotool search --name wine windowactivate --sync windowfocus --sync

for i in $(seq 1 20); do
    FILENAME="${DIR}/$(printf "%04d" $i).png"

    # Take a moment for page to load fully
    sleep 0.5

    # Take a screenshot
    gnome-screenshot -w -f ${FILENAME}

    # Crop unnecessary parts (pixel coordinates are manually determined via gimp)
    convert ${FILENAME} -crop $((926 - 122))x$((1169 - 125))+122+125 ${FILENAME}.cropped.png

    # Focus window named "Default - Wine desktop" and click next page button
    xdotool search --name wine windowactivate --sync windowfocus --sync mousemove --window %1 900 60 click 1
done

# Merge all png file into single pdf: https://stackoverflow.com/questions/11693137/how-do-i-control-pdf-paper-size-with-imagemagick
# $ convert -units pixelsperinch -density 72 -page letter **/*.png.cropped.png all.pdf
