#!/bin/bash

# Usage:
# $ ls -l --sort=t | head -n 20 | bash dialog.sh --title="Latest Change List" --no-cancel

function Main() {
    local XDIALOG_OPTS=("${@}")

    local DIALOG_PID
    local LOG_FILE="$(mktemp)"
    trap "rm ${LOG_FILE}" EXIT

    Xdialog "${XDIALOG_OPTS[@]}" --tailbox "${LOG_FILE}" 700x400 &
    DIALOG_PID="${!}"

    while read LINE; do
        echo "${LINE}" >> "${LOG_FILE}"
    done <&0

    # while read -n 1 LINE; do
    #     printf "${LINE}" >> "${LOG_FILE}"
    # done <&0

    wait "${DIALOG_PID}"
}

Main "${@}" <&0
