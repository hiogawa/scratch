#pragma once

class MyFact {
public:
  MyFact(int);
  int Run();

private:
  int n_;
};
