Exploring clang internal, cf. diary.md (2017-09-16).


# Example

```
$ clang++ test0.cc
$ ./a.out
fact(10) = 3628800
$ clang++ test1.cc
$ ./a.out
Fact::Run() = 3628800
```


# Clang Driver

```
$ clang++ -### -x c++ test0.cc
clang version 5.0.0 (tags/RELEASE_500/final)
Target: x86_64-unknown-linux-gnu
Thread model: posix
InstalledDir: /usr/bin
 "/usr/bin/clang-5.0" "-cc1" "-triple" "x86_64-unknown-linux-gnu" "-emit-obj" "-mrelax-all" "-disable-free" "-disable-llvm-verifier" "-discard-value-names" "-main-file-name" "test0.cc" "-mrelocation-model" "pic" "-pic-level" "2" "-pic-is-pie" "-mthread-model" "posix" "-mdisable-fp-elim" "-fmath-errno" "-masm-verbose" "-mconstructor-aliases" "-munwind-tables" "-fuse-init-array" "-target-cpu" "x86-64" "-dwarf-column-info" "-debugger-tuning=gdb" "-resource-dir" "/usr/lib/clang/5.0.0" "-internal-isystem" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../include/c++/7.2.0" "-internal-isystem" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../include/c++/7.2.0/x86_64-pc-linux-gnu" "-internal-isystem" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../include/c++/7.2.0/backward" "-internal-isystem" "/usr/local/include" "-internal-isystem" "/usr/lib/clang/5.0.0/include" "-internal-externc-isystem" "/include" "-internal-externc-isystem" "/usr/include" "-fdeprecated-macro" "-fdebug-compilation-dir" "/home/hiogawa/code/hiogawa/scratch/try-clang-llvm" "-ferror-limit" "19" "-fmessage-length" "150" "-stack-protector" "2" "-fobjc-runtime=gcc" "-fcxx-exceptions" "-fexceptions" "-fdiagnostics-show-option" "-fcolor-diagnostics" "-o" "/tmp/test-4cce02.o" "-x" "c++" "test0.cc"
  "/usr/bin/ld" "-pie" "--eh-frame-hdr" "-m" "elf_x86_64" "-dynamic-linker" "/lib64/ld-linux-x86-64.so.2" "-o" "a.out" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../lib64/Scrt1.o" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../lib64/crti.o" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/crtbeginS.o" "-L/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0" "-L/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../lib64" "-L/usr/bin/../lib64" "-L/lib/../lib64" "-L/usr/lib/../lib64" "-L/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../.." "-L/usr/bin/../lib" "-L/lib" "-L/usr/lib" "/tmp/test-4cce02.o" "-lstdc++" "-lm" "-lgcc_s" "-lgcc" "-lc" "-lgcc_s" "-lgcc" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/crtendS.o" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../lib64/crtn.o"


$ clang++ -ccc-print-phases -x c++ test0.cc
0: input, "test0.cc", c++
1: preprocessor, {0}, c++-cpp-output
2: compiler, {1}, ir
3: backend, {2}, assembler
4: assembler, {3}, object
5: linker, {4}, image


$ clang++ -ccc-print-bindings -x c++ test0.cc
# "x86_64-unknown-linux-gnu" - "clang", inputs: ["test0.cc"], output: "/tmp/test-dfd32d.o"
# "x86_64-unknown-linux-gnu" - "GNU::Linker", inputs: ["/tmp/test-dfd32d.o"], output: "a.out"


$ clang++ -ccc-print-phases -S -x c++ test0.cc
0: input, "test0.cc", c++
1: preprocessor, {0}, c++-cpp-output
2: compiler, {1}, ir
3: backend, {2}, assembler


$ clang++ -ccc-print-phases -S -emit-llvm -x c++ test0.cc
0: input, "test0.cc", c++
1: preprocessor, {0}, c++-cpp-output
2: compiler, {1}, ir
3: backend, {2}, ir


(we could choose not using llvm-based assembler)
$ clang++ -ccc-print-bindings -no-integrated-as -x c++ test0.cc
# "x86_64-unknown-linux-gnu" - "clang", inputs: ["test0.cc"], output: "/tmp/test0-db5648.s"
# "x86_64-unknown-linux-gnu" - "GNU::Assembler", inputs: ["/tmp/test0-db5648.s"], output: "/tmp/test0-5cd05f.o"
# "x86_64-unknown-linux-gnu" - "GNU::Linker", inputs: ["/tmp/test0-5cd05f.o"], output: "a.out"

$ clang++ -### -no-integrated-as -x c++ test0.cc
clang version 5.0.0 (tags/RELEASE_500/final)
Target: x86_64-unknown-linux-gnu
Thread model: posix
InstalledDir: /usr/bin
 "/usr/bin/clang-5.0" "-cc1" "-triple" "x86_64-unknown-linux-gnu" "-S" "-disable-free" "-disable-llvm-verifier" "-discard-value-names" "-main-file-name" "test0.cc" "-mrelocation-model" "pic" "-pic-level" "2" "-pic-is-pie" "-mthread-model" "posix" "-mdisable-fp-elim" "-fmath-errno" "-masm-verbose" "-no-integrated-as" "-mconstructor-aliases" "-munwind-tables" "-fuse-init-array" "-target-cpu" "x86-64" "-dwarf-column-info" "-debugger-tuning=gdb" "-resource-dir" "/usr/lib/clang/5.0.0" "-internal-isystem" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../include/c++/7.2.0" "-internal-isystem" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../include/c++/7.2.0/x86_64-pc-linux-gnu" "-internal-isystem" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../include/c++/7.2.0/backward" "-internal-isystem" "/usr/local/include" "-internal-isystem" "/usr/lib/clang/5.0.0/include" "-internal-externc-isystem" "/include" "-internal-externc-isystem" "/usr/include" "-fdeprecated-macro" "-fno-dwarf-directory-asm" "-fdebug-compilation-dir" "/home/hiogawa/code/hiogawa/scratch/try-clang-llvm" "-ferror-limit" "19" "-fmessage-length" "150" "-stack-protector" "2" "-fobjc-runtime=gcc" "-fcxx-exceptions" "-fexceptions" "-fdiagnostics-show-option" "-fcolor-diagnostics" "-o" "/tmp/test0-3d5459.s" "-x" "c++" "test0.cc"
 "/usr/bin/as" "--64" "-o" "/tmp/test0-546509.o" "/tmp/test0-3d5459.s"
 "/usr/bin/ld" "-pie" "--eh-frame-hdr" "-m" "elf_x86_64" "-dynamic-linker" "/lib64/ld-linux-x86-64.so.2" "-o" "a.out" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../lib64/Scrt1.o" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../lib64/crti.o" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/crtbeginS.o" "-L/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0" "-L/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../lib64" "-L/usr/bin/../lib64" "-L/lib/../lib64" "-L/usr/lib/../lib64" "-L/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../.." "-L/usr/bin/../lib" "-L/lib" "-L/usr/lib" "/tmp/test0-546509.o" "-lstdc++" "-lm" "-lgcc_s" "-lgcc" "-lc" "-lgcc_s" "-lgcc" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/crtendS.o" "/usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/7.2.0/../../../../lib64/crtn.o"
```


# Clang Compiler

- AST Dump

```
$ clang-check -ast-dump -ast-dump-filter=my_test test1.cc --
Dumping my_test:
NamespaceDecl 0x562464e63968 </home/hiogawa/code/hiogawa/scratch/try-clang-llvm/test1.cc:4:1, line:24:1> line:4:11 my_test
|-CXXRecordDecl 0x562464e639d0 </home/hiogawa/code/hiogawa/scratch/try-clang-llvm/test1.h:3:1, line:10:1> line:3:7 referenced class MyFact definition
| |-CXXRecordDecl 0x562464e63af8 <col:1, col:7> col:7 implicit referenced class MyFact
| |-AccessSpecDecl 0x562464e63b90 <line:4:1, col:7> col:1 public
| |-CXXConstructorDecl 0x562464e63c78 <line:5:3, col:13> col:3 used MyFact 'void (int)'
| | `-ParmVarDecl 0x562464e63bd0 <col:10> col:13 'int'
| |-CXXMethodDecl 0x562464e63d50 <line:6:3, col:11> col:7 used Run 'int (void)'
| |-AccessSpecDecl 0x562464e63de8 <line:8:1, col:8> col:1 private
| |-FieldDecl 0x562464e63e28 <line:9:3, col:7> col:7 referenced n_ 'int'
| `-CXXConstructorDecl 0x562464e63f98 <line:3:7> col:7 implicit MyFact 'void (const class my_test::MyFact &)' inline default trivial noexcept-unevaluated 0x562464e63f98
|   `-ParmVarDecl 0x562464e640c0 <col:7> col:7 'const class my_test::MyFact &'
|-CXXConstructorDecl 0x562464e64128 parent 0x562464e639d0 prev 0x562464e63c78 </home/hiogawa/code/hiogawa/scratch/try-clang-llvm/test1.cc:8:1, col:32> col:9 used MyFact 'void (int)'
| |-ParmVarDecl 0x562464e63ee0 <col:16, col:20> col:20 used n 'int'
| |-CXXCtorInitializer Field 0x562464e63e28 'n_' 'int'
| | `-ImplicitCastExpr 0x562464e64268 <col:28> 'int' <LValueToRValue>
| |   `-DeclRefExpr 0x562464e64210 <col:28> 'int' lvalue ParmVar 0x562464e63ee0 'n' 'int'
| `-CompoundStmt 0x562464e642b0 <col:31, col:32>
|-CXXMethodDecl 0x562464e642f8 parent 0x562464e639d0 prev 0x562464e63d50 <line:10:1, line:16:1> line:10:13 used Run 'int (void)'
| `-CompoundStmt 0x562464e647c8 <col:19, line:16:1>
|   |-DeclStmt 0x562464e64470 <line:11:3, col:17>
|   | `-VarDecl 0x562464e643f0 <col:3, col:16> col:7 used result 'int' cinit
|   |   `-IntegerLiteral 0x562464e64450 <col:16> 'int' 1
|   |-ForStmt 0x562464e64738 <line:12:3, line:14:3>
|   | |-DeclStmt 0x562464e64520 <line:12:8, col:17>
|   | | `-VarDecl 0x562464e644a0 <col:8, col:16> col:12 used i 'int' cinit
|   | |   `-IntegerLiteral 0x562464e64500 <col:16> 'int' 1
|   | |-<<<NULL>>>
|   | |-BinaryOperator 0x562464e64608 <col:19, col:24> '_Bool' '<='
|   | | |-ImplicitCastExpr 0x562464e645d8 <col:19> 'int' <LValueToRValue>
|   | | | `-DeclRefExpr 0x562464e64538 <col:19> 'int' lvalue Var 0x562464e644a0 'i' 'int'
|   | | `-ImplicitCastExpr 0x562464e645f0 <col:24> 'int' <LValueToRValue>
|   | |   `-MemberExpr 0x562464e645a0 <col:24> 'int' lvalue ->n_ 0x562464e63e28
|   | |     `-CXXThisExpr 0x562464e64588 <col:24> 'class my_test::MyFact *' this
|   | |-UnaryOperator 0x562464e64658 <col:28, col:29> 'int' postfix '++'
|   | | `-DeclRefExpr 0x562464e64630 <col:28> 'int' lvalue Var 0x562464e644a0 'i' 'int'
|   | `-CompoundStmt 0x562464e64718 <col:33, line:14:3>
|   |   `-CompoundAssignOperator 0x562464e646e0 <line:13:5, col:15> 'int' lvalue '*=' ComputeLHSTy='int' ComputeResultTy='int'
|   |     |-DeclRefExpr 0x562464e64678 <col:5> 'int' lvalue Var 0x562464e643f0 'result' 'int'
|   |     `-ImplicitCastExpr 0x562464e646c8 <col:15> 'int' <LValueToRValue>
|   |       `-DeclRefExpr 0x562464e646a0 <col:15> 'int' lvalue Var 0x562464e644a0 'i' 'int'
|   `-ReturnStmt 0x562464e647b0 <line:15:3, col:10>
|     `-ImplicitCastExpr 0x562464e64798 <col:10> 'int' <LValueToRValue>
|       `-DeclRefExpr 0x562464e64770 <col:10> 'int' lvalue Var 0x562464e643f0 'result' 'int'
`-FunctionDecl 0x562464e64a08 <line:18:1, line:22:1> line:18:5 used main 'int (int, char **)'
  |-ParmVarDecl 0x562464e64810 <col:10, col:14> col:14 argc 'int'
  |-ParmVarDecl 0x562464e648f0 <col:20, col:31> col:26 argv 'char **':'char **'
  `-CompoundStmt 0x562464e657f0 <col:34, line:22:1>
    |-DeclStmt 0x562464e64ba8 <line:19:3, col:18>
    | `-VarDecl 0x562464e64ac0 <col:3, col:17> col:10 used fact 'class my_test::MyFact' callinit
    |   `-CXXConstructExpr 0x562464e64b70 <col:10, col:17> 'class my_test::MyFact' 'void (int)'
    |     `-IntegerLiteral 0x562464e64b20 <col:15> 'int' 10
    |-CXXOperatorCallExpr 0x562464e65770 <line:20:3, col:50> 'basic_ostream<char, struct std::char_traits<char> >':'class std::basic_ostream<char>' lvalue
    | |-ImplicitCastExpr 0x562464e65758 <col:47> 'basic_ostream<char, struct std::char_traits<char> > &(*)(basic_ostream<char, struct std::char_traits<char> > &, const char *)' <FunctionToPointerDecay>
    | | `-DeclRefExpr 0x562464e65730 <col:47> 'basic_ostream<char, struct std::char_traits<char> > &(basic_ostream<char, struct std::char_traits<char> > &, const char *)' lvalue Function 0x562464ded250 'operator<<' 'basic_ostream<char, struct std::char_traits<char> > &(basic_ostream<char, struct std::char_traits<char> > &, const char *)'
    | |-CXXOperatorCallExpr 0x562464e654d0 <col:3, col:45> 'std::basic_ostream<char, struct std::char_traits<char> >::__ostream_type':'class std::basic_ostream<char>' lvalue
    | | |-ImplicitCastExpr 0x562464e654b8 <col:33> 'std::basic_ostream<char, struct std::char_traits<char> >::__ostream_type &(*)(int)' <FunctionToPointerDecay>
    | | | `-DeclRefExpr 0x562464e65438 <col:33> 'std::basic_ostream<char, struct std::char_traits<char> >::__ostream_type &(int)' lvalue CXXMethod 0x562464de5c30 'operator<<' 'std::basic_ostream<char, struct std::char_traits<char> >::__ostream_type &(int)'
    | | |-CXXOperatorCallExpr 0x562464e64f50 <col:3, col:16> 'basic_ostream<char, struct std::char_traits<char> >':'class std::basic_ostream<char>' lvalue
    | | | |-ImplicitCastExpr 0x562464e64f38 <col:13> 'basic_ostream<char, struct std::char_traits<char> > &(*)(basic_ostream<char, struct std::char_traits<char> > &, const char *)' <FunctionToPointerDecay>
    | | | | `-DeclRefExpr 0x562464e64eb0 <col:13> 'basic_ostream<char, struct std::char_traits<char> > &(basic_ostream<char, struct std::char_traits<char> > &, const char *)' lvalue Function 0x562464ded250 'operator<<' 'basic_ostream<char, struct std::char_traits<char> > &(basic_ostream<char, struct std::char_traits<char> > &, const char *)'
    | | | |-DeclRefExpr 0x562464e64bf8 <col:3, col:8> 'std::ostream':'class std::basic_ostream<char>' lvalue Var 0x562464e63388 'cout' 'std::ostream':'class std::basic_ostream<char>'
    | | | `-ImplicitCastExpr 0x562464e64e98 <col:16> 'const char *' <ArrayToPointerDecay>
    | | |   `-StringLiteral 0x562464e64c30 <col:16> 'const char [15]' lvalue "Fact::Run() = "
    | | `-CXXMemberCallExpr 0x562464e64ff8 <col:36, col:45> 'int'
    | |   `-MemberExpr 0x562464e64fc0 <col:36, col:41> '<bound member function type>' .Run 0x562464e642f8
    | |     `-DeclRefExpr 0x562464e64f98 <col:36> 'class my_test::MyFact' lvalue Var 0x562464e64ac0 'fact' 'class my_test::MyFact'
    | `-ImplicitCastExpr 0x562464e65718 <col:50> 'const char *' <ArrayToPointerDecay>
    |   `-StringLiteral 0x562464e65518 <col:50> 'const char [2]' lvalue "\n"
    `-ReturnStmt 0x562464e657d8 <line:21:3, col:10>
      `-IntegerLiteral 0x562464e657b8 <col:10> 'int' 0
```

- Emit LLVM IR (-S -emit-llvm)

```
$ clang++ -S -emit-llvm -x c++ test0.cc # see test0.ll
```

- Emit x86 assembly (-S)

```
$ llc -mtriple=x86_64-unknown-linux-gnu test0.ll # see test0.s

(or from clang)
$ clang++ -S -x c++ test0.cc
```

- integrated assembler (llvm ir -> object)

```
$ llc test2.ll -filetype=obj -o test2.o
```

- SelectionDAG ("-view-.." is only available on debug build)

```
$ llc test2.ll -filetype=obj --debug-pass=Structure # see static PassDebugging in LegacyPassManager.cpp
$ llc test2.ll -filetype=obj -print-before=expand-isel-pseudos -print-isel-input # print before/after of selectionDAG (LLVM IR -> MachineFunctions)
$ llc -view-dag-combine1-dags test2.ll
$ llc -view-isel-dags test2.ll
```


# Linker

```
TODO
```
