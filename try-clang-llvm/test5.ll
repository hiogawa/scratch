; ModuleID = 'test5.cc'
source_filename = "test5.cc"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%class.X = type { i32 }

$_ZN1XC2Ev = comdat any

$_ZN1XD2Ev = comdat any

$__clang_call_terminate = comdat any

; Function Attrs: noinline optnone sspstrong uwtable
define void @_Z1fv() #0 {
  call void @__cxa_rethrow() #5
  unreachable
                                                  ; No predecessors!
  ret void
}

declare void @__cxa_rethrow()

; Function Attrs: noinline optnone sspstrong uwtable
define i32 @_Z1gv() #0 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %1 = alloca i32, align 4
  %2 = alloca %class.X, align 4
  %3 = alloca i8*
  %4 = alloca i32
  %5 = alloca i32
  call void @_ZN1XC2Ev(%class.X* %2)
  invoke void @_Z1fv()
          to label %6 unwind label %7

; <label>:6:                                      ; preds = %0
  store i32 0, i32* %1, align 4
  store i32 1, i32* %5, align 4
  br label %20

; <label>:7:                                      ; preds = %0
  %8 = landingpad { i8*, i32 }
          catch i8* null
  %9 = extractvalue { i8*, i32 } %8, 0
  store i8* %9, i8** %3, align 8
  %10 = extractvalue { i8*, i32 } %8, 1
  store i32 %10, i32* %4, align 4
  br label %11

; <label>:11:                                     ; preds = %7
  %12 = load i8*, i8** %3, align 8
  %13 = call i8* @__cxa_begin_catch(i8* %12) #6
  store i32 1, i32* %1, align 4
  store i32 1, i32* %5, align 4
  invoke void @__cxa_end_catch()
          to label %14 unwind label %15

; <label>:14:                                     ; preds = %11
  br label %20

; <label>:15:                                     ; preds = %11
  %16 = landingpad { i8*, i32 }
          cleanup
  %17 = extractvalue { i8*, i32 } %16, 0
  store i8* %17, i8** %3, align 8
  %18 = extractvalue { i8*, i32 } %16, 1
  store i32 %18, i32* %4, align 4
  invoke void @_ZN1XD2Ev(%class.X* %2)
          to label %22 unwind label %28
                                                  ; No predecessors!
  call void @llvm.trap()
  unreachable

; <label>:20:                                     ; preds = %14, %6
  call void @_ZN1XD2Ev(%class.X* %2)
  %21 = load i32, i32* %1, align 4
  ret i32 %21

; <label>:22:                                     ; preds = %15
  br label %23

; <label>:23:                                     ; preds = %22
  %24 = load i8*, i8** %3, align 8
  %25 = load i32, i32* %4, align 4
  %26 = insertvalue { i8*, i32 } undef, i8* %24, 0
  %27 = insertvalue { i8*, i32 } %26, i32 %25, 1
  resume { i8*, i32 } %27

; <label>:28:                                     ; preds = %15
  %29 = landingpad { i8*, i32 }
          catch i8* null
  %30 = extractvalue { i8*, i32 } %29, 0
  call void @__clang_call_terminate(i8* %30) #2
  unreachable
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define linkonce_odr void @_ZN1XC2Ev(%class.X*) unnamed_addr #1 comdat align 2 {
  %2 = alloca %class.X*, align 8
  store %class.X* %0, %class.X** %2, align 8
  %3 = load %class.X*, %class.X** %2, align 8
  ret void
}

declare i32 @__gxx_personality_v0(...)

declare i8* @__cxa_begin_catch(i8*)

declare void @__cxa_end_catch()

; Function Attrs: noreturn nounwind
declare void @llvm.trap() #2

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define linkonce_odr void @_ZN1XD2Ev(%class.X*) unnamed_addr #1 comdat align 2 {
  %2 = alloca %class.X*, align 8
  store %class.X* %0, %class.X** %2, align 8
  %3 = load %class.X*, %class.X** %2, align 8
  ret void
}

; Function Attrs: noinline noreturn nounwind
define linkonce_odr hidden void @__clang_call_terminate(i8*) #3 comdat {
  %2 = call i8* @__cxa_begin_catch(i8* %0) #6
  call void @_ZSt9terminatev() #2
  unreachable
}

declare void @_ZSt9terminatev()

; Function Attrs: noinline norecurse optnone sspstrong uwtable
define i32 @main(i32, i8**) #4 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i8**, align 8
  store i32 0, i32* %3, align 4
  store i32 %0, i32* %4, align 4
  store i8** %1, i8*** %5, align 8
  %6 = call i32 @_Z1gv()
  ret i32 %6
}

attributes #0 = { noinline optnone sspstrong uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone sspstrong uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn nounwind }
attributes #3 = { noinline noreturn nounwind }
attributes #4 = { noinline norecurse optnone sspstrong uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0, !1, !2}
!llvm.ident = !{!3}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{i32 7, !"PIE Level", i32 2}
!3 = !{!"clang version 5.0.0 (tags/RELEASE_500/final)"}
