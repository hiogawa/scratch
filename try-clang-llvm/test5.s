	.text
	.file	"test5.cc"
	.globl	_Z1fv                   # -- Begin function _Z1fv
	.p2align	4, 0x90
	.type	_Z1fv,@function
_Z1fv:                                  # @_Z1fv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi2:
	.cfi_def_cfa_register %rbp
	callq	__cxa_rethrow
.Lfunc_end0:
	.size	_Z1fv, .Lfunc_end0-_Z1fv
	.cfi_endproc
                                        # -- End function
	.globl	_Z1gv                   # -- Begin function _Z1gv
	.p2align	4, 0x90
	.type	_Z1gv,@function
_Z1gv:                                  # @_Z1gv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi5:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	leaq	-16(%rbp), %rdi
	callq	_ZN1XC2Ev
.Ltmp0:
	callq	_Z1fv
.Ltmp1:
	jmp	.LBB1_1
.LBB1_1:
	movl	$0, -20(%rbp)
	movl	$1, -36(%rbp)
	jmp	.LBB1_6
.LBB1_2:
.Ltmp2:
	movq	%rax, -32(%rbp)
	movl	%edx, -40(%rbp)
# BB#3:
	movq	-32(%rbp), %rdi
	callq	__cxa_begin_catch
	movl	$1, -20(%rbp)
	movl	$1, -36(%rbp)
.Ltmp3:
	callq	__cxa_end_catch
.Ltmp4:
	jmp	.LBB1_4
.LBB1_4:
	jmp	.LBB1_6
.LBB1_5:
.Ltmp5:
	movq	%rax, -32(%rbp)
	movl	%edx, -40(%rbp)
.Ltmp6:
	leaq	-16(%rbp), %rdi
	callq	_ZN1XD2Ev
.Ltmp7:
	jmp	.LBB1_8
.LBB1_6:
	leaq	-16(%rbp), %rdi
	callq	_ZN1XD2Ev
	movl	-20(%rbp), %eax
	movq	%fs:40, %rcx
	movq	-8(%rbp), %rdx
	cmpq	%rdx, %rcx
	jne	.LBB1_11
# BB#7:                                 # %SP_return
	addq	$48, %rsp
	popq	%rbp
	retq
.LBB1_8:
	jmp	.LBB1_9
.LBB1_9:
	movq	-32(%rbp), %rdi
	callq	_Unwind_Resume
.LBB1_10:
.Ltmp8:
                                        # kill: %RDX<kill>
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_11:                               # %CallStackCheckFailBlk
	callq	__stack_chk_fail
.Lfunc_end1:
	.size	_Z1gv, .Lfunc_end1-_Z1gv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	1                       #   On action: 1
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end1-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2
                                        # -- End function
	.section	.text._ZN1XC2Ev,"axG",@progbits,_ZN1XC2Ev,comdat
	.weak	_ZN1XC2Ev               # -- Begin function _ZN1XC2Ev
	.p2align	4, 0x90
	.type	_ZN1XC2Ev,@function
_ZN1XC2Ev:                              # @_ZN1XC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi8:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN1XC2Ev, .Lfunc_end2-_ZN1XC2Ev
	.cfi_endproc
                                        # -- End function
	.section	.text._ZN1XD2Ev,"axG",@progbits,_ZN1XD2Ev,comdat
	.weak	_ZN1XD2Ev               # -- Begin function _ZN1XD2Ev
	.p2align	4, 0x90
	.type	_ZN1XD2Ev,@function
_ZN1XD2Ev:                              # @_ZN1XD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi11:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN1XD2Ev, .Lfunc_end3-_ZN1XD2Ev
	.cfi_endproc
                                        # -- End function
	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate  # -- Begin function __clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate
                                        # -- End function
	.text
	.globl	main                    # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi14:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	$0, -8(%rbp)
	movl	%edi, -4(%rbp)
	movq	%rsi, -16(%rbp)
	callq	_Z1gv
	addq	$16, %rsp
	popq	%rbp
	retq
.Lfunc_end5:
	.size	main, .Lfunc_end5-main
	.cfi_endproc
                                        # -- End function

	.ident	"clang version 5.0.0 (tags/RELEASE_500/final)"
	.section	".note.GNU-stack","",@progbits
