// $ clang++ -S -emit-llvm test3.cc -o test3.ll
// $ llc test3.ll -o test3.s
// $ llc test3.ll -filetype=obj -o test3.o

int fact_rec(int n) {
  if (n <= 1) {
    return 1;
  } else {
    return n * fact_rec(n - 1);
  }
}
