#include <iostream>

// convenient for ast-dump, ast-print
namespace my_test {

#include "test1.h"

MyFact::MyFact(int n) : n_(n) {}

int MyFact::Run() {
  int result = 1;
  for (int i = 1; i <= n_; i++) {
    result *= i;
  }
  return result;
}

int main(int argc, char* argv[]) {
  MyFact fact(10);
  std::cout << "Fact::Run() = " << fact.Run() << "\n";
  return 0;
}

} // namespace my_test


int main(int argc, char* argv[]) {
  return my_test::main(argc, argv);
}
