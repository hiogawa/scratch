// $ clang++ -S -emit-llvm test2.cc -o test2.ll
// $ llc test2.ll -o test2.s
// $ llc test2.ll -filtype=obj -o test2.o

int fact_loop(int n) {
  int result = 1;
  for (int i = 1; i <= n; i++) {
    result *= i;
  }
  return result;
}
