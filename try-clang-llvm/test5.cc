// $ clang++ -S -emit-llvm test5.cc -o test5.ll
// $ llc test5.ll -o test5.s
// $ llc test5.ll -o test5.o -filetype=obj
// $ readelf -aW test5.o

class X {
public:
  X() {}
  ~X() {}
private:
  int n;
};

void f() {
  throw;
}

int g() {
  X x;
  try {
    f();
    return 0;
  } catch (...) {
    return 1;
  }
}

int main(int argc, char* argv[]) {
  return g();
}
