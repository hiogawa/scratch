#include <iostream>

// convenient for ast-dump, ast-print
namespace my_test {

int fact(int n) {
  if (n <= 1) {
    return 1;
  } else {
    return n * fact(n - 1);
  }
}

int main(int argc, char* argv[]) {
  std::cout << "fact(10) = " << fact(10) << "\n";
  return 0;
}

} // my_test


int main(int argc, char* argv[]) {
  return my_test::main(argc, argv);
}
