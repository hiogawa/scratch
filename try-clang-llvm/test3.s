	.text
	.file	"test3.cc"
	.globl	_Z8fact_reci            # -- Begin function _Z8fact_reci
	.p2align	4, 0x90
	.type	_Z8fact_reci,@function
_Z8fact_reci:                           # @_Z8fact_reci
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi2:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
.Lcfi3:
	.cfi_offset %rbx, -24
	movl	%edi, -12(%rbp)
	cmpl	$1, -12(%rbp)
	jg	.LBB0_2
# BB#1:
	movl	$1, -16(%rbp)
	jmp	.LBB0_3
.LBB0_2:
	movl	-12(%rbp), %ebx
	movl	-12(%rbp), %edi
	subl	$1, %edi
	callq	_Z8fact_reci
	imull	%ebx, %eax
	movl	%eax, -16(%rbp)
.LBB0_3:
	movl	-16(%rbp), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_Z8fact_reci, .Lfunc_end0-_Z8fact_reci
	.cfi_endproc
                                        # -- End function

	.ident	"clang version 5.0.0 (tags/RELEASE_500/final)"
	.section	".note.GNU-stack","",@progbits
