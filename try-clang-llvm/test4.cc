// $ clang++ -S -emit-llvm test4.cc -o test4.ll
// $ llc test4.ll -o test4.s

int m = 0;

int fact_rec(int n) {
  if (n <= 1) {
    return 1;
  } else {
    return n * fact_rec(n - 1);
  }
}
