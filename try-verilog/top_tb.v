module top_tb ();

reg clk;
wire led1;
wire led2;
wire led3;
wire led4;
wire led5;
wire led6;
wire led7;
wire led8;

top top (
    .hwclk(clk),
    .led1(led1),
    .led2(led2),
    .led3(led3),
    .led4(led4),
    .led5(led5),
    .led6(led6),
    .led7(led7),
    .led8(led8)
);

initial begin
    $dumpfile("top_tb.lxt");
    $dumpvars(0, top);
    clk = 1'b0;
    repeat(1000) begin
        #1 clk = ~clk;
        #1 clk = ~clk;
    end
end

endmodule
