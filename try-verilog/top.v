module top (
    input hwclk,
    output led1,
    output led2,
    output led3,
    output led4,
    output led5,
    output led6,
    output led7,
    output led8
);

reg [7:0] count = 8'b0;

assign led1 = count[0] ^ count[1];
assign led2 = count[1] ^ count[2];
assign led3 = count[2] ^ count[3];
assign led4 = count[3] ^ count[4];
assign led5 = count[4] ^ count[5];
assign led6 = count[5] ^ count[6];
assign led7 = count[6] ^ count[7];
assign led8 = count[7];

always @(posedge hwclk)
    count <= count + 1;

endmodule
