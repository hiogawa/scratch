Example from [Open-Source Tools for FPGA Development (youtube)](https://www.youtube.com/watch?v=MI18Wk4gxA4) ([slide](http://events.linuxfoundation.org/sites/events/files/slides/lcj-2016.pdf)).

```
$ iverilog -o top.vvp top_tb.v top.v
$ vvp top.vvp -lxt2
$ gtkwave top_tb.lxt
```
