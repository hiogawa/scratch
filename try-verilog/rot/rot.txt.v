// Reading file 'rot.txt'..

module chip (input clk, output D5, output D2, output D3, output D1, output D4);

wire n1;
// (0, 0, 'glb_netwk_2')
// (5, 10, 'lutff_global/s_r')
// (5, 11, 'lutff_global/s_r')
// (6, 8, 'lutff_global/s_r')
// (6, 12, 'lutff_global/s_r')
// (7, 8, 'lutff_global/s_r')
// (7, 9, 'lutff_global/s_r')
// (7, 9, 'sp12_h_r_0')
// (7, 10, 'lutff_global/s_r')
// (7, 11, 'lutff_global/s_r')
// (8, 8, 'neigh_op_tnr_6')
// (8, 9, 'neigh_op_rgt_6')
// (8, 9, 'sp12_h_r_3')
// (8, 10, 'neigh_op_bnr_6')
// (9, 8, 'neigh_op_top_6')
// (9, 9, 'lutff_6/out')
// (9, 9, 'sp12_h_r_4')
// (9, 10, 'neigh_op_bot_6')
// (10, 8, 'neigh_op_tnl_6')
// (10, 9, 'neigh_op_lft_6')
// (10, 9, 'sp12_h_r_7')
// (10, 10, 'neigh_op_bnl_6')
// (11, 9, 'sp12_h_r_8')
// (12, 9, 'sp12_h_r_11')
// (12, 12, 'lutff_global/s_r')
// (13, 9, 'fabout')
// (13, 9, 'local_g0_3')
// (13, 9, 'span12_horz_11')

wire clk;
// (0, 0, 'glb_netwk_6')
// (0, 4, 'span4_vert_t_14')
// (0, 5, 'span4_vert_b_14')
// (0, 6, 'span4_vert_b_10')
// (0, 7, 'span4_vert_b_6')
// (0, 8, 'fabout')
// (0, 8, 'io_1/D_IN_0')
// (0, 8, 'io_1/PAD')
// (0, 8, 'local_g1_2')
// (0, 8, 'span4_vert_b_2')
// (1, 7, 'neigh_op_tnl_2')
// (1, 7, 'neigh_op_tnl_6')
// (1, 8, 'neigh_op_lft_2')
// (1, 8, 'neigh_op_lft_6')
// (1, 9, 'neigh_op_bnl_2')
// (1, 9, 'neigh_op_bnl_6')
// (5, 10, 'lutff_global/clk')
// (5, 11, 'lutff_global/clk')
// (6, 8, 'lutff_global/clk')
// (6, 12, 'lutff_global/clk')
// (7, 8, 'lutff_global/clk')
// (7, 9, 'lutff_global/clk')
// (7, 10, 'lutff_global/clk')
// (7, 11, 'lutff_global/clk')
// (8, 9, 'lutff_global/clk')
// (12, 12, 'lutff_global/clk')

reg n3 = 0;
// (0, 10, 'span12_horz_7')
// (1, 10, 'sp12_h_r_8')
// (2, 10, 'sp12_h_r_11')
// (3, 10, 'sp12_h_r_12')
// (4, 9, 'neigh_op_tnr_4')
// (4, 10, 'neigh_op_rgt_4')
// (4, 10, 'sp12_h_r_15')
// (4, 11, 'neigh_op_bnr_4')
// (5, 9, 'neigh_op_top_4')
// (5, 10, 'lutff_4/out')
// (5, 10, 'sp12_h_r_16')
// (5, 11, 'neigh_op_bot_4')
// (6, 9, 'neigh_op_tnl_4')
// (6, 10, 'local_g0_4')
// (6, 10, 'lutff_6/in_2')
// (6, 10, 'neigh_op_lft_4')
// (6, 10, 'sp12_h_r_19')
// (6, 11, 'neigh_op_bnl_4')
// (7, 10, 'local_g1_4')
// (7, 10, 'lutff_1/in_2')
// (7, 10, 'sp12_h_r_20')
// (8, 10, 'sp12_h_r_23')
// (9, 10, 'sp12_h_l_23')

wire n4;
// (1, 10, 'sp12_h_r_0')
// (2, 10, 'sp12_h_r_3')
// (3, 10, 'sp12_h_r_4')
// (4, 10, 'sp12_h_r_7')
// (4, 11, 'sp4_h_r_8')
// (5, 10, 'local_g0_0')
// (5, 10, 'local_g1_0')
// (5, 10, 'lutff_1/in_0')
// (5, 10, 'lutff_2/in_0')
// (5, 10, 'lutff_3/in_0')
// (5, 10, 'lutff_4/in_0')
// (5, 10, 'sp12_h_r_8')
// (5, 11, 'local_g0_5')
// (5, 11, 'lutff_3/in_0')
// (5, 11, 'lutff_7/in_0')
// (5, 11, 'sp4_h_r_21')
// (6, 7, 'sp4_r_v_b_38')
// (6, 7, 'sp4_r_v_b_41')
// (6, 8, 'local_g1_3')
// (6, 8, 'local_g1_4')
// (6, 8, 'lutff_3/in_0')
// (6, 8, 'lutff_4/in_0')
// (6, 8, 'lutff_5/in_0')
// (6, 8, 'lutff_7/in_0')
// (6, 8, 'sp4_r_v_b_27')
// (6, 8, 'sp4_r_v_b_28')
// (6, 9, 'neigh_op_tnr_2')
// (6, 9, 'sp4_r_v_b_14')
// (6, 9, 'sp4_r_v_b_17')
// (6, 10, 'neigh_op_rgt_2')
// (6, 10, 'sp12_h_r_11')
// (6, 10, 'sp4_r_v_b_3')
// (6, 10, 'sp4_r_v_b_4')
// (6, 11, 'neigh_op_bnr_2')
// (6, 11, 'sp4_h_r_32')
// (6, 11, 'sp4_r_v_b_42')
// (6, 12, 'local_g0_7')
// (6, 12, 'lutff_1/in_0')
// (6, 12, 'lutff_3/in_0')
// (6, 12, 'lutff_5/in_0')
// (6, 12, 'lutff_7/in_0')
// (6, 12, 'sp4_r_v_b_31')
// (6, 13, 'sp4_r_v_b_18')
// (6, 14, 'sp4_r_v_b_7')
// (7, 6, 'sp4_v_t_38')
// (7, 6, 'sp4_v_t_41')
// (7, 7, 'sp4_v_b_38')
// (7, 7, 'sp4_v_b_41')
// (7, 8, 'local_g3_4')
// (7, 8, 'lutff_7/in_0')
// (7, 8, 'sp4_r_v_b_45')
// (7, 8, 'sp4_v_b_27')
// (7, 8, 'sp4_v_b_28')
// (7, 9, 'local_g0_2')
// (7, 9, 'local_g1_6')
// (7, 9, 'lutff_0/in_0')
// (7, 9, 'lutff_3/in_0')
// (7, 9, 'lutff_5/in_0')
// (7, 9, 'lutff_7/in_0')
// (7, 9, 'neigh_op_top_2')
// (7, 9, 'sp4_r_v_b_32')
// (7, 9, 'sp4_v_b_14')
// (7, 9, 'sp4_v_b_17')
// (7, 10, 'local_g2_2')
// (7, 10, 'local_g3_2')
// (7, 10, 'lutff_2/out')
// (7, 10, 'lutff_3/in_0')
// (7, 10, 'lutff_4/in_0')
// (7, 10, 'lutff_6/in_0')
// (7, 10, 'lutff_7/in_0')
// (7, 10, 'sp12_h_r_12')
// (7, 10, 'sp4_r_v_b_21')
// (7, 10, 'sp4_v_b_3')
// (7, 10, 'sp4_v_b_4')
// (7, 10, 'sp4_v_t_42')
// (7, 11, 'local_g0_2')
// (7, 11, 'lutff_0/in_0')
// (7, 11, 'lutff_2/in_0')
// (7, 11, 'neigh_op_bot_2')
// (7, 11, 'sp4_h_r_45')
// (7, 11, 'sp4_r_v_b_8')
// (7, 11, 'sp4_v_b_42')
// (7, 12, 'local_g2_7')
// (7, 12, 'lutff_3/in_0')
// (7, 12, 'sp4_v_b_31')
// (7, 13, 'sp4_v_b_18')
// (7, 14, 'sp4_v_b_7')
// (8, 7, 'sp4_v_t_45')
// (8, 8, 'sp4_v_b_45')
// (8, 9, 'neigh_op_tnl_2')
// (8, 9, 'sp4_v_b_32')
// (8, 10, 'neigh_op_lft_2')
// (8, 10, 'sp12_h_r_15')
// (8, 10, 'sp4_v_b_21')
// (8, 11, 'neigh_op_bnl_2')
// (8, 11, 'sp4_h_l_45')
// (8, 11, 'sp4_v_b_8')
// (9, 10, 'sp12_h_r_16')
// (10, 10, 'sp12_h_r_19')
// (11, 10, 'sp12_h_r_20')
// (12, 10, 'sp12_h_r_23')
// (13, 10, 'span12_horz_23')

wire D5;
// (3, 9, 'sp12_h_r_1')
// (4, 9, 'sp12_h_r_2')
// (5, 9, 'sp12_h_r_5')
// (6, 9, 'sp12_h_r_6')
// (7, 9, 'local_g1_4')
// (7, 9, 'lutff_2/in_1')
// (7, 9, 'sp12_h_r_9')
// (7, 9, 'sp4_h_r_4')
// (8, 9, 'local_g0_2')
// (8, 9, 'lutff_6/in_0')
// (8, 9, 'sp12_h_r_10')
// (8, 9, 'sp4_h_r_17')
// (9, 9, 'sp12_h_r_13')
// (9, 9, 'sp4_h_r_28')
// (10, 9, 'sp12_h_r_14')
// (10, 9, 'sp4_h_r_41')
// (11, 8, 'neigh_op_tnr_5')
// (11, 9, 'neigh_op_rgt_5')
// (11, 9, 'sp12_h_r_17')
// (11, 9, 'sp4_h_l_41')
// (11, 10, 'neigh_op_bnr_5')
// (12, 8, 'neigh_op_top_5')
// (12, 9, 'lutff_5/out')
// (12, 9, 'sp12_h_r_18')
// (12, 10, 'neigh_op_bot_5')
// (13, 8, 'logic_op_tnl_5')
// (13, 9, 'io_1/D_OUT_0')
// (13, 9, 'io_1/PAD')
// (13, 9, 'local_g0_5')
// (13, 9, 'logic_op_lft_5')
// (13, 9, 'span12_horz_18')
// (13, 10, 'logic_op_bnl_5')

wire n6;
// (3, 10, 'sp4_h_r_7')
// (4, 10, 'sp4_h_r_18')
// (4, 11, 'sp4_h_r_11')
// (5, 10, 'local_g2_7')
// (5, 10, 'local_g3_7')
// (5, 10, 'lutff_1/in_1')
// (5, 10, 'lutff_2/in_1')
// (5, 10, 'lutff_3/in_1')
// (5, 10, 'lutff_4/in_1')
// (5, 10, 'sp4_h_r_31')
// (5, 11, 'local_g0_6')
// (5, 11, 'lutff_3/in_1')
// (5, 11, 'lutff_7/in_1')
// (5, 11, 'sp4_h_r_22')
// (6, 7, 'neigh_op_tnr_5')
// (6, 7, 'sp4_r_v_b_39')
// (6, 8, 'local_g2_5')
// (6, 8, 'local_g3_5')
// (6, 8, 'lutff_3/in_1')
// (6, 8, 'lutff_4/in_1')
// (6, 8, 'lutff_5/in_1')
// (6, 8, 'lutff_7/in_1')
// (6, 8, 'neigh_op_rgt_5')
// (6, 8, 'sp4_r_v_b_26')
// (6, 9, 'neigh_op_bnr_5')
// (6, 9, 'sp4_r_v_b_15')
// (6, 10, 'sp4_h_r_42')
// (6, 10, 'sp4_r_v_b_2')
// (6, 11, 'sp4_h_r_35')
// (6, 11, 'sp4_r_v_b_37')
// (6, 12, 'local_g0_0')
// (6, 12, 'lutff_1/in_1')
// (6, 12, 'lutff_3/in_1')
// (6, 12, 'lutff_5/in_1')
// (6, 12, 'lutff_7/in_1')
// (6, 12, 'sp4_r_v_b_24')
// (6, 13, 'sp4_r_v_b_13')
// (6, 14, 'sp4_r_v_b_0')
// (7, 6, 'sp4_v_t_39')
// (7, 7, 'neigh_op_top_5')
// (7, 7, 'sp4_v_b_39')
// (7, 8, 'local_g1_5')
// (7, 8, 'lutff_5/out')
// (7, 8, 'lutff_7/in_1')
// (7, 8, 'sp4_r_v_b_43')
// (7, 8, 'sp4_v_b_26')
// (7, 9, 'local_g0_5')
// (7, 9, 'local_g1_5')
// (7, 9, 'lutff_0/in_1')
// (7, 9, 'lutff_3/in_1')
// (7, 9, 'lutff_5/in_1')
// (7, 9, 'lutff_7/in_1')
// (7, 9, 'neigh_op_bot_5')
// (7, 9, 'sp4_r_v_b_30')
// (7, 9, 'sp4_v_b_15')
// (7, 10, 'local_g0_2')
// (7, 10, 'local_g1_2')
// (7, 10, 'lutff_3/in_1')
// (7, 10, 'lutff_4/in_1')
// (7, 10, 'lutff_6/in_1')
// (7, 10, 'lutff_7/in_1')
// (7, 10, 'sp4_h_l_42')
// (7, 10, 'sp4_r_v_b_19')
// (7, 10, 'sp4_v_b_2')
// (7, 10, 'sp4_v_t_37')
// (7, 11, 'local_g2_5')
// (7, 11, 'lutff_0/in_1')
// (7, 11, 'lutff_2/in_1')
// (7, 11, 'sp4_h_r_46')
// (7, 11, 'sp4_r_v_b_6')
// (7, 11, 'sp4_v_b_37')
// (7, 12, 'local_g2_0')
// (7, 12, 'lutff_3/in_1')
// (7, 12, 'sp4_v_b_24')
// (7, 13, 'sp4_v_b_13')
// (7, 14, 'sp4_v_b_0')
// (8, 7, 'neigh_op_tnl_5')
// (8, 7, 'sp4_v_t_43')
// (8, 8, 'neigh_op_lft_5')
// (8, 8, 'sp4_v_b_43')
// (8, 9, 'neigh_op_bnl_5')
// (8, 9, 'sp4_v_b_30')
// (8, 10, 'sp4_v_b_19')
// (8, 11, 'sp4_h_l_46')
// (8, 11, 'sp4_v_b_6')

wire n7;
// (4, 9, 'neigh_op_tnr_0')
// (4, 10, 'neigh_op_rgt_0')
// (4, 10, 'sp4_h_r_5')
// (4, 11, 'neigh_op_bnr_0')
// (5, 9, 'neigh_op_top_0')
// (5, 10, 'lutff_0/out')
// (5, 10, 'sp4_h_r_16')
// (5, 11, 'neigh_op_bot_0')
// (6, 9, 'neigh_op_tnl_0')
// (6, 10, 'neigh_op_lft_0')
// (6, 10, 'sp4_h_r_29')
// (6, 11, 'neigh_op_bnl_0')
// (7, 10, 'local_g2_0')
// (7, 10, 'lutff_2/in_2')
// (7, 10, 'sp4_h_r_40')
// (8, 10, 'sp4_h_l_40')

reg n8 = 0;
// (4, 9, 'neigh_op_tnr_1')
// (4, 10, 'neigh_op_rgt_1')
// (4, 11, 'neigh_op_bnr_1')
// (5, 9, 'neigh_op_top_1')
// (5, 10, 'local_g0_1')
// (5, 10, 'lutff_0/in_1')
// (5, 10, 'lutff_1/out')
// (5, 11, 'neigh_op_bot_1')
// (6, 9, 'neigh_op_tnl_1')
// (6, 10, 'local_g1_1')
// (6, 10, 'lutff_4/in_2')
// (6, 10, 'neigh_op_lft_1')
// (6, 11, 'neigh_op_bnl_1')

reg n9 = 0;
// (4, 9, 'neigh_op_tnr_2')
// (4, 10, 'neigh_op_rgt_2')
// (4, 10, 'sp4_h_r_9')
// (4, 11, 'neigh_op_bnr_2')
// (5, 9, 'neigh_op_top_2')
// (5, 10, 'lutff_2/out')
// (5, 10, 'sp4_h_r_20')
// (5, 11, 'neigh_op_bot_2')
// (6, 9, 'neigh_op_tnl_2')
// (6, 10, 'local_g0_2')
// (6, 10, 'lutff_2/in_2')
// (6, 10, 'neigh_op_lft_2')
// (6, 10, 'sp4_h_r_33')
// (6, 11, 'neigh_op_bnl_2')
// (7, 10, 'local_g3_4')
// (7, 10, 'lutff_1/in_0')
// (7, 10, 'sp4_h_r_44')
// (8, 10, 'sp4_h_l_44')

reg n10 = 0;
// (4, 9, 'neigh_op_tnr_3')
// (4, 10, 'neigh_op_rgt_3')
// (4, 10, 'sp4_h_r_11')
// (4, 11, 'neigh_op_bnr_3')
// (5, 9, 'neigh_op_top_3')
// (5, 10, 'lutff_3/out')
// (5, 10, 'sp4_h_r_22')
// (5, 11, 'neigh_op_bot_3')
// (6, 9, 'neigh_op_tnl_3')
// (6, 10, 'local_g0_3')
// (6, 10, 'lutff_7/in_2')
// (6, 10, 'neigh_op_lft_3')
// (6, 10, 'sp4_h_r_35')
// (6, 11, 'neigh_op_bnl_3')
// (7, 10, 'local_g2_6')
// (7, 10, 'lutff_1/in_3')
// (7, 10, 'sp4_h_r_46')
// (8, 10, 'sp4_h_l_46')

reg n11 = 0;
// (4, 10, 'neigh_op_tnr_3')
// (4, 11, 'neigh_op_rgt_3')
// (4, 12, 'neigh_op_bnr_3')
// (5, 2, 'sp12_v_t_22')
// (5, 3, 'sp12_v_b_22')
// (5, 4, 'sp12_v_b_21')
// (5, 5, 'sp12_v_b_18')
// (5, 6, 'sp12_v_b_17')
// (5, 7, 'sp12_v_b_14')
// (5, 8, 'sp12_v_b_13')
// (5, 9, 'sp12_v_b_10')
// (5, 10, 'local_g2_1')
// (5, 10, 'lutff_0/in_3')
// (5, 10, 'neigh_op_top_3')
// (5, 10, 'sp12_v_b_9')
// (5, 11, 'lutff_3/out')
// (5, 11, 'sp12_v_b_6')
// (5, 12, 'neigh_op_bot_3')
// (5, 12, 'sp12_v_b_5')
// (5, 13, 'sp12_v_b_2')
// (5, 14, 'sp12_v_b_1')
// (6, 10, 'neigh_op_tnl_3')
// (6, 11, 'local_g0_3')
// (6, 11, 'lutff_1/in_2')
// (6, 11, 'neigh_op_lft_3')
// (6, 12, 'neigh_op_bnl_3')

reg n12 = 0;
// (4, 10, 'neigh_op_tnr_7')
// (4, 11, 'neigh_op_rgt_7')
// (4, 12, 'neigh_op_bnr_7')
// (5, 10, 'local_g1_7')
// (5, 10, 'lutff_0/in_2')
// (5, 10, 'neigh_op_top_7')
// (5, 11, 'lutff_7/out')
// (5, 12, 'neigh_op_bot_7')
// (6, 10, 'neigh_op_tnl_7')
// (6, 11, 'local_g1_7')
// (6, 11, 'lutff_0/in_2')
// (6, 11, 'neigh_op_lft_7')
// (6, 12, 'neigh_op_bnl_7')

wire n13;
// (4, 10, 'sp4_h_r_6')
// (5, 9, 'neigh_op_tnr_7')
// (5, 10, 'local_g0_3')
// (5, 10, 'lutff_3/in_2')
// (5, 10, 'neigh_op_rgt_7')
// (5, 10, 'sp4_h_r_19')
// (5, 11, 'neigh_op_bnr_7')
// (6, 9, 'neigh_op_top_7')
// (6, 10, 'lutff_7/out')
// (6, 10, 'sp4_h_r_30')
// (6, 11, 'neigh_op_bot_7')
// (7, 9, 'neigh_op_tnl_7')
// (7, 10, 'neigh_op_lft_7')
// (7, 10, 'sp4_h_r_43')
// (7, 11, 'neigh_op_bnl_7')
// (8, 10, 'sp4_h_l_43')

wire n14;
// (5, 7, 'neigh_op_tnr_0')
// (5, 8, 'neigh_op_rgt_0')
// (5, 9, 'neigh_op_bnr_0')
// (6, 7, 'neigh_op_top_0')
// (6, 8, 'lutff_0/out')
// (6, 9, 'neigh_op_bot_0')
// (7, 7, 'neigh_op_tnl_0')
// (7, 8, 'local_g1_0')
// (7, 8, 'lutff_5/in_0')
// (7, 8, 'neigh_op_lft_0')
// (7, 9, 'neigh_op_bnl_0')

reg n15 = 0;
// (5, 7, 'neigh_op_tnr_3')
// (5, 8, 'neigh_op_rgt_3')
// (5, 9, 'neigh_op_bnr_3')
// (6, 7, 'neigh_op_top_3')
// (6, 8, 'local_g0_3')
// (6, 8, 'lutff_0/in_3')
// (6, 8, 'lutff_3/out')
// (6, 9, 'local_g0_3')
// (6, 9, 'lutff_7/in_2')
// (6, 9, 'neigh_op_bot_3')
// (7, 7, 'neigh_op_tnl_3')
// (7, 8, 'neigh_op_lft_3')
// (7, 9, 'neigh_op_bnl_3')

reg n16 = 0;
// (5, 7, 'neigh_op_tnr_4')
// (5, 8, 'neigh_op_rgt_4')
// (5, 9, 'neigh_op_bnr_4')
// (6, 7, 'neigh_op_top_4')
// (6, 8, 'local_g2_4')
// (6, 8, 'lutff_0/in_0')
// (6, 8, 'lutff_4/out')
// (6, 9, 'local_g0_4')
// (6, 9, 'lutff_4/in_2')
// (6, 9, 'neigh_op_bot_4')
// (7, 7, 'neigh_op_tnl_4')
// (7, 8, 'neigh_op_lft_4')
// (7, 9, 'neigh_op_bnl_4')

reg n17 = 0;
// (5, 7, 'neigh_op_tnr_5')
// (5, 8, 'neigh_op_rgt_5')
// (5, 9, 'neigh_op_bnr_5')
// (6, 7, 'neigh_op_top_5')
// (6, 8, 'local_g1_5')
// (6, 8, 'lutff_0/in_2')
// (6, 8, 'lutff_5/out')
// (6, 9, 'local_g1_5')
// (6, 9, 'lutff_6/in_2')
// (6, 9, 'neigh_op_bot_5')
// (7, 7, 'neigh_op_tnl_5')
// (7, 8, 'neigh_op_lft_5')
// (7, 9, 'neigh_op_bnl_5')

reg n18 = 0;
// (5, 7, 'neigh_op_tnr_7')
// (5, 8, 'neigh_op_rgt_7')
// (5, 9, 'neigh_op_bnr_7')
// (6, 7, 'neigh_op_top_7')
// (6, 8, 'local_g2_7')
// (6, 8, 'lutff_0/in_1')
// (6, 8, 'lutff_7/out')
// (6, 8, 'sp4_r_v_b_47')
// (6, 9, 'local_g0_1')
// (6, 9, 'lutff_5/in_2')
// (6, 9, 'neigh_op_bot_7')
// (6, 9, 'sp4_r_v_b_34')
// (6, 10, 'sp4_r_v_b_23')
// (6, 11, 'sp4_r_v_b_10')
// (7, 7, 'neigh_op_tnl_7')
// (7, 7, 'sp4_v_t_47')
// (7, 8, 'neigh_op_lft_7')
// (7, 8, 'sp4_v_b_47')
// (7, 9, 'neigh_op_bnl_7')
// (7, 9, 'sp4_v_b_34')
// (7, 10, 'sp4_v_b_23')
// (7, 11, 'sp4_v_b_10')

wire n19;
// (5, 8, 'neigh_op_tnr_2')
// (5, 9, 'neigh_op_rgt_2')
// (5, 10, 'neigh_op_bnr_2')
// (6, 8, 'neigh_op_top_2')
// (6, 9, 'lutff_2/out')
// (6, 9, 'sp4_h_r_4')
// (6, 10, 'neigh_op_bot_2')
// (7, 8, 'neigh_op_tnl_2')
// (7, 9, 'local_g0_1')
// (7, 9, 'lutff_5/in_2')
// (7, 9, 'neigh_op_lft_2')
// (7, 9, 'sp4_h_r_17')
// (7, 10, 'neigh_op_bnl_2')
// (8, 9, 'sp4_h_r_28')
// (9, 9, 'sp4_h_r_41')
// (10, 9, 'sp4_h_l_41')

wire n20;
// (5, 8, 'neigh_op_tnr_3')
// (5, 9, 'neigh_op_rgt_3')
// (5, 10, 'neigh_op_bnr_3')
// (6, 8, 'neigh_op_top_3')
// (6, 9, 'lutff_3/out')
// (6, 10, 'neigh_op_bot_3')
// (7, 8, 'neigh_op_tnl_3')
// (7, 9, 'local_g0_3')
// (7, 9, 'lutff_7/in_2')
// (7, 9, 'neigh_op_lft_3')
// (7, 10, 'neigh_op_bnl_3')

wire n21;
// (5, 8, 'neigh_op_tnr_4')
// (5, 9, 'neigh_op_rgt_4')
// (5, 10, 'neigh_op_bnr_4')
// (6, 8, 'local_g0_4')
// (6, 8, 'lutff_4/in_2')
// (6, 8, 'neigh_op_top_4')
// (6, 9, 'lutff_4/out')
// (6, 10, 'neigh_op_bot_4')
// (7, 8, 'neigh_op_tnl_4')
// (7, 9, 'neigh_op_lft_4')
// (7, 10, 'neigh_op_bnl_4')

wire n22;
// (5, 8, 'neigh_op_tnr_5')
// (5, 9, 'neigh_op_rgt_5')
// (5, 10, 'neigh_op_bnr_5')
// (6, 8, 'local_g0_5')
// (6, 8, 'lutff_7/in_2')
// (6, 8, 'neigh_op_top_5')
// (6, 9, 'lutff_5/out')
// (6, 10, 'neigh_op_bot_5')
// (7, 8, 'neigh_op_tnl_5')
// (7, 9, 'neigh_op_lft_5')
// (7, 10, 'neigh_op_bnl_5')

wire n23;
// (5, 8, 'neigh_op_tnr_6')
// (5, 9, 'neigh_op_rgt_6')
// (5, 10, 'neigh_op_bnr_6')
// (6, 8, 'local_g1_6')
// (6, 8, 'lutff_5/in_2')
// (6, 8, 'neigh_op_top_6')
// (6, 9, 'lutff_6/out')
// (6, 10, 'neigh_op_bot_6')
// (7, 8, 'neigh_op_tnl_6')
// (7, 9, 'neigh_op_lft_6')
// (7, 10, 'neigh_op_bnl_6')

wire n24;
// (5, 8, 'neigh_op_tnr_7')
// (5, 9, 'neigh_op_rgt_7')
// (5, 10, 'neigh_op_bnr_7')
// (6, 8, 'local_g0_7')
// (6, 8, 'lutff_3/in_2')
// (6, 8, 'neigh_op_top_7')
// (6, 9, 'lutff_7/out')
// (6, 10, 'neigh_op_bot_7')
// (7, 8, 'neigh_op_tnl_7')
// (7, 9, 'neigh_op_lft_7')
// (7, 10, 'neigh_op_bnl_7')

wire n25;
// (5, 9, 'neigh_op_tnr_0')
// (5, 10, 'neigh_op_rgt_0')
// (5, 11, 'neigh_op_bnr_0')
// (6, 9, 'neigh_op_top_0')
// (6, 10, 'lutff_0/out')
// (6, 11, 'neigh_op_bot_0')
// (7, 9, 'neigh_op_tnl_0')
// (7, 10, 'local_g1_0')
// (7, 10, 'lutff_7/in_2')
// (7, 10, 'neigh_op_lft_0')
// (7, 11, 'neigh_op_bnl_0')

wire n26;
// (5, 9, 'neigh_op_tnr_1')
// (5, 10, 'neigh_op_rgt_1')
// (5, 11, 'neigh_op_bnr_1')
// (6, 9, 'neigh_op_top_1')
// (6, 10, 'lutff_1/out')
// (6, 11, 'neigh_op_bot_1')
// (7, 9, 'neigh_op_tnl_1')
// (7, 10, 'local_g0_1')
// (7, 10, 'lutff_3/in_2')
// (7, 10, 'neigh_op_lft_1')
// (7, 11, 'neigh_op_bnl_1')

wire n27;
// (5, 9, 'neigh_op_tnr_2')
// (5, 10, 'local_g2_2')
// (5, 10, 'lutff_2/in_2')
// (5, 10, 'neigh_op_rgt_2')
// (5, 11, 'neigh_op_bnr_2')
// (6, 9, 'neigh_op_top_2')
// (6, 10, 'lutff_2/out')
// (6, 11, 'neigh_op_bot_2')
// (7, 9, 'neigh_op_tnl_2')
// (7, 10, 'neigh_op_lft_2')
// (7, 11, 'neigh_op_bnl_2')

wire n28;
// (5, 9, 'neigh_op_tnr_3')
// (5, 10, 'neigh_op_rgt_3')
// (5, 11, 'neigh_op_bnr_3')
// (6, 9, 'neigh_op_top_3')
// (6, 10, 'lutff_3/out')
// (6, 11, 'neigh_op_bot_3')
// (7, 9, 'neigh_op_tnl_3')
// (7, 10, 'local_g1_3')
// (7, 10, 'lutff_6/in_2')
// (7, 10, 'neigh_op_lft_3')
// (7, 11, 'neigh_op_bnl_3')

wire n29;
// (5, 9, 'neigh_op_tnr_4')
// (5, 10, 'local_g3_4')
// (5, 10, 'lutff_1/in_2')
// (5, 10, 'neigh_op_rgt_4')
// (5, 11, 'neigh_op_bnr_4')
// (6, 9, 'neigh_op_top_4')
// (6, 10, 'lutff_4/out')
// (6, 11, 'neigh_op_bot_4')
// (7, 9, 'neigh_op_tnl_4')
// (7, 10, 'neigh_op_lft_4')
// (7, 11, 'neigh_op_bnl_4')

wire n30;
// (5, 9, 'neigh_op_tnr_5')
// (5, 10, 'neigh_op_rgt_5')
// (5, 11, 'neigh_op_bnr_5')
// (6, 9, 'neigh_op_top_5')
// (6, 10, 'lutff_5/out')
// (6, 11, 'neigh_op_bot_5')
// (7, 9, 'neigh_op_tnl_5')
// (7, 10, 'local_g1_5')
// (7, 10, 'lutff_4/in_2')
// (7, 10, 'neigh_op_lft_5')
// (7, 11, 'neigh_op_bnl_5')

wire n31;
// (5, 9, 'neigh_op_tnr_6')
// (5, 10, 'local_g2_6')
// (5, 10, 'lutff_4/in_2')
// (5, 10, 'neigh_op_rgt_6')
// (5, 11, 'neigh_op_bnr_6')
// (6, 9, 'neigh_op_top_6')
// (6, 10, 'lutff_6/out')
// (6, 11, 'neigh_op_bot_6')
// (7, 9, 'neigh_op_tnl_6')
// (7, 10, 'neigh_op_lft_6')
// (7, 11, 'neigh_op_bnl_6')

reg n32 = 0;
// (5, 10, 'local_g0_4')
// (5, 10, 'lutff_0/in_0')
// (5, 10, 'sp4_h_r_4')
// (6, 9, 'neigh_op_tnr_6')
// (6, 10, 'local_g3_6')
// (6, 10, 'lutff_3/in_2')
// (6, 10, 'neigh_op_rgt_6')
// (6, 10, 'sp4_h_r_17')
// (6, 11, 'neigh_op_bnr_6')
// (7, 9, 'neigh_op_top_6')
// (7, 10, 'lutff_6/out')
// (7, 10, 'sp4_h_r_28')
// (7, 11, 'neigh_op_bot_6')
// (8, 9, 'neigh_op_tnl_6')
// (8, 10, 'neigh_op_lft_6')
// (8, 10, 'sp4_h_r_41')
// (8, 11, 'neigh_op_bnl_6')
// (9, 10, 'sp4_h_l_41')

wire n33;
// (5, 10, 'neigh_op_tnr_0')
// (5, 11, 'local_g3_0')
// (5, 11, 'lutff_7/in_2')
// (5, 11, 'neigh_op_rgt_0')
// (5, 12, 'neigh_op_bnr_0')
// (6, 10, 'neigh_op_top_0')
// (6, 11, 'lutff_0/out')
// (6, 12, 'neigh_op_bot_0')
// (7, 10, 'neigh_op_tnl_0')
// (7, 11, 'neigh_op_lft_0')
// (7, 12, 'neigh_op_bnl_0')

wire n34;
// (5, 10, 'neigh_op_tnr_1')
// (5, 11, 'local_g2_1')
// (5, 11, 'lutff_3/in_2')
// (5, 11, 'neigh_op_rgt_1')
// (5, 12, 'neigh_op_bnr_1')
// (6, 10, 'neigh_op_top_1')
// (6, 11, 'lutff_1/out')
// (6, 12, 'neigh_op_bot_1')
// (7, 10, 'neigh_op_tnl_1')
// (7, 11, 'neigh_op_lft_1')
// (7, 12, 'neigh_op_bnl_1')

wire n35;
// (5, 10, 'neigh_op_tnr_2')
// (5, 11, 'neigh_op_rgt_2')
// (5, 12, 'neigh_op_bnr_2')
// (6, 10, 'neigh_op_top_2')
// (6, 11, 'lutff_2/out')
// (6, 12, 'local_g1_2')
// (6, 12, 'lutff_5/in_2')
// (6, 12, 'neigh_op_bot_2')
// (7, 10, 'neigh_op_tnl_2')
// (7, 11, 'neigh_op_lft_2')
// (7, 12, 'neigh_op_bnl_2')

wire n36;
// (5, 10, 'neigh_op_tnr_3')
// (5, 11, 'neigh_op_rgt_3')
// (5, 12, 'neigh_op_bnr_3')
// (6, 10, 'neigh_op_top_3')
// (6, 11, 'lutff_3/out')
// (6, 12, 'local_g0_3')
// (6, 12, 'lutff_7/in_2')
// (6, 12, 'neigh_op_bot_3')
// (7, 10, 'neigh_op_tnl_3')
// (7, 11, 'neigh_op_lft_3')
// (7, 12, 'neigh_op_bnl_3')

wire n37;
// (5, 10, 'neigh_op_tnr_4')
// (5, 11, 'neigh_op_rgt_4')
// (5, 12, 'neigh_op_bnr_4')
// (6, 10, 'neigh_op_top_4')
// (6, 11, 'lutff_4/out')
// (6, 12, 'local_g1_4')
// (6, 12, 'lutff_1/in_2')
// (6, 12, 'neigh_op_bot_4')
// (7, 10, 'neigh_op_tnl_4')
// (7, 11, 'neigh_op_lft_4')
// (7, 12, 'neigh_op_bnl_4')

wire n38;
// (5, 10, 'neigh_op_tnr_5')
// (5, 11, 'neigh_op_rgt_5')
// (5, 12, 'neigh_op_bnr_5')
// (6, 10, 'neigh_op_top_5')
// (6, 11, 'lutff_5/out')
// (6, 12, 'neigh_op_bot_5')
// (7, 10, 'neigh_op_tnl_5')
// (7, 11, 'local_g1_5')
// (7, 11, 'lutff_0/in_2')
// (7, 11, 'neigh_op_lft_5')
// (7, 12, 'neigh_op_bnl_5')

wire n39;
// (5, 10, 'neigh_op_tnr_6')
// (5, 11, 'neigh_op_rgt_6')
// (5, 12, 'neigh_op_bnr_6')
// (6, 10, 'neigh_op_top_6')
// (6, 11, 'lutff_6/out')
// (6, 12, 'local_g1_6')
// (6, 12, 'lutff_3/in_2')
// (6, 12, 'neigh_op_bot_6')
// (7, 10, 'neigh_op_tnl_6')
// (7, 11, 'neigh_op_lft_6')
// (7, 12, 'neigh_op_bnl_6')

wire n40;
// (5, 10, 'neigh_op_tnr_7')
// (5, 11, 'neigh_op_rgt_7')
// (5, 12, 'neigh_op_bnr_7')
// (6, 10, 'neigh_op_top_7')
// (6, 11, 'lutff_7/out')
// (6, 12, 'neigh_op_bot_7')
// (7, 10, 'neigh_op_tnl_7')
// (7, 11, 'local_g1_7')
// (7, 11, 'lutff_2/in_2')
// (7, 11, 'neigh_op_lft_7')
// (7, 12, 'neigh_op_bnl_7')

wire n41;
// (5, 11, 'neigh_op_tnr_0')
// (5, 12, 'neigh_op_rgt_0')
// (5, 13, 'neigh_op_bnr_0')
// (6, 9, 'sp4_r_v_b_36')
// (6, 10, 'sp4_r_v_b_25')
// (6, 11, 'neigh_op_top_0')
// (6, 11, 'sp4_r_v_b_12')
// (6, 12, 'lutff_0/out')
// (6, 12, 'sp4_r_v_b_1')
// (6, 13, 'neigh_op_bot_0')
// (7, 8, 'sp4_v_t_36')
// (7, 9, 'sp4_v_b_36')
// (7, 10, 'local_g3_1')
// (7, 10, 'lutff_2/in_0')
// (7, 10, 'sp4_v_b_25')
// (7, 11, 'neigh_op_tnl_0')
// (7, 11, 'sp4_v_b_12')
// (7, 12, 'neigh_op_lft_0')
// (7, 12, 'sp4_v_b_1')
// (7, 13, 'neigh_op_bnl_0')

reg n42 = 0;
// (5, 11, 'neigh_op_tnr_1')
// (5, 12, 'neigh_op_rgt_1')
// (5, 13, 'neigh_op_bnr_1')
// (6, 11, 'local_g1_1')
// (6, 11, 'lutff_4/in_2')
// (6, 11, 'neigh_op_top_1')
// (6, 12, 'local_g2_1')
// (6, 12, 'lutff_0/in_3')
// (6, 12, 'lutff_1/out')
// (6, 13, 'neigh_op_bot_1')
// (7, 11, 'neigh_op_tnl_1')
// (7, 12, 'neigh_op_lft_1')
// (7, 13, 'neigh_op_bnl_1')

reg n43 = 0;
// (5, 11, 'neigh_op_tnr_3')
// (5, 12, 'neigh_op_rgt_3')
// (5, 13, 'neigh_op_bnr_3')
// (6, 11, 'local_g1_3')
// (6, 11, 'lutff_6/in_2')
// (6, 11, 'neigh_op_top_3')
// (6, 12, 'local_g2_3')
// (6, 12, 'lutff_0/in_1')
// (6, 12, 'lutff_3/out')
// (6, 13, 'neigh_op_bot_3')
// (7, 11, 'neigh_op_tnl_3')
// (7, 12, 'neigh_op_lft_3')
// (7, 13, 'neigh_op_bnl_3')

reg n44 = 0;
// (5, 11, 'neigh_op_tnr_5')
// (5, 12, 'neigh_op_rgt_5')
// (5, 13, 'neigh_op_bnr_5')
// (6, 11, 'local_g1_5')
// (6, 11, 'lutff_2/in_2')
// (6, 11, 'neigh_op_top_5')
// (6, 12, 'local_g1_5')
// (6, 12, 'lutff_0/in_2')
// (6, 12, 'lutff_5/out')
// (6, 13, 'neigh_op_bot_5')
// (7, 11, 'neigh_op_tnl_5')
// (7, 12, 'neigh_op_lft_5')
// (7, 13, 'neigh_op_bnl_5')

reg n45 = 0;
// (5, 11, 'neigh_op_tnr_7')
// (5, 12, 'neigh_op_rgt_7')
// (5, 13, 'neigh_op_bnr_7')
// (6, 11, 'local_g0_7')
// (6, 11, 'lutff_3/in_2')
// (6, 11, 'neigh_op_top_7')
// (6, 12, 'local_g1_7')
// (6, 12, 'lutff_0/in_0')
// (6, 12, 'lutff_7/out')
// (6, 13, 'neigh_op_bot_7')
// (7, 11, 'neigh_op_tnl_7')
// (7, 12, 'neigh_op_lft_7')
// (7, 13, 'neigh_op_bnl_7')

reg n46 = 0;
// (6, 7, 'neigh_op_tnr_7')
// (6, 8, 'neigh_op_rgt_7')
// (6, 9, 'local_g0_7')
// (6, 9, 'lutff_1/in_2')
// (6, 9, 'neigh_op_bnr_7')
// (7, 7, 'neigh_op_top_7')
// (7, 8, 'local_g0_7')
// (7, 8, 'lutff_7/in_2')
// (7, 8, 'lutff_7/out')
// (7, 9, 'local_g1_7')
// (7, 9, 'lutff_1/in_1')
// (7, 9, 'neigh_op_bot_7')
// (8, 7, 'neigh_op_tnl_7')
// (8, 8, 'neigh_op_lft_7')
// (8, 9, 'neigh_op_bnl_7')

wire n47;
// (6, 8, 'neigh_op_tnr_0')
// (6, 9, 'neigh_op_rgt_0')
// (6, 10, 'neigh_op_bnr_0')
// (7, 0, 'span12_vert_16')
// (7, 1, 'sp12_v_b_16')
// (7, 2, 'sp12_v_b_15')
// (7, 3, 'sp12_v_b_12')
// (7, 4, 'sp12_v_b_11')
// (7, 5, 'sp12_v_b_8')
// (7, 6, 'sp12_v_b_7')
// (7, 7, 'sp12_v_b_4')
// (7, 8, 'local_g3_3')
// (7, 8, 'lutff_global/cen')
// (7, 8, 'neigh_op_top_0')
// (7, 8, 'sp12_v_b_3')
// (7, 9, 'lutff_0/out')
// (7, 9, 'sp12_v_b_0')
// (7, 10, 'neigh_op_bot_0')
// (8, 8, 'neigh_op_tnl_0')
// (8, 9, 'neigh_op_lft_0')
// (8, 10, 'neigh_op_bnl_0')

wire n48;
// (6, 8, 'neigh_op_tnr_1')
// (6, 9, 'neigh_op_rgt_1')
// (6, 10, 'neigh_op_bnr_1')
// (7, 8, 'local_g1_1')
// (7, 8, 'lutff_5/in_1')
// (7, 8, 'neigh_op_top_1')
// (7, 9, 'lutff_1/out')
// (7, 10, 'neigh_op_bot_1')
// (8, 8, 'neigh_op_tnl_1')
// (8, 9, 'neigh_op_lft_1')
// (8, 10, 'neigh_op_bnl_1')

wire n49;
// (6, 8, 'neigh_op_tnr_2')
// (6, 9, 'neigh_op_rgt_2')
// (6, 10, 'neigh_op_bnr_2')
// (7, 8, 'neigh_op_top_2')
// (7, 9, 'local_g1_2')
// (7, 9, 'lutff_2/out')
// (7, 9, 'lutff_3/in_2')
// (7, 10, 'neigh_op_bot_2')
// (8, 8, 'neigh_op_tnl_2')
// (8, 9, 'neigh_op_lft_2')
// (8, 10, 'neigh_op_bnl_2')

reg n50 = 0;
// (6, 8, 'neigh_op_tnr_3')
// (6, 9, 'local_g2_3')
// (6, 9, 'lutff_0/in_1')
// (6, 9, 'neigh_op_rgt_3')
// (6, 10, 'neigh_op_bnr_3')
// (7, 8, 'neigh_op_top_3')
// (7, 9, 'local_g1_3')
// (7, 9, 'local_g2_3')
// (7, 9, 'lutff_0/in_2')
// (7, 9, 'lutff_1/in_0')
// (7, 9, 'lutff_2/in_2')
// (7, 9, 'lutff_3/out')
// (7, 10, 'neigh_op_bot_3')
// (8, 8, 'neigh_op_tnl_3')
// (8, 9, 'neigh_op_lft_3')
// (8, 10, 'neigh_op_bnl_3')

reg n51 = 0;
// (6, 8, 'neigh_op_tnr_5')
// (6, 9, 'local_g3_5')
// (6, 9, 'lutff_2/in_2')
// (6, 9, 'neigh_op_rgt_5')
// (6, 10, 'neigh_op_bnr_5')
// (7, 8, 'neigh_op_top_5')
// (7, 9, 'local_g2_5')
// (7, 9, 'lutff_1/in_2')
// (7, 9, 'lutff_5/out')
// (7, 10, 'neigh_op_bot_5')
// (8, 8, 'neigh_op_tnl_5')
// (8, 9, 'neigh_op_lft_5')
// (8, 10, 'neigh_op_bnl_5')

reg n52 = 0;
// (6, 8, 'neigh_op_tnr_7')
// (6, 9, 'local_g2_7')
// (6, 9, 'lutff_3/in_2')
// (6, 9, 'neigh_op_rgt_7')
// (6, 10, 'neigh_op_bnr_7')
// (7, 8, 'neigh_op_top_7')
// (7, 9, 'local_g3_7')
// (7, 9, 'lutff_1/in_3')
// (7, 9, 'lutff_7/out')
// (7, 10, 'neigh_op_bot_7')
// (8, 8, 'neigh_op_tnl_7')
// (8, 9, 'neigh_op_lft_7')
// (8, 10, 'neigh_op_bnl_7')

wire n53;
// (6, 9, 'lutff_1/cout')
// (6, 9, 'lutff_2/in_3')

wire n54;
// (6, 9, 'lutff_2/cout')
// (6, 9, 'lutff_3/in_3')

wire n55;
// (6, 9, 'lutff_3/cout')
// (6, 9, 'lutff_4/in_3')

wire n56;
// (6, 9, 'lutff_4/cout')
// (6, 9, 'lutff_5/in_3')

wire n57;
// (6, 9, 'lutff_5/cout')
// (6, 9, 'lutff_6/in_3')

wire n58;
// (6, 9, 'lutff_6/cout')
// (6, 9, 'lutff_7/in_3')

wire n59;
// (6, 9, 'lutff_7/cout')
// (6, 10, 'carry_in')
// (6, 10, 'carry_in_mux')
// (6, 10, 'lutff_0/in_3')

wire n60;
// (6, 9, 'neigh_op_tnr_0')
// (6, 10, 'neigh_op_rgt_0')
// (6, 11, 'neigh_op_bnr_0')
// (7, 9, 'neigh_op_top_0')
// (7, 10, 'local_g3_0')
// (7, 10, 'lutff_0/out')
// (7, 10, 'lutff_2/in_3')
// (7, 11, 'neigh_op_bot_0')
// (8, 9, 'neigh_op_tnl_0')
// (8, 10, 'neigh_op_lft_0')
// (8, 11, 'neigh_op_bnl_0')

wire n61;
// (6, 9, 'neigh_op_tnr_1')
// (6, 10, 'neigh_op_rgt_1')
// (6, 11, 'neigh_op_bnr_1')
// (7, 9, 'neigh_op_top_1')
// (7, 10, 'local_g2_1')
// (7, 10, 'lutff_1/out')
// (7, 10, 'lutff_2/in_1')
// (7, 11, 'neigh_op_bot_1')
// (8, 9, 'neigh_op_tnl_1')
// (8, 10, 'neigh_op_lft_1')
// (8, 11, 'neigh_op_bnl_1')

reg n62 = 0;
// (6, 9, 'neigh_op_tnr_3')
// (6, 10, 'local_g2_3')
// (6, 10, 'lutff_1/in_2')
// (6, 10, 'neigh_op_rgt_3')
// (6, 11, 'neigh_op_bnr_3')
// (7, 9, 'neigh_op_top_3')
// (7, 10, 'local_g2_3')
// (7, 10, 'lutff_0/in_3')
// (7, 10, 'lutff_3/out')
// (7, 11, 'neigh_op_bot_3')
// (8, 9, 'neigh_op_tnl_3')
// (8, 10, 'neigh_op_lft_3')
// (8, 11, 'neigh_op_bnl_3')

reg n63 = 0;
// (6, 9, 'neigh_op_tnr_4')
// (6, 10, 'local_g3_4')
// (6, 10, 'lutff_5/in_2')
// (6, 10, 'neigh_op_rgt_4')
// (6, 11, 'neigh_op_bnr_4')
// (7, 9, 'neigh_op_top_4')
// (7, 10, 'local_g0_4')
// (7, 10, 'lutff_1/in_1')
// (7, 10, 'lutff_4/out')
// (7, 11, 'neigh_op_bot_4')
// (8, 9, 'neigh_op_tnl_4')
// (8, 10, 'neigh_op_lft_4')
// (8, 11, 'neigh_op_bnl_4')

reg n64 = 0;
// (6, 9, 'neigh_op_tnr_7')
// (6, 10, 'local_g3_7')
// (6, 10, 'lutff_0/in_2')
// (6, 10, 'neigh_op_rgt_7')
// (6, 11, 'neigh_op_bnr_7')
// (7, 9, 'neigh_op_top_7')
// (7, 10, 'local_g1_7')
// (7, 10, 'lutff_0/in_2')
// (7, 10, 'lutff_7/out')
// (7, 11, 'neigh_op_bot_7')
// (8, 9, 'neigh_op_tnl_7')
// (8, 10, 'neigh_op_lft_7')
// (8, 11, 'neigh_op_bnl_7')

wire n65;
// (6, 10, 'lutff_0/cout')
// (6, 10, 'lutff_1/in_3')

wire n66;
// (6, 10, 'lutff_1/cout')
// (6, 10, 'lutff_2/in_3')

wire n67;
// (6, 10, 'lutff_2/cout')
// (6, 10, 'lutff_3/in_3')

wire n68;
// (6, 10, 'lutff_3/cout')
// (6, 10, 'lutff_4/in_3')

wire n69;
// (6, 10, 'lutff_4/cout')
// (6, 10, 'lutff_5/in_3')

wire n70;
// (6, 10, 'lutff_5/cout')
// (6, 10, 'lutff_6/in_3')

wire n71;
// (6, 10, 'lutff_6/cout')
// (6, 10, 'lutff_7/in_3')

wire n72;
// (6, 10, 'lutff_7/cout')
// (6, 11, 'carry_in')
// (6, 11, 'carry_in_mux')
// (6, 11, 'lutff_0/in_3')

reg n73 = 0;
// (6, 10, 'neigh_op_tnr_0')
// (6, 11, 'local_g3_0')
// (6, 11, 'lutff_5/in_2')
// (6, 11, 'neigh_op_rgt_0')
// (6, 12, 'neigh_op_bnr_0')
// (7, 10, 'local_g0_0')
// (7, 10, 'lutff_0/in_0')
// (7, 10, 'neigh_op_top_0')
// (7, 11, 'lutff_0/out')
// (7, 12, 'neigh_op_bot_0')
// (8, 10, 'neigh_op_tnl_0')
// (8, 11, 'neigh_op_lft_0')
// (8, 12, 'neigh_op_bnl_0')

reg n74 = 0;
// (6, 10, 'neigh_op_tnr_2')
// (6, 11, 'local_g3_2')
// (6, 11, 'lutff_7/in_2')
// (6, 11, 'neigh_op_rgt_2')
// (6, 12, 'neigh_op_bnr_2')
// (7, 9, 'sp4_r_v_b_45')
// (7, 10, 'local_g0_3')
// (7, 10, 'lutff_0/in_1')
// (7, 10, 'neigh_op_top_2')
// (7, 10, 'sp4_r_v_b_32')
// (7, 11, 'lutff_2/out')
// (7, 11, 'sp4_r_v_b_21')
// (7, 12, 'neigh_op_bot_2')
// (7, 12, 'sp4_r_v_b_8')
// (8, 8, 'sp4_v_t_45')
// (8, 9, 'sp4_v_b_45')
// (8, 10, 'neigh_op_tnl_2')
// (8, 10, 'sp4_v_b_32')
// (8, 11, 'neigh_op_lft_2')
// (8, 11, 'sp4_v_b_21')
// (8, 12, 'neigh_op_bnl_2')
// (8, 12, 'sp4_v_b_8')

wire n75;
// (6, 11, 'lutff_0/cout')
// (6, 11, 'lutff_1/in_3')

wire n76;
// (6, 11, 'lutff_1/cout')
// (6, 11, 'lutff_2/in_3')

wire n77;
// (6, 11, 'lutff_2/cout')
// (6, 11, 'lutff_3/in_3')

wire n78;
// (6, 11, 'lutff_3/cout')
// (6, 11, 'lutff_4/in_3')

wire n79;
// (6, 11, 'lutff_4/cout')
// (6, 11, 'lutff_5/in_3')

wire n80;
// (6, 11, 'lutff_5/cout')
// (6, 11, 'lutff_6/in_3')

wire n81;
// (6, 11, 'lutff_6/cout')
// (6, 11, 'lutff_7/in_3')

wire n82;
// (6, 11, 'neigh_op_tnr_3')
// (6, 12, 'neigh_op_rgt_3')
// (6, 13, 'neigh_op_bnr_3')
// (7, 11, 'neigh_op_top_3')
// (7, 12, 'lutff_3/out')
// (7, 12, 'sp4_h_r_6')
// (7, 13, 'neigh_op_bot_3')
// (8, 11, 'neigh_op_tnl_3')
// (8, 12, 'neigh_op_lft_3')
// (8, 12, 'sp4_h_r_19')
// (8, 13, 'neigh_op_bnl_3')
// (9, 12, 'sp4_h_r_30')
// (10, 12, 'sp4_h_r_43')
// (11, 12, 'sp4_h_l_43')
// (11, 12, 'sp4_h_r_6')
// (12, 12, 'local_g1_3')
// (12, 12, 'lutff_global/cen')
// (12, 12, 'sp4_h_r_19')
// (13, 12, 'span4_horz_19')

reg n83 = 0;
// (7, 8, 'neigh_op_tnr_6')
// (7, 9, 'local_g3_6')
// (7, 9, 'lutff_0/in_3')
// (7, 9, 'neigh_op_rgt_6')
// (7, 9, 'sp4_r_v_b_44')
// (7, 10, 'neigh_op_bnr_6')
// (7, 10, 'sp4_r_v_b_33')
// (7, 11, 'sp4_r_v_b_20')
// (7, 12, 'local_g2_1')
// (7, 12, 'lutff_3/in_2')
// (7, 12, 'sp4_r_v_b_9')
// (8, 8, 'neigh_op_top_6')
// (8, 8, 'sp4_v_t_44')
// (8, 9, 'lutff_6/out')
// (8, 9, 'sp4_v_b_44')
// (8, 10, 'neigh_op_bot_6')
// (8, 10, 'sp4_v_b_33')
// (8, 11, 'sp4_v_b_20')
// (8, 12, 'sp4_v_b_9')
// (9, 8, 'neigh_op_tnl_6')
// (9, 9, 'local_g0_6')
// (9, 9, 'lutff_6/in_0')
// (9, 9, 'neigh_op_lft_6')
// (9, 10, 'neigh_op_bnl_6')

reg D2 = 0;
// (11, 11, 'neigh_op_tnr_1')
// (11, 12, 'neigh_op_rgt_1')
// (11, 13, 'neigh_op_bnr_1')
// (12, 11, 'neigh_op_top_1')
// (12, 12, 'local_g1_1')
// (12, 12, 'lutff_1/out')
// (12, 12, 'lutff_2/in_0')
// (12, 13, 'neigh_op_bot_1')
// (13, 11, 'logic_op_tnl_1')
// (13, 12, 'io_0/D_OUT_0')
// (13, 12, 'io_0/PAD')
// (13, 12, 'local_g1_1')
// (13, 12, 'logic_op_lft_1')
// (13, 13, 'logic_op_bnl_1')

reg D3 = 0;
// (11, 11, 'neigh_op_tnr_2')
// (11, 12, 'neigh_op_rgt_2')
// (11, 13, 'neigh_op_bnr_2')
// (12, 11, 'neigh_op_top_2')
// (12, 12, 'local_g1_2')
// (12, 12, 'lutff_2/out')
// (12, 12, 'lutff_5/in_0')
// (12, 13, 'neigh_op_bot_2')
// (13, 11, 'io_1/D_OUT_0')
// (13, 11, 'io_1/PAD')
// (13, 11, 'local_g1_2')
// (13, 11, 'logic_op_tnl_2')
// (13, 12, 'logic_op_lft_2')
// (13, 13, 'logic_op_bnl_2')

reg D1 = 0;
// (11, 11, 'neigh_op_tnr_3')
// (11, 12, 'neigh_op_rgt_3')
// (11, 13, 'neigh_op_bnr_3')
// (12, 11, 'neigh_op_top_3')
// (12, 12, 'local_g0_3')
// (12, 12, 'lutff_1/in_0')
// (12, 12, 'lutff_3/out')
// (12, 13, 'neigh_op_bot_3')
// (13, 11, 'logic_op_tnl_3')
// (13, 12, 'io_1/D_OUT_0')
// (13, 12, 'io_1/PAD')
// (13, 12, 'local_g0_3')
// (13, 12, 'logic_op_lft_3')
// (13, 13, 'logic_op_bnl_3')

reg D4 = 0;
// (11, 11, 'neigh_op_tnr_5')
// (11, 12, 'neigh_op_rgt_5')
// (11, 13, 'neigh_op_bnr_5')
// (12, 11, 'neigh_op_top_5')
// (12, 12, 'local_g0_5')
// (12, 12, 'lutff_3/in_0')
// (12, 12, 'lutff_5/out')
// (12, 13, 'neigh_op_bot_5')
// (13, 11, 'io_0/D_OUT_0')
// (13, 11, 'io_0/PAD')
// (13, 11, 'local_g1_5')
// (13, 11, 'logic_op_tnl_5')
// (13, 12, 'logic_op_lft_5')
// (13, 13, 'logic_op_bnl_5')

wire n88;
// (6, 9, 'lutff_0/cout')

wire n89;
// (6, 9, 'lutff_0/out')

wire n90;
// (6, 9, 'lutff_0/lout')

wire n91;
// (6, 9, 'carry_in_mux')

// Carry-In for (6 9)
assign n91 = 1;

wire n92;
// (6, 10, 'lutff_1/lout')

wire n93;
// (7, 8, 'lutff_7/lout')

wire n94;
// (7, 12, 'lutff_3/lout')

wire n95;
// (6, 9, 'lutff_1/out')

wire n96;
// (6, 9, 'lutff_1/lout')

wire n97;
// (5, 11, 'lutff_3/lout')

wire n98;
// (6, 10, 'lutff_0/lout')

wire n99;
// (12, 12, 'lutff_5/lout')

wire n100;
// (6, 9, 'lutff_6/lout')

wire n101;
// (6, 9, 'lutff_7/lout')

wire n102;
// (12, 12, 'lutff_3/lout')

wire n103;
// (9, 9, 'lutff_6/lout')

wire n104;
// (6, 9, 'lutff_4/lout')

wire n105;
// (12, 12, 'lutff_2/lout')

wire n106;
// (6, 9, 'lutff_5/lout')

wire n107;
// (12, 12, 'lutff_1/lout')

wire n108;
// (5, 11, 'lutff_7/lout')

wire n109;
// (5, 10, 'lutff_4/lout')

wire n110;
// (7, 10, 'lutff_6/lout')

wire n111;
// (6, 11, 'lutff_0/lout')

wire n112;
// (8, 9, 'lutff_6/lout')

wire n113;
// (7, 11, 'lutff_0/lout')

wire n114;
// (7, 10, 'lutff_7/lout')

wire n115;
// (6, 11, 'lutff_1/lout')

wire n116;
// (6, 8, 'lutff_7/lout')

wire n117;
// (7, 10, 'lutff_4/lout')

wire n118;
// (6, 9, 'lutff_3/lout')

wire n119;
// (6, 11, 'lutff_2/lout')

wire n120;
// (7, 11, 'lutff_2/lout')

wire n121;
// (5, 10, 'lutff_0/lout')

wire n122;
// (6, 11, 'lutff_3/lout')

wire n123;
// (5, 10, 'lutff_1/lout')

wire n124;
// (7, 10, 'lutff_2/lout')

wire n125;
// (6, 11, 'lutff_4/lout')

wire n126;
// (6, 12, 'lutff_5/lout')

wire n127;
// (7, 9, 'lutff_3/lout')

wire n128;
// (5, 10, 'lutff_2/lout')

wire n129;
// (7, 10, 'lutff_3/lout')

wire n130;
// (6, 11, 'lutff_5/lout')

wire n131;
// (7, 9, 'lutff_2/lout')

wire n132;
// (12, 9, 'lutff_5/lout')

wire n133;
// (5, 10, 'lutff_3/lout')

wire n134;
// (6, 10, 'lutff_7/lout')

wire n135;
// (6, 8, 'lutff_0/lout')

wire n136;
// (6, 11, 'lutff_6/lout')

wire n137;
// (7, 10, 'lutff_0/lout')

wire n138;
// (6, 8, 'lutff_3/lout')

wire n139;
// (6, 10, 'lutff_6/lout')

wire n140;
// (6, 12, 'lutff_7/lout')

wire n141;
// (7, 10, 'lutff_1/lout')

wire n142;
// (6, 11, 'lutff_7/lout')

wire n143;
// (7, 9, 'lutff_1/lout')

wire n144;
// (7, 9, 'lutff_0/lout')

wire n145;
// (6, 10, 'lutff_5/lout')

wire n146;
// (6, 8, 'lutff_5/lout')

wire n147;
// (6, 12, 'lutff_1/lout')

wire n148;
// (7, 9, 'lutff_7/lout')

wire n149;
// (6, 10, 'lutff_4/lout')

wire n150;
// (6, 9, 'lutff_2/lout')

wire n151;
// (7, 9, 'lutff_5/lout')

wire n152;
// (6, 8, 'lutff_4/lout')

wire n153;
// (6, 10, 'lutff_3/lout')

wire n154;
// (6, 12, 'lutff_0/lout')

wire n155;
// (7, 8, 'lutff_5/lout')

wire n156;
// (6, 12, 'lutff_3/lout')

wire n157;
// (6, 10, 'lutff_2/lout')

assign n89  = 1'b0;
assign n95  = 1'b0;
assign n92  = /* LUT    6 10  1 */ n65 ? n62 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n62 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n93  = /* LUT    7  8  7 */ 1'b0 ? 1'b0 : n46 ? 1'b0 : n6 ? n4 ? 1'b0 : 1'b1 : 1'b1;
assign n94  = /* LUT    7 12  3 */ 1'b0 ? 1'b0 : n83 ? n6 ? n4 ? 1'b1 : 1'b0 : 1'b0 : 1'b1;
assign n97  = /* LUT    5 11  3 */ 1'b0 ? 1'b0 : n34 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n98  = /* LUT    6 10  0 */ n59 ? n64 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n64 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n99  = /* LUT   12 12  5 */ 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : D3 ? 1'b1 : 1'b0;
assign n100 = /* LUT    6  9  6 */ n57 ? n17 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n17 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n101 = /* LUT    6  9  7 */ n58 ? n15 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n15 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n102 = /* LUT   12 12  3 */ 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : D4 ? 1'b1 : 1'b0;
assign n103 = /* LUT    9  9  6 */ 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : n83 ? 1'b0 : 1'b1;
assign n104 = /* LUT    6  9  4 */ n55 ? n16 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n16 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n105 = /* LUT   12 12  2 */ 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : D2 ? 1'b1 : 1'b0;
assign n106 = /* LUT    6  9  5 */ n56 ? n18 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n18 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n107 = /* LUT   12 12  1 */ 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : D1 ? 1'b1 : 1'b0;
assign n108 = /* LUT    5 11  7 */ 1'b0 ? 1'b0 : n33 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n109 = /* LUT    5 10  4 */ 1'b0 ? 1'b0 : n31 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n110 = /* LUT    7 10  6 */ 1'b0 ? 1'b0 : n28 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n111 = /* LUT    6 11  0 */ n72 ? n12 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n12 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n112 = /* LUT    8  9  6 */ 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : D5 ? 1'b1 : 1'b0;
assign n113 = /* LUT    7 11  0 */ 1'b0 ? 1'b0 : n38 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n114 = /* LUT    7 10  7 */ 1'b0 ? 1'b0 : n25 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n115 = /* LUT    6 11  1 */ n75 ? n11 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n11 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n116 = /* LUT    6  8  7 */ 1'b0 ? 1'b0 : n22 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n117 = /* LUT    7 10  4 */ 1'b0 ? 1'b0 : n30 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n118 = /* LUT    6  9  3 */ n54 ? n52 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n52 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n119 = /* LUT    6 11  2 */ n76 ? n44 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n44 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n120 = /* LUT    7 11  2 */ 1'b0 ? 1'b0 : n40 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n121 = /* LUT    5 10  0 */ n11 ? n12 ? n8 ? n32 ? 1'b1 : 1'b0 : 1'b0 : 1'b0 : 1'b0;
assign n122 = /* LUT    6 11  3 */ n77 ? n45 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n45 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n123 = /* LUT    5 10  1 */ 1'b0 ? 1'b0 : n29 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n124 = /* LUT    7 10  2 */ n60 ? n7 ? n61 ? n41 ? 1'b1 : 1'b0 : 1'b0 : 1'b0 : 1'b0;
assign n125 = /* LUT    6 11  4 */ n78 ? n42 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n42 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n126 = /* LUT    6 12  5 */ 1'b0 ? 1'b0 : n35 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n127 = /* LUT    7  9  3 */ 1'b0 ? 1'b0 : n49 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n128 = /* LUT    5 10  2 */ 1'b0 ? 1'b0 : n27 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n129 = /* LUT    7 10  3 */ 1'b0 ? 1'b0 : n26 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n130 = /* LUT    6 11  5 */ n79 ? n73 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n73 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n131 = /* LUT    7  9  2 */ 1'b0 ? n50 ? D5 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : D5 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n50 ? D5 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : D5 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n132 = /* LUT   12  9  5 */ 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : 1'b1;
assign n133 = /* LUT    5 10  3 */ 1'b0 ? 1'b0 : n13 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n134 = /* LUT    6 10  7 */ n71 ? n10 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n10 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n135 = /* LUT    6  8  0 */ n15 ? 1'b0 : n17 ? 1'b0 : n18 ? 1'b0 : n16 ? 1'b0 : 1'b1;
assign n136 = /* LUT    6 11  6 */ n80 ? n43 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n43 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n137 = /* LUT    7 10  0 */ n62 ? n64 ? n74 ? n73 ? 1'b1 : 1'b0 : 1'b0 : 1'b0 : 1'b0;
assign n138 = /* LUT    6  8  3 */ 1'b0 ? 1'b0 : n24 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n139 = /* LUT    6 10  6 */ n70 ? n3 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n3 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n140 = /* LUT    6 12  7 */ 1'b0 ? 1'b0 : n36 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n141 = /* LUT    7 10  1 */ n10 ? 1'b0 : n3 ? 1'b0 : n63 ? 1'b0 : n9 ? 1'b0 : 1'b1;
assign n142 = /* LUT    6 11  7 */ n81 ? n74 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n74 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n143 = /* LUT    7  9  1 */ n52 ? 1'b0 : n51 ? 1'b0 : n46 ? 1'b0 : n50 ? 1'b0 : 1'b1;
assign n144 = /* LUT    7  9  0 */ n83 ? n50 ? 1'b1 : n6 ? n4 ? 1'b1 : 1'b0 : 1'b0 : 1'b1;
assign n145 = /* LUT    6 10  5 */ n69 ? n63 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n63 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n146 = /* LUT    6  8  5 */ 1'b0 ? 1'b0 : n23 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n147 = /* LUT    6 12  1 */ 1'b0 ? 1'b0 : n37 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n148 = /* LUT    7  9  7 */ 1'b0 ? 1'b0 : n20 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n149 = /* LUT    6 10  4 */ n68 ? n8 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n8 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n150 = /* LUT    6  9  2 */ n53 ? n51 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n51 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n151 = /* LUT    7  9  5 */ 1'b0 ? 1'b0 : n19 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n152 = /* LUT    6  8  4 */ 1'b0 ? 1'b0 : n21 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n153 = /* LUT    6 10  3 */ n67 ? n32 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n32 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n154 = /* LUT    6 12  0 */ n42 ? n44 ? n43 ? 1'b0 : n45 ? 1'b0 : 1'b1 : 1'b0 : 1'b0;
assign n155 = /* LUT    7  8  5 */ 1'b0 ? 1'b0 : 1'b0 ? 1'b0 : n48 ? n14 ? 1'b1 : 1'b0 : 1'b0;
assign n156 = /* LUT    6 12  3 */ 1'b0 ? 1'b0 : n39 ? n6 ? n4 ? 1'b0 : 1'b1 : 1'b1 : 1'b0;
assign n157 = /* LUT    6 10  2 */ n66 ? n9 ? 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : n9 ? 1'b0 ? 1'b0 ? 1'b1 : 1'b0 : 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b0 ? 1'b0 : 1'b1 : 1'b0 ? 1'b1 : 1'b0;
assign n88  = /* CARRY  6  9  0 */ (n50 & 1'b0) | ((n50 | 1'b0) & n91);
assign n66  = /* CARRY  6 10  1 */ (1'b0 & n62) | ((1'b0 | n62) & n65);
assign n53  = /* CARRY  6  9  1 */ (1'b0 & n46) | ((1'b0 | n46) & n88);
assign n65  = /* CARRY  6 10  0 */ (1'b0 & n64) | ((1'b0 | n64) & n59);
assign n58  = /* CARRY  6  9  6 */ (1'b0 & n17) | ((1'b0 | n17) & n57);
assign n59  = /* CARRY  6  9  7 */ (1'b0 & n15) | ((1'b0 | n15) & n58);
assign n56  = /* CARRY  6  9  4 */ (1'b0 & n16) | ((1'b0 | n16) & n55);
assign n57  = /* CARRY  6  9  5 */ (1'b0 & n18) | ((1'b0 | n18) & n56);
assign n75  = /* CARRY  6 11  0 */ (1'b0 & n12) | ((1'b0 | n12) & n72);
assign n76  = /* CARRY  6 11  1 */ (1'b0 & n11) | ((1'b0 | n11) & n75);
assign n55  = /* CARRY  6  9  3 */ (1'b0 & n52) | ((1'b0 | n52) & n54);
assign n77  = /* CARRY  6 11  2 */ (1'b0 & n44) | ((1'b0 | n44) & n76);
assign n78  = /* CARRY  6 11  3 */ (1'b0 & n45) | ((1'b0 | n45) & n77);
assign n79  = /* CARRY  6 11  4 */ (1'b0 & n42) | ((1'b0 | n42) & n78);
assign n80  = /* CARRY  6 11  5 */ (1'b0 & n73) | ((1'b0 | n73) & n79);
assign n72  = /* CARRY  6 10  7 */ (1'b0 & n10) | ((1'b0 | n10) & n71);
assign n81  = /* CARRY  6 11  6 */ (1'b0 & n43) | ((1'b0 | n43) & n80);
assign n71  = /* CARRY  6 10  6 */ (1'b0 & n3) | ((1'b0 | n3) & n70);
assign n70  = /* CARRY  6 10  5 */ (1'b0 & n63) | ((1'b0 | n63) & n69);
assign n69  = /* CARRY  6 10  4 */ (1'b0 & n8) | ((1'b0 | n8) & n68);
assign n54  = /* CARRY  6  9  2 */ (1'b0 & n51) | ((1'b0 | n51) & n53);
assign n68  = /* CARRY  6 10  3 */ (1'b0 & n32) | ((1'b0 | n32) & n67);
assign n67  = /* CARRY  6 10  2 */ (1'b0 & n9) | ((1'b0 | n9) & n66);
/* FF  6  9  0 */ assign n89 = n90;
/* FF  6 10  1 */ assign n26 = n92;
/* FF  7  8  7 */ always @(posedge clk) if (n47) n46 <= n1 ? 1'b0 : n93;
/* FF  7 12  3 */ assign n82 = n94;
/* FF  6  9  1 */ assign n95 = n96;
/* FF  5 11  3 */ always @(posedge clk) if (1'b1) n11 <= n1 ? 1'b0 : n97;
/* FF  6 10  0 */ assign n25 = n98;
/* FF 12 12  5 */ always @(posedge clk) if (n82) D4 <= n1 ? 1'b0 : n99;
/* FF  6  9  6 */ assign n23 = n100;
/* FF  6  9  7 */ assign n24 = n101;
/* FF 12 12  3 */ always @(posedge clk) if (n82) D1 <= n1 ? 1'b1 : n102;
/* FF  9  9  6 */ assign n1 = n103;
/* FF  6  9  4 */ assign n21 = n104;
/* FF 12 12  2 */ always @(posedge clk) if (n82) D3 <= n1 ? 1'b0 : n105;
/* FF  6  9  5 */ assign n22 = n106;
/* FF 12 12  1 */ always @(posedge clk) if (n82) D2 <= n1 ? 1'b0 : n107;
/* FF  5 11  7 */ always @(posedge clk) if (1'b1) n12 <= n1 ? 1'b0 : n108;
/* FF  5 10  4 */ always @(posedge clk) if (1'b1) n3 <= n1 ? 1'b0 : n109;
/* FF  7 10  6 */ always @(posedge clk) if (1'b1) n32 <= n1 ? 1'b0 : n110;
/* FF  6 11  0 */ assign n33 = n111;
/* FF  8  9  6 */ always @(posedge clk) if (1'b1) n83 <= 1'b0 ? 1'b0 : n112;
/* FF  7 11  0 */ always @(posedge clk) if (1'b1) n73 <= n1 ? 1'b0 : n113;
/* FF  7 10  7 */ always @(posedge clk) if (1'b1) n64 <= n1 ? 1'b0 : n114;
/* FF  6 11  1 */ assign n34 = n115;
/* FF  6  8  7 */ always @(posedge clk) if (1'b1) n18 <= n1 ? 1'b0 : n116;
/* FF  7 10  4 */ always @(posedge clk) if (1'b1) n63 <= n1 ? 1'b0 : n117;
/* FF  6  9  3 */ assign n20 = n118;
/* FF  6 11  2 */ assign n35 = n119;
/* FF  7 11  2 */ always @(posedge clk) if (1'b1) n74 <= n1 ? 1'b0 : n120;
/* FF  5 10  0 */ assign n7 = n121;
/* FF  6 11  3 */ assign n36 = n122;
/* FF  5 10  1 */ always @(posedge clk) if (1'b1) n8 <= n1 ? 1'b0 : n123;
/* FF  7 10  2 */ assign n4 = n124;
/* FF  6 11  4 */ assign n37 = n125;
/* FF  6 12  5 */ always @(posedge clk) if (1'b1) n44 <= n1 ? 1'b0 : n126;
/* FF  7  9  3 */ always @(posedge clk) if (1'b1) n50 <= n1 ? 1'b0 : n127;
/* FF  5 10  2 */ always @(posedge clk) if (1'b1) n9 <= n1 ? 1'b0 : n128;
/* FF  7 10  3 */ always @(posedge clk) if (1'b1) n62 <= n1 ? 1'b0 : n129;
/* FF  6 11  5 */ assign n38 = n130;
/* FF  7  9  2 */ assign n49 = n131;
/* FF 12  9  5 */ assign D5 = n132;
/* FF  5 10  3 */ always @(posedge clk) if (1'b1) n10 <= n1 ? 1'b0 : n133;
/* FF  6 10  7 */ assign n13 = n134;
/* FF  6  8  0 */ assign n14 = n135;
/* FF  6 11  6 */ assign n39 = n136;
/* FF  7 10  0 */ assign n60 = n137;
/* FF  6  8  3 */ always @(posedge clk) if (1'b1) n15 <= n1 ? 1'b0 : n138;
/* FF  6 10  6 */ assign n31 = n139;
/* FF  6 12  7 */ always @(posedge clk) if (1'b1) n45 <= n1 ? 1'b0 : n140;
/* FF  7 10  1 */ assign n61 = n141;
/* FF  6 11  7 */ assign n40 = n142;
/* FF  7  9  1 */ assign n48 = n143;
/* FF  7  9  0 */ assign n47 = n144;
/* FF  6 10  5 */ assign n30 = n145;
/* FF  6  8  5 */ always @(posedge clk) if (1'b1) n17 <= n1 ? 1'b0 : n146;
/* FF  6 12  1 */ always @(posedge clk) if (1'b1) n42 <= n1 ? 1'b0 : n147;
/* FF  7  9  7 */ always @(posedge clk) if (1'b1) n52 <= n1 ? 1'b0 : n148;
/* FF  6 10  4 */ assign n29 = n149;
/* FF  6  9  2 */ assign n19 = n150;
/* FF  7  9  5 */ always @(posedge clk) if (1'b1) n51 <= n1 ? 1'b0 : n151;
/* FF  6  8  4 */ always @(posedge clk) if (1'b1) n16 <= n1 ? 1'b0 : n152;
/* FF  6 10  3 */ assign n28 = n153;
/* FF  6 12  0 */ assign n41 = n154;
/* FF  7  8  5 */ assign n6 = n155;
/* FF  6 12  3 */ always @(posedge clk) if (1'b1) n43 <= n1 ? 1'b0 : n156;
/* FF  6 10  2 */ assign n27 = n157;

endmodule

