module counter(
    input clk,
    input reset,
    input enable,
    output reg [3:0] count
);
    always @(posedge clk) begin
        if (reset) begin
            count <= 0;
        end else
        if (enable) begin
            count <= count + 1;
        end
    end
endmodule


`ifdef SIMULATION

module tb();
    reg clk_tb;
    reg reset_tb;
    reg enable_tb;
    wire [3:0] count_tb;

    counter counter_inst (
        .clk (clk_tb),
        .reset (reset_tb),
        .enable (enable_tb),
        .count (count_tb)
    );

    initial begin
        $dumpfile("counter.lxt");
        $dumpvars(0, counter_inst);

        $display("time\tclk\treset\tenable\tcount");
        $monitor("%g\t%b\t%b\t%b\t%b", $time, clk_tb, reset_tb, enable_tb, count_tb);

        clk_tb = 1;
        reset_tb = 0;
        enable_tb = 0;
        #5 reset_tb = 1;
        #5 reset_tb = 0;
        #5 enable_tb = 1;
        #100 enable_tb = 0;
        #5 $finish;
    end

    always begin
        #1 clk_tb = ~clk_tb;
    end
endmodule

`endif
