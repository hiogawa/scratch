# ELF

Towards understanding binary.

Build targets

- Executable
  - executable from single file (some_exe_single)
  - static linked executable    (some_exe_with_lib_static)
  - dynamic liked executable    (some_exe_with_lib_shared)
- Libraries
  - static archive (some_lib_static)
  - shared library (some_lib_shared)


# Building

```
$ mkdir -p out/Release
$ cd out/Release
$ cmake -G Ninja -DCMAKE_C_COMPILER=clang ../..
$ ninja -v
[1/10] /usr/sbin/clang    -MD -MT src/CMakeFiles/some_exe_single.dir/some_exe_single.c.o -MF src/CMakeFiles/some_exe_single.dir/some_exe_single.c.o.d -o src/CMakeFiles/some_exe_single.dir/some_exe_single.c.o   -c ../../src/some_exe_single.c
[2/10] /usr/sbin/clang -Dsome_lib_shared_EXPORTS  -fPIC -MD -MT src/CMakeFiles/some_lib_shared.dir/some_lib.c.o -MF src/CMakeFiles/some_lib_shared.dir/some_lib.c.o.d -o src/CMakeFiles/some_lib_shared.dir/some_lib.c.o   -c ../../src/some_lib.c
[3/10] /usr/sbin/clang    -MD -MT src/CMakeFiles/some_lib_static.dir/some_lib.c.o -MF src/CMakeFiles/some_lib_static.dir/some_lib.c.o.d -o src/CMakeFiles/some_lib_static.dir/some_lib.c.o   -c ../../src/some_lib.c
[4/10] : && /usr/sbin/clang -fPIC    -shared -Wl,-soname,libsome_lib_shared.so -o src/libsome_lib_shared.so src/CMakeFiles/some_lib_shared.dir/some_lib.c.o   && :
[5/10] : && /usr/sbin/clang    src/CMakeFiles/some_exe_single.dir/some_exe_single.c.o  -o src/some_exe_single   && :
[6/10] : && /usr/bin/cmake -E remove src/libsome_lib_static.a && /usr/sbin/ar qc src/libsome_lib_static.a  src/CMakeFiles/some_lib_static.dir/some_lib.c.o && /usr/sbin/ranlib src/libsome_lib_static.a && :
[7/10] /usr/sbin/clang  -I../../src  -MD -MT src/CMakeFiles/some_exe_with_lib_shared.dir/some_exe_with_lib.c.o -MF src/CMakeFiles/some_exe_with_lib_shared.dir/some_exe_with_lib.c.o.d -o src/CMakeFiles/some_exe_with_lib_shared.dir/some_exe_with_lib.c.o   -c ../../src/some_exe_with_lib.c
[8/10] /usr/sbin/clang  -I../../src  -MD -MT src/CMakeFiles/some_exe_with_lib_static.dir/some_exe_with_lib.c.o -MF src/CMakeFiles/some_exe_with_lib_static.dir/some_exe_with_lib.c.o.d -o src/CMakeFiles/some_exe_with_lib_static.dir/some_exe_with_lib.c.o   -c ../../src/some_exe_with_lib.c
[9/10] : && /usr/sbin/clang    src/CMakeFiles/some_exe_with_lib_shared.dir/some_exe_with_lib.c.o  -o src/some_exe_with_lib_shared  -Wl,-rpath,/home/hiogawa/code/hiogawa/try-elf/out/Release/src src/libsome_lib_shared.so && :
[10/10] : && /usr/sbin/clang    src/CMakeFiles/some_exe_with_lib_static.dir/some_exe_with_lib.c.o  -o src/some_exe_with_lib_static  src/libsome_lib_static.a && :
$ tree src
src
├── CMakeFiles
│   ├── some_exe_single.dir
│   │   └── some_exe_single.c.o
│   ├── some_exe_with_lib_shared.dir
│   │   └── some_exe_with_lib.c.o
│   ├── some_exe_with_lib_static.dir
│   │   └── some_exe_with_lib.c.o
│   ├── some_lib_shared.dir
│   │   └── some_lib.c.o
│   └── some_lib_static.dir
│       └── some_lib.c.o
├── cmake_install.cmake
├── libsome_lib_shared.so
├── libsome_lib_static.a
├── some_exe_single
├── some_exe_with_lib_shared
└── some_exe_with_lib_static

6 directories, 11 files
$ ldd src/some_exe_with_lib_shared
	linux-vdso.so.1 (0x00007ffe8d761000)
	libsome_lib_shared.so => /home/hiogawa/code/hiogawa/try-elf/out/Release/src/libsome_lib_shared.so (0x00007f01f7a84000)
	libc.so.6 => /usr/lib/libc.so.6 (0x00007f01f76e0000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f01f7c86000)
$ ldd src/some_exe_with_lib_static
	linux-vdso.so.1 (0x00007fff9a196000)
	libc.so.6 => /usr/lib/libc.so.6 (0x00007f1d0da33000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f1d0ddd7000)
$ src/some_exe_single; echo $?
3
$ src/some_exe_with_lib_static; echo $?
2
$ src/some_exe_with_lib_shared; echo $?
2
```


# Readelf

- Relocatable file
- Shared object file
- Executable file


```
[Relocatable file]

$ readelf -aW out/Release/src/CMakeFiles/some_exe_single.dir/some_exe_single.c.o
ELF Header:
  Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00
  Class:                             ELF64
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              REL (Relocatable file)
  Machine:                           Advanced Micro Devices X86-64
  Version:                           0x1
  Entry point address:               0x0
  Start of program headers:          0 (bytes into file)
  Start of section headers:          536 (bytes into file)
  Flags:                             0x0
  Size of this header:               64 (bytes)
  Size of program headers:           0 (bytes)
  Number of program headers:         0
  Size of section headers:           64 (bytes)
  Number of section headers:         8
  Section header string table index: 1

Section Headers:
  [Nr] Name              Type            Address          Off    Size   ES Flg Lk Inf Al
  [ 0]                   NULL            0000000000000000 000000 000000 00      0   0  0
  [ 1] .strtab           STRTAB          0000000000000000 0001b0 000062 00      0   0  1
  [ 2] .text             PROGBITS        0000000000000000 000040 000044 00  AX  0   0 16
  [ 3] .comment          PROGBITS        0000000000000000 000084 00002e 01  MS  0   0  1
  [ 4] .note.GNU-stack   PROGBITS        0000000000000000 0000b2 000000 00      0   0  1
  [ 5] .eh_frame         X86_64_UNWIND   0000000000000000 0000b8 000050 00   A  0   0  8
  [ 6] .rela.eh_frame    RELA            0000000000000000 000180 000030 18      7   5  8
  [ 7] .symtab           SYMTAB          0000000000000000 000108 000078 18      1   3  8
Key to Flags:
  W (write), A (alloc), X (execute), M (merge), S (strings), I (info),
  L (link order), O (extra OS processing required), G (group), T (TLS),
  C (compressed), x (unknown), o (OS specific), E (exclude),
  l (large), p (processor specific)

There are no section groups in this file.

There are no program headers in this file.

Relocation section '.rela.eh_frame' at offset 0x180 contains 2 entries:
    Offset             Info             Type               Symbol's Value  Symbol's Name + Addend
0000000000000020  0000000200000002 R_X86_64_PC32          0000000000000000 .text + 0
000000000000003c  0000000200000002 R_X86_64_PC32          0000000000000000 .text + 20

The decoding of unwind sections for machine type Advanced Micro Devices X86-64 is not currently supported.

Symbol table '.symtab' contains 5 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND
     1: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS ../../src/some_exe_single.c
     2: 0000000000000000     0 SECTION LOCAL  DEFAULT    2
     3: 0000000000000000    20 FUNC    GLOBAL DEFAULT    2 f
     4: 0000000000000020    36 FUNC    GLOBAL DEFAULT    2 main

No version information found in this file.


[Executable file]

$ readelf -aW out/Release/src/some_exe_single
ELF Header:
  Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00
  Class:                             ELF64
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              EXEC (Executable file)
  Machine:                           Advanced Micro Devices X86-64
  Version:                           0x1
  Entry point address:               0x400380
  Start of program headers:          64 (bytes into file)
  Start of section headers:          6376 (bytes into file)
  Flags:                             0x0
  Size of this header:               64 (bytes)
  Size of program headers:           56 (bytes)
  Number of program headers:         9
  Size of section headers:           64 (bytes)
  Number of section headers:         27
  Section header string table index: 26

Section Headers:
  [Nr] Name              Type            Address          Off    Size   ES Flg Lk Inf Al
  [ 0]                   NULL            0000000000000000 000000 000000 00      0   0  0
  [ 1] .interp           PROGBITS        0000000000400238 000238 00001c 00   A  0   0  1
  [ 2] .note.ABI-tag     NOTE            0000000000400254 000254 000020 00   A  0   0  4
  [ 3] .hash             HASH            0000000000400278 000278 000018 04   A  4   0  8
  [ 4] .dynsym           DYNSYM          0000000000400290 000290 000048 18   A  5   1  8
  [ 5] .dynstr           STRTAB          00000000004002d8 0002d8 000038 00   A  0   0  1
  [ 6] .gnu.version      VERSYM          0000000000400310 000310 000006 02   A  4   0  2
  [ 7] .gnu.version_r    VERNEED         0000000000400318 000318 000020 00   A  5   1  8
  [ 8] .rela.dyn         RELA            0000000000400338 000338 000030 18   A  4   0  8
  [ 9] .init             PROGBITS        0000000000400368 000368 000017 00  AX  0   0  4
  [10] .text             PROGBITS        0000000000400380 000380 0001c2 00  AX  0   0 16
  [11] .fini             PROGBITS        0000000000400544 000544 000009 00  AX  0   0  4
  [12] .rodata           PROGBITS        0000000000400550 000550 000004 04  AM  0   0  4
  [13] .eh_frame_hdr     PROGBITS        0000000000400554 000554 000034 00   A  0   0  4
  [14] .eh_frame         PROGBITS        0000000000400588 000588 0000ec 00   A  0   0  8
  [15] .init_array       INIT_ARRAY      0000000000600e48 000e48 000008 08  WA  0   0  8
  [16] .fini_array       FINI_ARRAY      0000000000600e50 000e50 000008 08  WA  0   0  8
  [17] .jcr              PROGBITS        0000000000600e58 000e58 000008 00  WA  0   0  8
  [18] .dynamic          DYNAMIC         0000000000600e60 000e60 000190 10  WA  5   0  8
  [19] .got              PROGBITS        0000000000600ff0 000ff0 000010 08  WA  0   0  8
  [20] .got.plt          PROGBITS        0000000000601000 001000 000018 08  WA  0   0  8
  [21] .data             PROGBITS        0000000000601018 001018 000010 00  WA  0   0  8
  [22] .bss              NOBITS          0000000000601028 001028 000008 00  WA  0   0  1
  [23] .comment          PROGBITS        0000000000000000 001028 000047 01  MS  0   0  1
  [24] .symtab           SYMTAB          0000000000000000 001070 0005b8 18     25  44  8
  [25] .strtab           STRTAB          0000000000000000 001628 0001d5 00      0   0  1
  [26] .shstrtab         STRTAB          0000000000000000 0017fd 0000e7 00      0   0  1
Key to Flags:
  W (write), A (alloc), X (execute), M (merge), S (strings), I (info),
  L (link order), O (extra OS processing required), G (group), T (TLS),
  C (compressed), x (unknown), o (OS specific), E (exclude),
  l (large), p (processor specific)

There are no section groups in this file.

Program Headers:
  Type           Offset   VirtAddr           PhysAddr           FileSiz  MemSiz   Flg Align
  PHDR           0x000040 0x0000000000400040 0x0000000000400040 0x0001f8 0x0001f8 R E 0x8
  INTERP         0x000238 0x0000000000400238 0x0000000000400238 0x00001c 0x00001c R   0x1
      [Requesting program interpreter: /lib64/ld-linux-x86-64.so.2]
  LOAD           0x000000 0x0000000000400000 0x0000000000400000 0x000674 0x000674 R E 0x200000
  LOAD           0x000e48 0x0000000000600e48 0x0000000000600e48 0x0001e0 0x0001e8 RW  0x200000
  DYNAMIC        0x000e60 0x0000000000600e60 0x0000000000600e60 0x000190 0x000190 RW  0x8
  NOTE           0x000254 0x0000000000400254 0x0000000000400254 0x000020 0x000020 R   0x4
  GNU_EH_FRAME   0x000554 0x0000000000400554 0x0000000000400554 0x000034 0x000034 R   0x4
  GNU_STACK      0x000000 0x0000000000000000 0x0000000000000000 0x000000 0x000000 RW  0x10
  GNU_RELRO      0x000e48 0x0000000000600e48 0x0000000000600e48 0x0001b8 0x0001b8 R   0x1

 Section to Segment mapping:
  Segment Sections...
   00
   01     .interp
   02     .interp .note.ABI-tag .hash .dynsym .dynstr .gnu.version .gnu.version_r .rela.dyn .init .text .fini .rodata .eh_frame_hdr .eh_frame
   03     .init_array .fini_array .jcr .dynamic .got .got.plt .data .bss
   04     .dynamic
   05     .note.ABI-tag
   06     .eh_frame_hdr
   07
   08     .init_array .fini_array .jcr .dynamic .got

Dynamic section at offset 0xe60 contains 20 entries:
  Tag        Type                         Name/Value
 0x0000000000000001 (NEEDED)             Shared library: [libc.so.6]
 0x000000000000000c (INIT)               0x400368
 0x000000000000000d (FINI)               0x400544
 0x0000000000000019 (INIT_ARRAY)         0x600e48
 0x000000000000001b (INIT_ARRAYSZ)       8 (bytes)
 0x000000000000001a (FINI_ARRAY)         0x600e50
 0x000000000000001c (FINI_ARRAYSZ)       8 (bytes)
 0x0000000000000004 (HASH)               0x400278
 0x0000000000000005 (STRTAB)             0x4002d8
 0x0000000000000006 (SYMTAB)             0x400290
 0x000000000000000a (STRSZ)              56 (bytes)
 0x000000000000000b (SYMENT)             24 (bytes)
 0x0000000000000015 (DEBUG)              0x0
 0x0000000000000007 (RELA)               0x400338
 0x0000000000000008 (RELASZ)             48 (bytes)
 0x0000000000000009 (RELAENT)            24 (bytes)
 0x000000006ffffffe (VERNEED)            0x400318
 0x000000006fffffff (VERNEEDNUM)         1
 0x000000006ffffff0 (VERSYM)             0x400310
 0x0000000000000000 (NULL)               0x0

Relocation section '.rela.dyn' at offset 0x338 contains 2 entries:
    Offset             Info             Type               Symbol's Value  Symbol's Name + Addend
0000000000600ff0  0000000100000006 R_X86_64_GLOB_DAT      0000000000000000 __libc_start_main@GLIBC_2.2.5 + 0
0000000000600ff8  0000000200000006 R_X86_64_GLOB_DAT      0000000000000000 __gmon_start__ + 0

The decoding of unwind sections for machine type Advanced Micro Devices X86-64 is not currently supported.

Symbol table '.dynsym' contains 3 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND
     1: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND __libc_start_main@GLIBC_2.2.5 (2)
     2: 0000000000000000     0 NOTYPE  WEAK   DEFAULT  UND __gmon_start__

Symbol table '.symtab' contains 61 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND
     1: 0000000000400238     0 SECTION LOCAL  DEFAULT    1
     2: 0000000000400254     0 SECTION LOCAL  DEFAULT    2
     3: 0000000000400278     0 SECTION LOCAL  DEFAULT    3
     4: 0000000000400290     0 SECTION LOCAL  DEFAULT    4
     5: 00000000004002d8     0 SECTION LOCAL  DEFAULT    5
     6: 0000000000400310     0 SECTION LOCAL  DEFAULT    6
     7: 0000000000400318     0 SECTION LOCAL  DEFAULT    7
     8: 0000000000400338     0 SECTION LOCAL  DEFAULT    8
     9: 0000000000400368     0 SECTION LOCAL  DEFAULT    9
    10: 0000000000400380     0 SECTION LOCAL  DEFAULT   10
    11: 0000000000400544     0 SECTION LOCAL  DEFAULT   11
    12: 0000000000400550     0 SECTION LOCAL  DEFAULT   12
    13: 0000000000400554     0 SECTION LOCAL  DEFAULT   13
    14: 0000000000400588     0 SECTION LOCAL  DEFAULT   14
    15: 0000000000600e48     0 SECTION LOCAL  DEFAULT   15
    16: 0000000000600e50     0 SECTION LOCAL  DEFAULT   16
    17: 0000000000600e58     0 SECTION LOCAL  DEFAULT   17
    18: 0000000000600e60     0 SECTION LOCAL  DEFAULT   18
    19: 0000000000600ff0     0 SECTION LOCAL  DEFAULT   19
    20: 0000000000601000     0 SECTION LOCAL  DEFAULT   20
    21: 0000000000601018     0 SECTION LOCAL  DEFAULT   21
    22: 0000000000601028     0 SECTION LOCAL  DEFAULT   22
    23: 0000000000000000     0 SECTION LOCAL  DEFAULT   23
    24: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS init.c
    25: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS crtstuff.c
    26: 0000000000600e58     0 OBJECT  LOCAL  DEFAULT   17 __JCR_LIST__
    27: 00000000004003b0     0 FUNC    LOCAL  DEFAULT   10 deregister_tm_clones
    28: 00000000004003f0     0 FUNC    LOCAL  DEFAULT   10 register_tm_clones
    29: 0000000000400430     0 FUNC    LOCAL  DEFAULT   10 __do_global_dtors_aux
    30: 0000000000601028     1 OBJECT  LOCAL  DEFAULT   22 completed.6960
    31: 0000000000600e50     0 OBJECT  LOCAL  DEFAULT   16 __do_global_dtors_aux_fini_array_entry
    32: 0000000000400450     0 FUNC    LOCAL  DEFAULT   10 frame_dummy
    33: 0000000000600e48     0 OBJECT  LOCAL  DEFAULT   15 __frame_dummy_init_array_entry
    34: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS ../../src/some_exe_single.c
    35: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS crtstuff.c
    36: 0000000000400670     0 OBJECT  LOCAL  DEFAULT   14 __FRAME_END__
    37: 0000000000600e58     0 OBJECT  LOCAL  DEFAULT   17 __JCR_END__
    38: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS
    39: 0000000000600e50     0 NOTYPE  LOCAL  DEFAULT   15 __init_array_end
    40: 0000000000600e60     0 OBJECT  LOCAL  DEFAULT   18 _DYNAMIC
    41: 0000000000600e48     0 NOTYPE  LOCAL  DEFAULT   15 __init_array_start
    42: 0000000000400554     0 NOTYPE  LOCAL  DEFAULT   13 __GNU_EH_FRAME_HDR
    43: 0000000000601000     0 OBJECT  LOCAL  DEFAULT   20 _GLOBAL_OFFSET_TABLE_
    44: 0000000000400540     2 FUNC    GLOBAL DEFAULT   10 __libc_csu_fini
    45: 0000000000601018     0 NOTYPE  WEAK   DEFAULT   21 data_start
    46: 0000000000601028     0 NOTYPE  GLOBAL DEFAULT   21 _edata
    47: 0000000000400544     0 FUNC    GLOBAL DEFAULT   11 _fini
    48: 0000000000400480    20 FUNC    GLOBAL DEFAULT   10 f
    49: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND __libc_start_main@@GLIBC_2.2.5
    50: 0000000000601018     0 NOTYPE  GLOBAL DEFAULT   21 __data_start
    51: 0000000000000000     0 NOTYPE  WEAK   DEFAULT  UND __gmon_start__
    52: 0000000000601020     0 OBJECT  GLOBAL HIDDEN    21 __dso_handle
    53: 0000000000400550     4 OBJECT  GLOBAL DEFAULT   12 _IO_stdin_used
    54: 00000000004004d0   101 FUNC    GLOBAL DEFAULT   10 __libc_csu_init
    55: 0000000000601030     0 NOTYPE  GLOBAL DEFAULT   22 _end
    56: 0000000000400380    43 FUNC    GLOBAL DEFAULT   10 _start
    57: 0000000000601028     0 NOTYPE  GLOBAL DEFAULT   22 __bss_start
    58: 00000000004004a0    36 FUNC    GLOBAL DEFAULT   10 main
    59: 0000000000601028     0 OBJECT  GLOBAL HIDDEN    21 __TMC_END__
    60: 0000000000400368     0 FUNC    GLOBAL DEFAULT    9 _init

Histogram for bucket list length (total of 1 buckets):
 Length  Number     % of total  Coverage
      0  1          (100.0%)

Version symbols section '.gnu.version' contains 3 entries:
 Addr: 0000000000400310  Offset: 0x000310  Link: 4 (.dynsym)
  000:   0 (*local*)       2 (GLIBC_2.2.5)   0 (*local*)

Version needs section '.gnu.version_r' contains 1 entries:
 Addr: 0x0000000000400318  Offset: 0x000318  Link: 5 (.dynstr)
  000000: Version: 1  File: libc.so.6  Cnt: 1
  0x0010:   Name: GLIBC_2.2.5  Flags: none  Version: 2

Displaying notes found in: .note.ABI-tag
  Owner                 Data size	Description
  GNU                  0x00000010	NT_GNU_ABI_TAG (ABI version tag)
    OS: Linux, ABI: 2.6.32

[ Shared object file ]
$ readelf -aW out/Release/src/libsome_lib_shared.so
ELF Header:
  Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00
  Class:                             ELF64
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              DYN (Shared object file)
  Machine:                           Advanced Micro Devices X86-64
  Version:                           0x1
  Entry point address:               0x4f0
  Start of program headers:          64 (bytes into file)
  Start of section headers:          6128 (bytes into file)
  Flags:                             0x0
  Size of this header:               64 (bytes)
  Size of program headers:           56 (bytes)
  Number of program headers:         6
  Size of section headers:           64 (bytes)
  Number of section headers:         26
  Section header string table index: 25

Section Headers:
  [Nr] Name              Type            Address          Off    Size   ES Flg Lk Inf Al
  [ 0]                   NULL            0000000000000000 000000 000000 00      0   0  0
  [ 1] .hash             HASH            0000000000000190 000190 000044 04   A  2   0  8
  [ 2] .dynsym           DYNSYM          00000000000001d8 0001d8 000120 18   A  3   1  8
  [ 3] .dynstr           STRTAB          00000000000002f8 0002f8 0000c2 00   A  0   0  1
  [ 4] .gnu.version      VERSYM          00000000000003ba 0003ba 000018 02   A  2   0  2
  [ 5] .gnu.version_r    VERNEED         00000000000003d8 0003d8 000020 00   A  3   1  8
  [ 6] .rela.dyn         RELA            00000000000003f8 0003f8 0000c0 18   A  2   0  8
  [ 7] .init             PROGBITS        00000000000004b8 0004b8 000017 00  AX  0   0  4
  [ 8] .plt              PROGBITS        00000000000004d0 0004d0 000010 10  AX  0   0 16
  [ 9] .plt.got          PROGBITS        00000000000004e0 0004e0 000008 00  AX  0   0  8
  [10] .text             PROGBITS        00000000000004f0 0004f0 000111 00  AX  0   0 16
  [11] .fini             PROGBITS        0000000000000604 000604 000009 00  AX  0   0  4
  [12] .eh_frame_hdr     PROGBITS        0000000000000610 000610 000024 00   A  0   0  4
  [13] .eh_frame         PROGBITS        0000000000000638 000638 00007c 00   A  0   0  8
  [14] .init_array       INIT_ARRAY      0000000000200e20 000e20 000008 08  WA  0   0  8
  [15] .fini_array       FINI_ARRAY      0000000000200e28 000e28 000008 08  WA  0   0  8
  [16] .jcr              PROGBITS        0000000000200e30 000e30 000008 00  WA  0   0  8
  [17] .dynamic          DYNAMIC         0000000000200e38 000e38 0001a0 10  WA  3   0  8
  [18] .got              PROGBITS        0000000000200fd8 000fd8 000028 08  WA  0   0  8
  [19] .got.plt          PROGBITS        0000000000201000 001000 000018 08  WA  0   0  8
  [20] .data             PROGBITS        0000000000201018 001018 000008 00  WA  0   0  8
  [21] .bss              NOBITS          0000000000201020 001020 000008 00  WA  0   0  1
  [22] .comment          PROGBITS        0000000000000000 001020 000047 01  MS  0   0  1
  [23] .symtab           SYMTAB          0000000000000000 001068 0004f8 18     24  42  8
  [24] .strtab           STRTAB          0000000000000000 001560 0001c1 00      0   0  1
  [25] .shstrtab         STRTAB          0000000000000000 001721 0000cd 00      0   0  1
Key to Flags:
  W (write), A (alloc), X (execute), M (merge), S (strings), I (info),
  L (link order), O (extra OS processing required), G (group), T (TLS),
  C (compressed), x (unknown), o (OS specific), E (exclude),
  l (large), p (processor specific)

There are no section groups in this file.

Program Headers:
  Type           Offset   VirtAddr           PhysAddr           FileSiz  MemSiz   Flg Align
  LOAD           0x000000 0x0000000000000000 0x0000000000000000 0x0006b4 0x0006b4 R E 0x200000
  LOAD           0x000e20 0x0000000000200e20 0x0000000000200e20 0x000200 0x000208 RW  0x200000
  DYNAMIC        0x000e38 0x0000000000200e38 0x0000000000200e38 0x0001a0 0x0001a0 RW  0x8
  GNU_EH_FRAME   0x000610 0x0000000000000610 0x0000000000000610 0x000024 0x000024 R   0x4
  GNU_STACK      0x000000 0x0000000000000000 0x0000000000000000 0x000000 0x000000 RW  0x10
  GNU_RELRO      0x000e20 0x0000000000200e20 0x0000000000200e20 0x0001e0 0x0001e0 R   0x1

 Section to Segment mapping:
  Segment Sections...
   00     .hash .dynsym .dynstr .gnu.version .gnu.version_r .rela.dyn .init .plt .plt.got .text .fini .eh_frame_hdr .eh_frame
   01     .init_array .fini_array .jcr .dynamic .got .got.plt .data .bss
   02     .dynamic
   03     .eh_frame_hdr
   04
   05     .init_array .fini_array .jcr .dynamic .got

Dynamic section at offset 0xe38 contains 22 entries:
  Tag        Type                         Name/Value
 0x0000000000000001 (NEEDED)             Shared library: [libc.so.6]
 0x000000000000000e (SONAME)             Library soname: [libsome_lib_shared.so]
 0x000000000000000c (INIT)               0x4b8
 0x000000000000000d (FINI)               0x604
 0x0000000000000019 (INIT_ARRAY)         0x200e20
 0x000000000000001b (INIT_ARRAYSZ)       8 (bytes)
 0x000000000000001a (FINI_ARRAY)         0x200e28
 0x000000000000001c (FINI_ARRAYSZ)       8 (bytes)
 0x0000000000000004 (HASH)               0x190
 0x0000000000000005 (STRTAB)             0x2f8
 0x0000000000000006 (SYMTAB)             0x1d8
 0x000000000000000a (STRSZ)              194 (bytes)
 0x000000000000000b (SYMENT)             24 (bytes)
 0x0000000000000003 (PLTGOT)             0x201000
 0x0000000000000007 (RELA)               0x3f8
 0x0000000000000008 (RELASZ)             192 (bytes)
 0x0000000000000009 (RELAENT)            24 (bytes)
 0x000000006ffffffe (VERNEED)            0x3d8
 0x000000006fffffff (VERNEEDNUM)         1
 0x000000006ffffff0 (VERSYM)             0x3ba
 0x000000006ffffff9 (RELACOUNT)          3
 0x0000000000000000 (NULL)               0x0

Relocation section '.rela.dyn' at offset 0x3f8 contains 8 entries:
    Offset             Info             Type               Symbol's Value  Symbol's Name + Addend
0000000000200e20  0000000000000008 R_X86_64_RELATIVE                         5c0
0000000000200e28  0000000000000008 R_X86_64_RELATIVE                         580
0000000000201018  0000000000000008 R_X86_64_RELATIVE                         201018
0000000000200fd8  0000000100000006 R_X86_64_GLOB_DAT      0000000000000000 _ITM_deregisterTMCloneTable + 0
0000000000200fe0  0000000400000006 R_X86_64_GLOB_DAT      0000000000000000 __gmon_start__ + 0
0000000000200fe8  0000000800000006 R_X86_64_GLOB_DAT      0000000000000000 _Jv_RegisterClasses + 0
0000000000200ff0  0000000900000006 R_X86_64_GLOB_DAT      0000000000000000 _ITM_registerTMCloneTable + 0
0000000000200ff8  0000000a00000006 R_X86_64_GLOB_DAT      0000000000000000 __cxa_finalize@GLIBC_2.2.5 + 0

The decoding of unwind sections for machine type Advanced Micro Devices X86-64 is not currently supported.

Symbol table '.dynsym' contains 12 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND
     1: 0000000000000000     0 NOTYPE  WEAK   DEFAULT  UND _ITM_deregisterTMCloneTable
     2: 0000000000201020     0 NOTYPE  GLOBAL DEFAULT   20 _edata
     3: 0000000000000604     0 FUNC    GLOBAL DEFAULT   11 _fini
     4: 0000000000000000     0 NOTYPE  WEAK   DEFAULT  UND __gmon_start__
     5: 0000000000201028     0 NOTYPE  GLOBAL DEFAULT   21 _end
     6: 00000000000005f0    17 FUNC    GLOBAL DEFAULT   10 lib_func
     7: 0000000000201020     0 NOTYPE  GLOBAL DEFAULT   21 __bss_start
     8: 0000000000000000     0 NOTYPE  WEAK   DEFAULT  UND _Jv_RegisterClasses
     9: 0000000000000000     0 NOTYPE  WEAK   DEFAULT  UND _ITM_registerTMCloneTable
    10: 0000000000000000     0 FUNC    WEAK   DEFAULT  UND __cxa_finalize@GLIBC_2.2.5 (2)
    11: 00000000000004b8     0 FUNC    GLOBAL DEFAULT    7 _init

Symbol table '.symtab' contains 53 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND
     1: 0000000000000190     0 SECTION LOCAL  DEFAULT    1
     2: 00000000000001d8     0 SECTION LOCAL  DEFAULT    2
     3: 00000000000002f8     0 SECTION LOCAL  DEFAULT    3
     4: 00000000000003ba     0 SECTION LOCAL  DEFAULT    4
     5: 00000000000003d8     0 SECTION LOCAL  DEFAULT    5
     6: 00000000000003f8     0 SECTION LOCAL  DEFAULT    6
     7: 00000000000004b8     0 SECTION LOCAL  DEFAULT    7
     8: 00000000000004d0     0 SECTION LOCAL  DEFAULT    8
     9: 00000000000004e0     0 SECTION LOCAL  DEFAULT    9
    10: 00000000000004f0     0 SECTION LOCAL  DEFAULT   10
    11: 0000000000000604     0 SECTION LOCAL  DEFAULT   11
    12: 0000000000000610     0 SECTION LOCAL  DEFAULT   12
    13: 0000000000000638     0 SECTION LOCAL  DEFAULT   13
    14: 0000000000200e20     0 SECTION LOCAL  DEFAULT   14
    15: 0000000000200e28     0 SECTION LOCAL  DEFAULT   15
    16: 0000000000200e30     0 SECTION LOCAL  DEFAULT   16
    17: 0000000000200e38     0 SECTION LOCAL  DEFAULT   17
    18: 0000000000200fd8     0 SECTION LOCAL  DEFAULT   18
    19: 0000000000201000     0 SECTION LOCAL  DEFAULT   19
    20: 0000000000201018     0 SECTION LOCAL  DEFAULT   20
    21: 0000000000201020     0 SECTION LOCAL  DEFAULT   21
    22: 0000000000000000     0 SECTION LOCAL  DEFAULT   22
    23: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS crtstuff.c
    24: 0000000000200e30     0 OBJECT  LOCAL  DEFAULT   16 __JCR_LIST__
    25: 00000000000004f0     0 FUNC    LOCAL  DEFAULT   10 deregister_tm_clones
    26: 0000000000000530     0 FUNC    LOCAL  DEFAULT   10 register_tm_clones
    27: 0000000000000580     0 FUNC    LOCAL  DEFAULT   10 __do_global_dtors_aux
    28: 0000000000201020     1 OBJECT  LOCAL  DEFAULT   21 completed.6960
    29: 0000000000200e28     0 OBJECT  LOCAL  DEFAULT   15 __do_global_dtors_aux_fini_array_entry
    30: 00000000000005c0     0 FUNC    LOCAL  DEFAULT   10 frame_dummy
    31: 0000000000200e20     0 OBJECT  LOCAL  DEFAULT   14 __frame_dummy_init_array_entry
    32: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS ../../src/some_lib.c
    33: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS crtstuff.c
    34: 00000000000006b0     0 OBJECT  LOCAL  DEFAULT   13 __FRAME_END__
    35: 0000000000200e30     0 OBJECT  LOCAL  DEFAULT   16 __JCR_END__
    36: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS
    37: 0000000000201018     0 OBJECT  LOCAL  DEFAULT   20 __dso_handle
    38: 0000000000200e38     0 OBJECT  LOCAL  DEFAULT   17 _DYNAMIC
    39: 0000000000000610     0 NOTYPE  LOCAL  DEFAULT   12 __GNU_EH_FRAME_HDR
    40: 0000000000201020     0 OBJECT  LOCAL  DEFAULT   20 __TMC_END__
    41: 0000000000201000     0 OBJECT  LOCAL  DEFAULT   19 _GLOBAL_OFFSET_TABLE_
    42: 0000000000000000     0 NOTYPE  WEAK   DEFAULT  UND _ITM_deregisterTMCloneTable
    43: 0000000000201020     0 NOTYPE  GLOBAL DEFAULT   20 _edata
    44: 0000000000000604     0 FUNC    GLOBAL DEFAULT   11 _fini
    45: 0000000000000000     0 NOTYPE  WEAK   DEFAULT  UND __gmon_start__
    46: 0000000000201028     0 NOTYPE  GLOBAL DEFAULT   21 _end
    47: 00000000000005f0    17 FUNC    GLOBAL DEFAULT   10 lib_func
    48: 0000000000201020     0 NOTYPE  GLOBAL DEFAULT   21 __bss_start
    49: 0000000000000000     0 NOTYPE  WEAK   DEFAULT  UND _Jv_RegisterClasses
    50: 0000000000000000     0 NOTYPE  WEAK   DEFAULT  UND _ITM_registerTMCloneTable
    51: 0000000000000000     0 FUNC    WEAK   DEFAULT  UND __cxa_finalize@@GLIBC_2.2.5
    52: 00000000000004b8     0 FUNC    GLOBAL DEFAULT    7 _init

Histogram for bucket list length (total of 3 buckets):
 Length  Number     % of total  Coverage
      0  3          (100.0%)

Version symbols section '.gnu.version' contains 12 entries:
 Addr: 00000000000003ba  Offset: 0x0003ba  Link: 2 (.dynsym)
  000:   0 (*local*)       0 (*local*)       1 (*global*)      1 (*global*)
  004:   0 (*local*)       1 (*global*)      1 (*global*)      1 (*global*)
  008:   0 (*local*)       0 (*local*)       2 (GLIBC_2.2.5)   1 (*global*)

Version needs section '.gnu.version_r' contains 1 entries:
 Addr: 0x00000000000003d8  Offset: 0x0003d8  Link: 3 (.dynstr)
  000000: Version: 1  File: libc.so.6  Cnt: 1
  0x0010:   Name: GLIBC_2.2.5  Flags: none  Version: 2

$ readelf -aW out/Release/src/some_exe_with_lib_shared
ELF Header:
  Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00
  Class:                             ELF64
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              EXEC (Executable file)
  Machine:                           Advanced Micro Devices X86-64
  Version:                           0x1
  Entry point address:               0x4004f0
  Start of program headers:          64 (bytes into file)
  Start of section headers:          6448 (bytes into file)
  Flags:                             0x0
  Size of this header:               64 (bytes)
  Size of program headers:           56 (bytes)
  Number of program headers:         9
  Size of section headers:           64 (bytes)
  Number of section headers:         29
  Section header string table index: 28

Section Headers:
  [Nr] Name              Type            Address          Off    Size   ES Flg Lk Inf Al
  [ 0]                   NULL            0000000000000000 000000 000000 00      0   0  0
  [ 1] .interp           PROGBITS        0000000000400238 000238 00001c 00   A  0   0  1
  [ 2] .note.ABI-tag     NOTE            0000000000400254 000254 000020 00   A  0   0  4
  [ 3] .hash             HASH            0000000000400278 000278 000038 04   A  4   0  8
  [ 4] .dynsym           DYNSYM          00000000004002b0 0002b0 0000d8 18   A  5   1  8
  [ 5] .dynstr           STRTAB          0000000000400388 000388 0000ae 00   A  0   0  1
  [ 6] .gnu.version      VERSYM          0000000000400436 000436 000012 02   A  4   0  2
  [ 7] .gnu.version_r    VERNEED         0000000000400448 000448 000020 00   A  5   1  8
  [ 8] .rela.dyn         RELA            0000000000400468 000468 000030 18   A  4   0  8
  [ 9] .rela.plt         RELA            0000000000400498 000498 000018 18  AI  4  22  8
  [10] .init             PROGBITS        00000000004004b0 0004b0 000017 00  AX  0   0  4
  [11] .plt              PROGBITS        00000000004004d0 0004d0 000020 10  AX  0   0 16
  [12] .text             PROGBITS        00000000004004f0 0004f0 000192 00  AX  0   0 16
  [13] .fini             PROGBITS        0000000000400684 000684 000009 00  AX  0   0  4
  [14] .rodata           PROGBITS        0000000000400690 000690 000004 04  AM  0   0  4
  [15] .eh_frame_hdr     PROGBITS        0000000000400694 000694 000034 00   A  0   0  4
  [16] .eh_frame         PROGBITS        00000000004006c8 0006c8 0000f4 00   A  0   0  8
  [17] .init_array       INIT_ARRAY      0000000000600de8 000de8 000008 08  WA  0   0  8
  [18] .fini_array       FINI_ARRAY      0000000000600df0 000df0 000008 08  WA  0   0  8
  [19] .jcr              PROGBITS        0000000000600df8 000df8 000008 00  WA  0   0  8
  [20] .dynamic          DYNAMIC         0000000000600e00 000e00 0001f0 10  WA  5   0  8
  [21] .got              PROGBITS        0000000000600ff0 000ff0 000010 08  WA  0   0  8
  [22] .got.plt          PROGBITS        0000000000601000 001000 000020 08  WA  0   0  8
  [23] .data             PROGBITS        0000000000601020 001020 000010 00  WA  0   0  8
  [24] .bss              NOBITS          0000000000601030 001030 000008 00  WA  0   0  1
  [25] .comment          PROGBITS        0000000000000000 001030 000047 01  MS  0   0  1
  [26] .symtab           SYMTAB          0000000000000000 001078 0005e8 18     27  46  8
  [27] .strtab           STRTAB          0000000000000000 001660 0001de 00      0   0  1
  [28] .shstrtab         STRTAB          0000000000000000 00183e 0000f1 00      0   0  1
Key to Flags:
  W (write), A (alloc), X (execute), M (merge), S (strings), I (info),
  L (link order), O (extra OS processing required), G (group), T (TLS),
  C (compressed), x (unknown), o (OS specific), E (exclude),
  l (large), p (processor specific)

There are no section groups in this file.

Program Headers:
  Type           Offset   VirtAddr           PhysAddr           FileSiz  MemSiz   Flg Align
  PHDR           0x000040 0x0000000000400040 0x0000000000400040 0x0001f8 0x0001f8 R E 0x8
  INTERP         0x000238 0x0000000000400238 0x0000000000400238 0x00001c 0x00001c R   0x1
      [Requesting program interpreter: /lib64/ld-linux-x86-64.so.2]
  LOAD           0x000000 0x0000000000400000 0x0000000000400000 0x0007bc 0x0007bc R E 0x200000
  LOAD           0x000de8 0x0000000000600de8 0x0000000000600de8 0x000248 0x000250 RW  0x200000
  DYNAMIC        0x000e00 0x0000000000600e00 0x0000000000600e00 0x0001f0 0x0001f0 RW  0x8
  NOTE           0x000254 0x0000000000400254 0x0000000000400254 0x000020 0x000020 R   0x4
  GNU_EH_FRAME   0x000694 0x0000000000400694 0x0000000000400694 0x000034 0x000034 R   0x4
  GNU_STACK      0x000000 0x0000000000000000 0x0000000000000000 0x000000 0x000000 RW  0x10
  GNU_RELRO      0x000de8 0x0000000000600de8 0x0000000000600de8 0x000218 0x000218 R   0x1

 Section to Segment mapping:
  Segment Sections...
   00
   01     .interp
   02     .interp .note.ABI-tag .hash .dynsym .dynstr .gnu.version .gnu.version_r .rela.dyn .rela.plt .init .plt .text .fini .rodata .eh_frame_hdr .eh_frame
   03     .init_array .fini_array .jcr .dynamic .got .got.plt .data .bss
   04     .dynamic
   05     .note.ABI-tag
   06     .eh_frame_hdr
   07
   08     .init_array .fini_array .jcr .dynamic .got

Dynamic section at offset 0xe00 contains 26 entries:
  Tag        Type                         Name/Value
 0x0000000000000001 (NEEDED)             Shared library: [libsome_lib_shared.so]
 0x0000000000000001 (NEEDED)             Shared library: [libc.so.6]
 0x000000000000000f (RPATH)              Library rpath: [/home/hiogawa/code/hiogawa/try-elf/out/Release/src]
 0x000000000000000c (INIT)               0x4004b0
 0x000000000000000d (FINI)               0x400684
 0x0000000000000019 (INIT_ARRAY)         0x600de8
 0x000000000000001b (INIT_ARRAYSZ)       8 (bytes)
 0x000000000000001a (FINI_ARRAY)         0x600df0
 0x000000000000001c (FINI_ARRAYSZ)       8 (bytes)
 0x0000000000000004 (HASH)               0x400278
 0x0000000000000005 (STRTAB)             0x400388
 0x0000000000000006 (SYMTAB)             0x4002b0
 0x000000000000000a (STRSZ)              174 (bytes)
 0x000000000000000b (SYMENT)             24 (bytes)
 0x0000000000000015 (DEBUG)              0x0
 0x0000000000000003 (PLTGOT)             0x601000
 0x0000000000000002 (PLTRELSZ)           24 (bytes)
 0x0000000000000014 (PLTREL)             RELA
 0x0000000000000017 (JMPREL)             0x400498
 0x0000000000000007 (RELA)               0x400468
 0x0000000000000008 (RELASZ)             48 (bytes)
 0x0000000000000009 (RELAENT)            24 (bytes)
 0x000000006ffffffe (VERNEED)            0x400448
 0x000000006fffffff (VERNEEDNUM)         1
 0x000000006ffffff0 (VERSYM)             0x400436
 0x0000000000000000 (NULL)               0x0

Relocation section '.rela.dyn' at offset 0x468 contains 2 entries:
    Offset             Info             Type               Symbol's Value  Symbol's Name + Addend
0000000000600ff0  0000000300000006 R_X86_64_GLOB_DAT      0000000000000000 __libc_start_main@GLIBC_2.2.5 + 0
0000000000600ff8  0000000400000006 R_X86_64_GLOB_DAT      0000000000000000 __gmon_start__ + 0

Relocation section '.rela.plt' at offset 0x498 contains 1 entries:
    Offset             Info             Type               Symbol's Value  Symbol's Name + Addend
0000000000601018  0000000600000007 R_X86_64_JUMP_SLOT     0000000000000000 lib_func + 0

The decoding of unwind sections for machine type Advanced Micro Devices X86-64 is not currently supported.

Symbol table '.dynsym' contains 9 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND
     1: 0000000000601030     0 NOTYPE  GLOBAL DEFAULT   23 _edata
     2: 0000000000400684     0 FUNC    GLOBAL DEFAULT   13 _fini
     3: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND __libc_start_main@GLIBC_2.2.5 (2)
     4: 0000000000000000     0 NOTYPE  WEAK   DEFAULT  UND __gmon_start__
     5: 0000000000601038     0 NOTYPE  GLOBAL DEFAULT   24 _end
     6: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND lib_func
     7: 0000000000601030     0 NOTYPE  GLOBAL DEFAULT   24 __bss_start
     8: 00000000004004b0     0 FUNC    GLOBAL DEFAULT   10 _init

Symbol table '.symtab' contains 63 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND
     1: 0000000000400238     0 SECTION LOCAL  DEFAULT    1
     2: 0000000000400254     0 SECTION LOCAL  DEFAULT    2
     3: 0000000000400278     0 SECTION LOCAL  DEFAULT    3
     4: 00000000004002b0     0 SECTION LOCAL  DEFAULT    4
     5: 0000000000400388     0 SECTION LOCAL  DEFAULT    5
     6: 0000000000400436     0 SECTION LOCAL  DEFAULT    6
     7: 0000000000400448     0 SECTION LOCAL  DEFAULT    7
     8: 0000000000400468     0 SECTION LOCAL  DEFAULT    8
     9: 0000000000400498     0 SECTION LOCAL  DEFAULT    9
    10: 00000000004004b0     0 SECTION LOCAL  DEFAULT   10
    11: 00000000004004d0     0 SECTION LOCAL  DEFAULT   11
    12: 00000000004004f0     0 SECTION LOCAL  DEFAULT   12
    13: 0000000000400684     0 SECTION LOCAL  DEFAULT   13
    14: 0000000000400690     0 SECTION LOCAL  DEFAULT   14
    15: 0000000000400694     0 SECTION LOCAL  DEFAULT   15
    16: 00000000004006c8     0 SECTION LOCAL  DEFAULT   16
    17: 0000000000600de8     0 SECTION LOCAL  DEFAULT   17
    18: 0000000000600df0     0 SECTION LOCAL  DEFAULT   18
    19: 0000000000600df8     0 SECTION LOCAL  DEFAULT   19
    20: 0000000000600e00     0 SECTION LOCAL  DEFAULT   20
    21: 0000000000600ff0     0 SECTION LOCAL  DEFAULT   21
    22: 0000000000601000     0 SECTION LOCAL  DEFAULT   22
    23: 0000000000601020     0 SECTION LOCAL  DEFAULT   23
    24: 0000000000601030     0 SECTION LOCAL  DEFAULT   24
    25: 0000000000000000     0 SECTION LOCAL  DEFAULT   25
    26: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS init.c
    27: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS crtstuff.c
    28: 0000000000600df8     0 OBJECT  LOCAL  DEFAULT   19 __JCR_LIST__
    29: 0000000000400520     0 FUNC    LOCAL  DEFAULT   12 deregister_tm_clones
    30: 0000000000400560     0 FUNC    LOCAL  DEFAULT   12 register_tm_clones
    31: 00000000004005a0     0 FUNC    LOCAL  DEFAULT   12 __do_global_dtors_aux
    32: 0000000000601030     1 OBJECT  LOCAL  DEFAULT   24 completed.6960
    33: 0000000000600df0     0 OBJECT  LOCAL  DEFAULT   18 __do_global_dtors_aux_fini_array_entry
    34: 00000000004005c0     0 FUNC    LOCAL  DEFAULT   12 frame_dummy
    35: 0000000000600de8     0 OBJECT  LOCAL  DEFAULT   17 __frame_dummy_init_array_entry
    36: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS ../../src/some_exe_with_lib.c
    37: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS crtstuff.c
    38: 00000000004007b8     0 OBJECT  LOCAL  DEFAULT   16 __FRAME_END__
    39: 0000000000600df8     0 OBJECT  LOCAL  DEFAULT   19 __JCR_END__
    40: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS
    41: 0000000000600df0     0 NOTYPE  LOCAL  DEFAULT   17 __init_array_end
    42: 0000000000600e00     0 OBJECT  LOCAL  DEFAULT   20 _DYNAMIC
    43: 0000000000600de8     0 NOTYPE  LOCAL  DEFAULT   17 __init_array_start
    44: 0000000000400694     0 NOTYPE  LOCAL  DEFAULT   15 __GNU_EH_FRAME_HDR
    45: 0000000000601000     0 OBJECT  LOCAL  DEFAULT   22 _GLOBAL_OFFSET_TABLE_
    46: 0000000000400680     2 FUNC    GLOBAL DEFAULT   12 __libc_csu_fini
    47: 0000000000601020     0 NOTYPE  WEAK   DEFAULT   23 data_start
    48: 0000000000601030     0 NOTYPE  GLOBAL DEFAULT   23 _edata
    49: 0000000000400684     0 FUNC    GLOBAL DEFAULT   13 _fini
    50: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND __libc_start_main@@GLIBC_2.2.5
    51: 0000000000601020     0 NOTYPE  GLOBAL DEFAULT   23 __data_start
    52: 0000000000000000     0 NOTYPE  WEAK   DEFAULT  UND __gmon_start__
    53: 0000000000601028     0 OBJECT  GLOBAL HIDDEN    23 __dso_handle
    54: 0000000000400690     4 OBJECT  GLOBAL DEFAULT   14 _IO_stdin_used
    55: 0000000000400610   101 FUNC    GLOBAL DEFAULT   12 __libc_csu_init
    56: 0000000000601038     0 NOTYPE  GLOBAL DEFAULT   24 _end
    57: 00000000004004f0    43 FUNC    GLOBAL DEFAULT   12 _start
    58: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND lib_func
    59: 0000000000601030     0 NOTYPE  GLOBAL DEFAULT   24 __bss_start
    60: 00000000004005f0    31 FUNC    GLOBAL DEFAULT   12 main
    61: 0000000000601030     0 OBJECT  GLOBAL HIDDEN    23 __TMC_END__
    62: 00000000004004b0     0 FUNC    GLOBAL DEFAULT   10 _init

Histogram for bucket list length (total of 3 buckets):
 Length  Number     % of total  Coverage
      0  3          (100.0%)

Version symbols section '.gnu.version' contains 9 entries:
 Addr: 0000000000400436  Offset: 0x000436  Link: 4 (.dynsym)
  000:   0 (*local*)       1 (*global*)      1 (*global*)      2 (GLIBC_2.2.5)
  004:   0 (*local*)       1 (*global*)      0 (*local*)       1 (*global*)
  008:   1 (*global*)

Version needs section '.gnu.version_r' contains 1 entries:
 Addr: 0x0000000000400448  Offset: 0x000448  Link: 5 (.dynstr)
  000000: Version: 1  File: libc.so.6  Cnt: 1
  0x0010:   Name: GLIBC_2.2.5  Flags: none  Version: 2

Displaying notes found in: .note.ABI-tag
  Owner                 Data size	Description
  GNU                  0x00000010	NT_GNU_ABI_TAG (ABI version tag)
    OS: Linux, ABI: 2.6.32
```