```
$ make
clang -c exe.c -o exe.o
clang -c lib.c -o lib.o
clang -fPIC -shared lib.o -o lib.so
clang lib.o exe.o -o exe_static
clang exe.o lib.so -o exe_dynamic
$ ./exe_static; echo $?
4
$ LD_LIBRARY_PATH=$PWD ./exe_dynamic; echo $?
4
```

```
$ readelf -aW exe.o # TODO: how the symbol "f" is used/represented inside of .text
...
Section Headers:
  [Nr] Name              Type            Address          Off    Size   ES Flg Lk Inf Al
  ...
  [ 2] .text             PROGBITS        0000000000000000 000040 00001f 00  AX  0   0 16
  [ 3] .rela.text        RELA            0000000000000000 000140 000018 18      8   2  8
  ...
  [ 8] .symtab           SYMTAB          0000000000000000 0000c8 000078 18      1   3  8
...

Relocation section '.rela.text' at offset 0x140 contains 1 entries:
    Offset             Info             Type               Symbol's Value  Symbol's Name + Addend
0000000000000015  0000000300000002 R_X86_64_PC32          0000000000000000 f - 4
...

Symbol table '.symtab' contains 5 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND
     1: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS exe.c
     2: 0000000000000000     0 SECTION LOCAL  DEFAULT    2
     3: 0000000000000000     0 NOTYPE  GLOBAL DEFAULT  UND f
     4: 0000000000000000    31 FUNC    GLOBAL DEFAULT    2 main

$ hexdump -C -s 0x40 -n 0x20 exe.o # .text section
00000040  55 48 89 e5 48 83 ec 10  bf 02 00 00 00 c7 45 fc  |UH..H.........E.|
00000050  00 00 00 00 e8 00 00 00  00 48 83 c4 10 5d c3 00  |.........H...]..|
00000060

# .rela.text section (this part connects symbols (from .symtab) and where its used within (.text))
$ hexdump -C -s 0x140 -n 0x18 exe.o
00000140  15 00 00 00 00 00 00 00  02 00 00 00 03 00 00 00  |................|
00000150  fc ff ff ff ff ff ff ff                           |........|
00000158

$ readelf -aW exe_static # symbol f is linked
...
Section Headers:
  [Nr] Name              Type            Address          Off    Size   ES Flg Lk Inf Al
  ...
  [10] .text             PROGBITS        0000000000400380 000380 0001b2 00  AX  0   0 16
  ...
  [24] .symtab           SYMTAB          0000000000000000 001070 0005d0 18     25  45  8
...

Symbol table '.symtab' contains 62 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND
    ...
    49: 0000000000400480    17 FUNC    GLOBAL DEFAULT   10 f
    ...
    59: 00000000004004a0    31 FUNC    GLOBAL DEFAULT   10 main
    ...

$ readelf -aW exe_dynamic
...

Section Headers:
  [Nr] Name              Type            Address          Off    Size   ES Flg Lk Inf Al
  ...
* [ 4] .dynsym           DYNSYM          00000000004002b0 0002b0 0000d8 18   A  5   1  8
  ...
* [ 9] .rela.plt         RELA            0000000000400450 000450 000018 18  AI  4  22  8
  [10] .init             PROGBITS        0000000000400468 000468 000017 00  AX  0   0  4
  [11] .plt              PROGBITS        0000000000400480 000480 000020 10  AX  0   0 16
  [12] .text             PROGBITS        00000000004004a0 0004a0 000192 00  AX  0   0 16
  [13] .fini             PROGBITS        0000000000400634 000634 000009 00  AX  0   0  4
  [14] .rodata           PROGBITS        0000000000400640 000640 000004 04  AM  0   0  4
  [15] .eh_frame_hdr     PROGBITS        0000000000400644 000644 000034 00   A  0   0  4
  [16] .eh_frame         PROGBITS        0000000000400678 000678 0000f4 00   A  0   0  8
  [17] .init_array       INIT_ARRAY      0000000000600df8 000df8 000008 08  WA  0   0  8
  [18] .fini_array       FINI_ARRAY      0000000000600e00 000e00 000008 08  WA  0   0  8
  [19] .jcr              PROGBITS        0000000000600e08 000e08 000008 00  WA  0   0  8
* [20] .dynamic          DYNAMIC         0000000000600e10 000e10 0001e0 10  WA  5   0  8
  [21] .got              PROGBITS        0000000000600ff0 000ff0 000010 08  WA  0   0  8
* [22] .got.plt          PROGBITS        0000000000601000 001000 000020 08  WA  0   0  8
  ...

Program Headers:
  Type           Offset   VirtAddr           PhysAddr           FileSiz  MemSiz   Flg Align
  ...
  INTERP         0x000238 0x0000000000400238 0x0000000000400238 0x00001c 0x00001c R   0x1
      [Requesting program interpreter: /lib64/ld-linux-x86-64.so.2]
  LOAD           0x000000 0x0000000000400000 0x0000000000400000 0x00076c 0x00076c R E 0x200000
  LOAD           0x000df8 0x0000000000600df8 0x0000000000600df8 0x000238 0x000240 RW  0x200000
  DYNAMIC        0x000e10 0x0000000000600e10 0x0000000000600e10 0x0001e0 0x0001e0 RW  0x8
  ...


Dynamic section at offset 0xe10 contains 25 entries:
  Tag        Type                         Name/Value
 0x0000000000000001 (NEEDED)             Shared library: [lib.so]
 ...
 0x0000000000000003 (PLTGOT)             0x601000                   # NOTE: section [22]
 0x0000000000000002 (PLTRELSZ)           24 (bytes)
 0x0000000000000014 (PLTREL)             RELA
 0x0000000000000017 (JMPREL)             0x400450
 0x0000000000000007 (RELA)               0x400420
 0x0000000000000008 (RELASZ)             48 (bytes)
 0x0000000000000009 (RELAENT)            24 (bytes)
 0x000000006ffffffe (VERNEED)            0x400400
 0x000000006fffffff (VERNEEDNUM)         1
 0x000000006ffffff0 (VERSYM)             0x4003ee
 0x0000000000000000 (NULL)               0x0

Relocation section '.rela.plt' at offset 0x450 contains 1 entries:
    Offset             Info             Type               Symbol's Value  Symbol's Name + Addend
0000000000601018  0000000300000007 R_X86_64_JUMP_SLOT     0000000000000000 f + 0

...

Symbol table '.dynsym' contains 9 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     ...
     3: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND f
     ...

$ readelf -aW lib.so
(TODO)
```
