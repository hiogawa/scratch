makeQuine = () => {
    let makeCompo = (s1, s2) => `(x) => eval(${s1})(eval(${s2})(x))`;
    let makePrinter = (s) => `(x) => ${s}`;
    let B = (s) => `(x) => eval(${s})(eval((x) => ${s})(x))`; // inline of B = (s) => makeCompo(s, makePrinter(s))
    return makeCompo(B.toString(), makePrinter(B.toString()));
}

makeQuine();
// => '(x) => eval((s) => `(x) => eval(${s})(eval((x) => ${s})(x))`)(eval((x) => (s) => `(x) => eval(${s})(eval((x) => ${s})(x))`)(x))'

quine = makeQuine();
quine === eval(quine)('whatever');      // true
eval(quine)() === eval(eval(quine)())() // true
