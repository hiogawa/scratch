B = (s) => `() => eval(${s})(${s})`
Q = `() => eval(${B})(${B})`
// => '() => eval((s) => `() => eval(${s})(${s})`)((s) => `() => eval(${s})(${s})`)'

Q === eval(Q)()
eval(Q)() === eval(eval(Q)())()
